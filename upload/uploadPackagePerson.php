<?php
require_once '../Classes/PHPExcel.php';

include '../Classes/PHPExcel/IOFactory.php';
include("../inc/function/connect.php");
include("../inc/function/mainFunc.php");

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);


    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $r = -1;
    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
      $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
      if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
          ++$r;
          foreach($headingsArray as $columnKey => $columnHeading) {
              $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
          }
      }
    }
    $anum = 0;
    foreach ($namedDataArray as $result) {

      //COMPANY_CODE
      //TRANS_NO
      //TRANS_DATE
      //TRANS_TIME
      //TRANS_ITEM_CODE
      //TRANS_ITEM_NO
      //PERSON_CODE
      //MEMBER_SDATE
      //MEMBER_EDATE
      //MEMBER_TYPE_CODE
      //MEMBER_TYPE_NO	TRANS_ACT_SDATE	TRANS_ACT_TRAINER	TRANS_BILL_NO	TRANS_DESC	TRANS_ACTION_TYPE	TRANS_LIMIT_1DAY	TRANS_LIMIT_MAX	TRANS_LIMIT_TIMES	TRANS_LIMIT_TYPE	TRANS_LIMIT_DAYS	TRANS_PAID_STATUS	TRANS_PRICE_GTOTAL	TRANS_PRICE_NET	TRANS_PRICE_PREPAID	TRANS_PRICE_PROD	TRANS_PRICE_SUBTOTAL	TRANS_PRICE_TOTAL	TRANS_PRICE_UNIT	TRANS_PRICE_1UNIT_CALC	TRANS_PRICE_1UNIT_VAT	TRANS_PROD_CODE	TRANS_PROD_FREE	///TRANS_PROD_POINT_GET	TRANS_PROD_POINT_FREE	TRANS_PROD_POINT_MAX	TRANS_PROD_QTY	TRANS_PAY_POINT_PRICE	TRANS_PAY_POINT_TOTAL	TRANS_PROD_SN	TRANS_PROD_SET_OWNER_CODE	TRANS_PROD_SET_OWNER_NO	TRANS_RUNNO	TRANS_TYPE	TRANS_SUBTYPE	TRANS_VAT_PRICE	TRANS_VAT_TYPE	TRANS_SHIFT_DATE	TRANS_SHIFT_CODE	TRANS_MONTH	TRANS_YEAR	TRANS_TERM	TRANS_STATUS	TRANS_USAGE_COUNT	TRANS_USAGE_BALANCE	DATA_STATUS	DATA_UPTO_CENTER	DATA_CREATE_BY	DATA_CREATE_DATE	DATA_MODIFY_BY	DATA_MODIFY_DATE	DATA_RECORD_RUNNO


      $package_type = "";
      $branch1 = "Y";
      $branch2 = "";
      $anum++;
      // if($anum > 100){
      //     break;
      // }


      if($result["TRANS_SUBTYPE"] == "EXPENSE"){
        continue;
      }

      if($result["TRANS_SUBTYPE"] == "INCOME"){
        continue;
      }

      if($result["TRANS_SUBTYPE"] == "PRODUCT"){
        continue;
      }

      if($result["TRANS_SUBTYPE"] == "PACKAGE_TRIAL")
      {
        continue;
      }

      if($result["TRANS_SUBTYPE"] == "PACKAGE")
      {
        continue;
      }

      if($result["TRANS_SUBTYPE"] == "MEMBER_TYPE")
      {
        $package_type = "MB";
        $branch2 = "Y";
      }

      $status   = "";
      $endDate  =  $result["MEMBER_EDATE"];
      $date     =  date("Y/m/d");

      $diff = DateDiff($date,$endDate);

      if($diff > 0){
        $status   = "A";
      }else{
        $status   = "U";
      }

      $arrPackage = array();

      $limit = $result["TRANS_LIMIT_MAX"];
      $limitType = $result["TRANS_LIMIT_TYPE"];
      $max = 0;
      if($limitType == "MONTHS"){
        $max = $limit * 30;
      }
      else if($limitType == "YEAR")
      {
        $max = $limit * 365;
      }
      else if($limitType == "DAYS")
      {
        $max = $limit;
      }



      $arrPackage['company_code']          = "GYMMK01";
    	$arrPackage['person_code']           = $result["PERSON_CODE"];
    	$arrPackage['package_code']          = $result["MEMBER_TYPE_CODE"];
    	$arrPackage['package_name']          = $result["TRANS_DESC"];
    	$arrPackage['package_detail']        = "";
    	$arrPackage['reg_no']                = $result["MEMBER_TYPE_NO"];
    	$arrPackage['use_package']           = $limit;
    	$arrPackage['num_use']               = $limit;
    	$arrPackage['package_unit']          = $limitType;
    	$arrPackage['date_start']            = $result["MEMBER_SDATE"];
    	$arrPackage['date_expire']           = $endDate;
    	$arrPackage['max_use']               = $result["TRANS_LIMIT_DAYS"] != ""?$result["TRANS_LIMIT_DAYS"]:$max;
    	$arrPackage['package_price']         = $result["TRANS_PRICE_NET"];
    	$arrPackage['package_num']           = "1";
    	$arrPackage['package_price_total']   = $result["TRANS_PRICE_NET"];
    	$arrPackage['percent_discount']      = "0";
    	$arrPackage['discount']              = "0";
    	$arrPackage['vat']                   = "0";
    	$arrPackage['type_vat']              = "";
    	$arrPackage['net_total']             = $result["TRANS_PRICE_NET"];
    	$arrPackage['notify_num']            = "0";
    	$arrPackage['notify_unit']           = $result["TRANS_LIMIT_TYPE"];
    	$arrPackage['type_package']          = $package_type;
    	$arrPackage['status_notify']         = "";
    	$arrPackage['trainer_code']          = "";
    	$arrPackage['trainer_name']          = "";
    	$arrPackage['invoice_code']          = "";
    	$arrPackage['receipt_code']          = $result["TRANS_BILL_NO"];
    	$arrPackage['create_by']             = "10001";
    	$arrPackage['create_date']           = $result["DATA_CREATE_DATE"] != ""?$result["DATA_CREATE_DATE"]:"";
    	$arrPackage['status']                = $status;
    	$arrPackage['update_by']             = "10001";
    	$arrPackage['update_date']           = $result["DATA_CREATE_DATE"] != ""?$result["DATA_CREATE_DATE"]:"";
    	$arrPackage['pakage_transfer_id']    = "0";
    	$arrPackage['price_per_use']         = "0";
    	$arrPackage['type_buy']              = "R";
    	$arrPackage['sale_code']             = "";

      $person_code = $arrPackage["person_code"];
      $date_expire = $arrPackage['date_expire'];
      $reg_no      = $arrPackage['reg_no'];
      $sql = "UPDATE person SET  PERSON_EXPIRE_DATE = '$date_expire' where PERSON_CODE = '$person_code'";
      DbQuery($sql,null);

      $sql = "SELECT * FROM trans_package_person WHERE person_code = '$person_code' and reg_no = '$reg_no' and date_expire = '$date_expire'";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);

      $dataCount    = $json['dataCount'];

      if($dataCount > 0){
        $sqls = DBUpdatePOST2($arrPackage,"trans_package_person","reg_no");
      }else{
        $sqls = DBInsertPOST2($arrPackage,"trans_package_person");
      }
      echo $sqls;
      //break;
      DbQuery($sqls,null);

    }
}

?>
