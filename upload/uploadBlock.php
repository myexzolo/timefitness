<?php
require_once '../Classes/PHPExcel.php';

include '../Classes/PHPExcel/IOFactory.php';
include("../inc/function/connect.php");

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);

    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $headerName = $headingsArray['A'];

    $headingsArray = array("A" => "plan_code",
                          "B" => "plan_name",
                          "C" => "plan_shotName",
                          "D" => "plan_Description",
                          "E" => "plan_ZoneCode",
                          "F" => "plan_Status"
                         );

    $r = -1;

    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
        $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
        if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
            ++$r;
            foreach($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }
    }
    //
    $i = 0;
    foreach ($namedDataArray as $result) {
        if(trim($result["plan_code"]) == ""){
          continue;
        }
        $plan_code                = trim($result["plan_code"]);
        $plan_name                = trim($result["plan_name"]);
        $plan_shotName            = trim($result["plan_shotName"]);
        $plan_Description         = trim($result["plan_Description"]);
        $plan_ZoneCode            = trim($result["plan_ZoneCode"]);
        $plan_Status              = trim($result["plan_Status"]);


        $sql = "SELECT * FROM management_zone WHERE system_code = '$plan_ZoneCode'";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);

        $rows         = $json['data'];
        $dataCount    = $json['dataCount'];

        //echo  ">>>>>>>>>.".$sql." ,".$dataCount;
        $strSQL = "";
        if($dataCount > 0){
            $zone_code   = $rows[0]['zone_code'];
            if(strlen($plan_shotName) > 3)
            {
              $code = substr($plan_shotName,-3);
            }else{
              $code = $plan_shotName;
            }
            //echo "code :".$code;
            if(is_numeric($code)){
              $block_code  = $zone_code.sprintf("%03d", $code);
            }else{
              $block_code  =  $zone_code.substr($plan_code,-3);
            }


            $sqlb = "SELECT * FROM management_block WHERE block_code = '$block_code' and zone_code = '$zone_code'";
            $queryb = DbQuery($sqlb,null);
            $jsonb  = json_decode($queryb, true);
            $dataCountb    = $jsonb['dataCount'];



            if($dataCountb > 0){
                $sqls = "UPDATE management_block SET
                system_code   = '$plan_code',
                zone_code     = '$zone_code',
                block_name    = '$plan_name',
                detail        = '$plan_Description'
                WHERE block_code = '$block_code'";
            }else{
              $sqls = "INSERT INTO management_block VALUES(
                null,'$plan_code','$zone_code','$block_code','$plan_name','$plan_Description'
                ,'0','',''
                ,'0','0','0'
                ,'0','0','0'
                ,'0','0','0'
                ,'','0','0','0','0'
                ,'','0','0','0','0'
                ,'0','0','0'
                ,0,0,0,'Y',now(),now(),'0','0','0')";
            }
            echo $sqls;
            DbQuery($sqls,null);
        }
  }
}

?>
