<?php
require_once '../Classes/PHPExcel.php';

include '../Classes/PHPExcel/IOFactory.php';
include("../inc/function/connect.php");
include("../inc/function/mainFunc.php");

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);


    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $r = -1;
    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
      $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
      if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
          ++$r;
          foreach($headingsArray as $columnKey => $columnHeading) {
              $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
          }
      }
    }

    foreach ($namedDataArray as $result) {
      // COMPANY_CODE
      // PERSON_CODE
      // PERSON_CODE_INT
      // PERSON_TITLE
      // PERSON_NICKNAME
      // PERSON_NAME
      // PERSON_LASTNAME
      // PERSON_TITLE_ENG
      // PERSON_NAME_ENG
      // PERSON_LASTNAME_ENG
      // PERSON_SEX
      // PERSON_BIRTH_DATE
      // PERSON_CARD_ID
      // PERSON_CARD_SDATE
      // PERSON_CARD_EDATE
      // PERSON_MARRIAGE
      // PERSON_COOP_ENABLE
      // PERSON_TEL_HOME
      // PERSON_TEL_MOBILE
      // PERSON_TEL_MOBILE2
      // PERSON_TEL_OFFICE
      // PERSON_FAX
      // PERSON_EMAIL
      // PERSON_GROUP
      // PERSON_VIP
      // PERSON_STATUS
      // PERSON_REGISTER_DATE
      // PERSON_EXPIRE_DATE
      // PERSON_NOTE
      // PERSON_LAST_VISIT
      // PERSON_GRADE
      // PERSON_MUST_AT
      // PERSON_RECV_NAME
      // PERSON_HOME1_ADDR1
      // PERSON_HOME1_ADDR2
      // PERSON_HOME1_ADDR3
      // PERSON_HOME1_ADDR4
      // PERSON_HOME1_SUBDISTRICT
      // PERSON_HOME1_DISTRICT
      // PERSON_HOME1_PROVINCE
      // PERSON_HOME1_COUNTRY
      // PERSON_HOME1_POSTAL
      // PERSON_BILL_TYPE
      // PERSON_BILL_NAME
      // PERSON_BILL_ADDR1
      // PERSON_BILL_ADDR2
      // PERSON_BILL_SUBDISTRICT
      // PERSON_BILL_DISTRICT
      // PERSON_BILL_PROVINCE
      // PERSON_BILL_POSTAL
      // PERSON_BILL_COUNTRY
      // PERSON_BILL_TAXNO
      // SEND_NEWS_TO
      // PERSON_SALE_CODE
      // PERSON_GROUP_AGE
      // PERSON_RETRY_STATUS
      // PERSON_RETRY_DATE
      // PERSON_RECV_TYPE
      // PERSON_DEATH_STATUS
      // PERSON_DEATH_DATE
      // DATA_SHIFT_DATE
      // DATA_SHIFT_CODE
      // DATA_STATUS
      // DATA_CREATE_DATE
      // DATA_CREATE_BY
      // DATA_CREATE_SHIFT
      // DATA_MODIFY_DATE
      // DATA_MODIFY_BY
      // DATA_RECORD_RUNNO
      // OBJ_FIT_FIRM
      // OBJ_MUSCLE_BUILD
      // DATA_DELETE_STATUS
      if(!is_numeric($result["PERSON_CODE"])){
        continue;
      }
      $result["COMPANY_CODE"]         =   "GYMMK01";
      $result["PERSON_RUNNO"]         =   "";
      $result["PERSON_CODE_INT"]      =   str_replace(",","",$result["PERSON_CODE_INT"]);
      $result["PERSON_BIRTH_DATE"]    =  $result["PERSON_BIRTH_DATE"] != ""?dateThToEn($result["PERSON_BIRTH_DATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_CARD_SDATE"]    =  $result["PERSON_CARD_SDATE"] != ""?dateThToEn($result["PERSON_CARD_SDATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_CARD_EDATE"]    =  $result["PERSON_CARD_EDATE"] != ""?dateThToEn($result["PERSON_CARD_EDATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_REGISTER_DATE"] =  $result["PERSON_REGISTER_DATE"] != ""?datetimeEn($result["PERSON_REGISTER_DATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_EXPIRE_DATE"]   =  $result["PERSON_EXPIRE_DATE"] != ""?dateThToEn($result["PERSON_EXPIRE_DATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_LAST_VISIT"]   =  $result["PERSON_LAST_VISIT"] != ""?datetimeEn($result["PERSON_LAST_VISIT"],"dd.mm.yyyy","."):"";
      $result["DATA_SHIFT_DATE"]      =  $result["DATA_SHIFT_DATE"] != ""?dateThToEn($result["DATA_SHIFT_DATE"],"dd.mm.yyyy","."):"";
      $result["DATA_CREATE_DATE"]     =  $result["DATA_CREATE_DATE"] != ""?datetimeEn($result["DATA_CREATE_DATE"],"dd.mm.yyyy","."):"";
      $result["DATA_MODIFY_DATE"]     =  $result["DATA_MODIFY_DATE"] != ""?datetimeEn($result["DATA_MODIFY_DATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_RETRY_DATE"]     =  $result["PERSON_RETRY_DATE"] != ""?datetimeEn($result["PERSON_RETRY_DATE"],"dd.mm.yyyy","."):"";
      $result["PERSON_DEATH_DATE"]     =  $result["PERSON_DEATH_DATE"] != ""?datetimeEn($result["PERSON_DEATH_DATE"],"dd.mm.yyyy","."):"";
      $result["SEND_NEWS_TO"]         =   $result["SEND_NEWS_TO"] != ""? $result["SEND_NEWS_TO"]:"0";


      if($result["PERSON_BIRTH_DATE"] == ""){
        unset($result["PERSON_BIRTH_DATE"]);
      }
      if($result["PERSON_CARD_SDATE"] == ""){
        unset($result["PERSON_CARD_SDATE"]);
      }
      if($result["PERSON_CARD_EDATE"] == ""){
        unset($result["PERSON_CARD_EDATE"]);
      }
      if($result["PERSON_REGISTER_DATE"] == ""){
        unset($result["PERSON_REGISTER_DATE"]);
      }
      if($result["PERSON_EXPIRE_DATE"] == ""){
        unset($result["PERSON_EXPIRE_DATE"]);
      }
      if($result["PERSON_LAST_VISIT"] == ""){
        unset($result["PERSON_LAST_VISIT"]);
      }
      if($result["DATA_SHIFT_DATE"] == ""){
        unset($result["DATA_SHIFT_DATE"]);
      }
      if($result["DATA_CREATE_DATE"] == ""){
        unset($result["DATA_CREATE_DATE"]);
      }
      if($result["DATA_MODIFY_DATE"] == ""){
        unset($result["DATA_MODIFY_DATE"]);
      }
      if($result["PERSON_RETRY_DATE"] == ""){
        unset($result["PERSON_RETRY_DATE"]);
      }
      if($result["PERSON_DEATH_DATE"] == ""){
        unset($result["PERSON_DEATH_DATE"]);
      }


      $PERSON_CODE = $result["PERSON_CODE"];

      $sql = "SELECT * FROM person WHERE 	PERSON_CODE = '$PERSON_CODE'";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);

      $dataCount    = $json['dataCount'];

      if($dataCount > 0){
        $sqls = DBUpdatePOST2($result,"person","PERSON_CODE");
      }else{
        $sqls = DBInsertPOST2($result,"person");
      }
      echo $sqls;
      //break;
      DbQuery($sqls,null);

    }
}

?>
