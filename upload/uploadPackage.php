<?php
require_once '../Classes/PHPExcel.php';

include '../Classes/PHPExcel/IOFactory.php';
include("../inc/function/connect.php");
include("../inc/function/mainFunc.php");

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);


    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $r = -1;
    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
      $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
      if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
          ++$r;
          foreach($headingsArray as $columnKey => $columnHeading) {
              $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
          }
      }
    }

    foreach ($namedDataArray as $result) {

      // COMPANY_CODE
      // PROD_CODE	PROD_BARCODE
      // PROD_DESC
      // LOCATION_CODE
      // PROD_COMMS_TYPE
      // PROD_EDATE
      // PROD_GROUP
      // PROD_LIMIT_1DAY
      // PROD_LIMIT_MAX
      // PROD_LIMIT_TIMES
      // PROD_LIMIT_TYPE
      // PROD_LIMIT_DAYS
      // PROD_PRICE
      // PROD_POINT_CALC
      // PROD_SDATE
      // PROD_TYPE
      // PROD_VAT_TYPE
      // DATA_TYPE
      // DATA_STATUS
      // DATA_CREATE_BY
      // DATA_CREATE_DATE

      $package_type = "";
      $branch1 = "Y";
      $branch2 = "";

      if($result["PROD_TYPE"] == "EXPENSE"){
        continue;
      }

      if($result["PROD_TYPE"] == "INCOME"){
        continue;
      }

      if($result["PROD_TYPE"] == "PRODUCT"){
        continue;
      }

      if($result["PROD_TYPE"] == "PACKAGE_TRIAL")
      {
        $package_type = "PT";
      }

      if($result["PROD_TYPE"] == "PACKAGE")
      {
        $package_type = "PT";
      }

      if($result["PROD_TYPE"] == "MEMBER_TYPE")
      {
        $package_type = "MB";
        $branch2 = "Y";
      }

      $arrPackage = array();

      $package_code = $result["PROD_CODE"];

    	$arrPackage['package_code']      = $package_code;
    	$arrPackage['package_name']      = $result["PROD_DESC"];
    	$arrPackage['package_detail']    = "";
    	$arrPackage['package_type']      = $package_type;
    	$arrPackage['num_use']           = $result["PROD_LIMIT_MAX"];
    	$arrPackage['package_unit']      = $result["PROD_LIMIT_TYPE"];
    	$arrPackage['max_use']           = $result["PROD_LIMIT_DAYS"];
    	$arrPackage['package_price']     = $result["PROD_PRICE"];
    	$arrPackage['type_vat']          = $result["PROD_VAT_TYPE"];
    	$arrPackage['notify_num']        = "0";
    	$arrPackage['notify_unit']       = $result["PROD_LIMIT_TYPE"];
    	$arrPackage['package_status']    = $result["DATA_STATUS"];
    	$arrPackage['create_by']         = "10001";
    	$arrPackage['create_date']       = $result["DATA_CREATE_DATE"] != ""?$result["DATA_CREATE_DATE"]:"";
    	$arrPackage['company_code']      = "GYMMK01";
    	$arrPackage['branch1']           = $branch1;
    	$arrPackage['branch2']           = $branch2;
    	$arrPackage['branch3']           = "";
    	$arrPackage['branch4']           = "";
    	$arrPackage['branch5']           = "";
      $arrPackage['update_by']         = "10001";
      $arrPackage['seq']               = "0";
      $arrPackage['update_date']       = $result["DATA_CREATE_DATE"] != ""?$result["DATA_CREATE_DATE"]:"";

      if($arrPackage["create_date"] == ""){
        unset($arrPackage["create_date"]);
      }

      if($arrPackage["update_date"] == ""){
        unset($arrPackage["update_date"]);
      }

      $sql = "SELECT * FROM data_mas_package WHERE package_code = '$package_code'";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);

      $dataCount    = $json['dataCount'];

      if($dataCount > 0){
        $sqls = DBUpdatePOST2($arrPackage,"data_mas_package","package_code");
      }else{
        $sqls = DBInsertPOST2($arrPackage,"data_mas_package");
      }
      echo $sqls;
      //break;
      DbQuery($sqls,null);

    }
}

?>
