<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$code       = isset($_POST['code'])?$_POST['code']:"";
$typeActive = isset($_POST['typeActive'])?$_POST['typeActive']:"";

$EMP_CODE         = isset($_POST['EMP_CODE'])?$_POST['EMP_CODE']:"";
$EMP_TITLE        = isset($_POST['EMP_TITLE'])?$_POST['EMP_TITLE']:"";
$EMP_NAME         = isset($_POST['EMP_NAME'])?$_POST['EMP_NAME']:"";
$EMP_LASTNAME     = isset($_POST['EMP_LASTNAME'])?$_POST['EMP_LASTNAME']:"";
$EMP_POSITION     = isset($_POST['EMP_POSITION'])?$_POST['EMP_POSITION']:"";
$EMP_DEPARTMENT   = isset($_POST['EMP_DEPARTMENT'])?$_POST['EMP_DEPARTMENT']:"";
$EMP_SEX          = isset($_POST['EMP_SEX'])?$_POST['EMP_SEX']:"";
$EMP_BIRTH_DATE   = isset($_POST['EMP_BIRTH_DATE'])?$_POST['EMP_BIRTH_DATE']:"";
$EMP_TYPE         = isset($_POST['EMP_TYPE'])?$_POST['EMP_TYPE']:"";
$EMP_GROUP        = isset($_POST['EMP_GROUP'])?$_POST['EMP_GROUP']:"";
$EMP_NICKNAME     = isset($_POST['EMP_NICKNAME'])?$_POST['EMP_NICKNAME']:"";
$EMP_DATE_INCOME  = isset($_POST['EMP_DATE_INCOME'])?$_POST['EMP_DATE_INCOME']:"";
$EMP_USER         = isset($_POST['EMP_USER'])?$_POST['EMP_USER']:$EMP_CODE;
$EMP_PSW          = isset($_POST['EMP_PSW'])?$_POST['EMP_PSW']:$EMP_CODE;
$EMP_CARD_ID      = isset($_POST['EMP_CARD_ID'])?$_POST['EMP_CARD_ID']:"";
$EMP_ADDRESS1     = isset($_POST['EMP_ADDRESS1'])?$_POST['EMP_ADDRESS1']:"";
$EMP_ADDRESS2     = isset($_POST['EMP_ADDRESS2'])?$_POST['EMP_ADDRESS2']:"";
$EMP_ADDR_SUBDISTRICT   = isset($_POST['EMP_ADDR_SUBDISTRICT'])?$_POST['EMP_ADDR_SUBDISTRICT']:"";
$EMP_ADDR_DISTRICT      = isset($_POST['EMP_ADDR_DISTRICT'])?$_POST['EMP_ADDR_DISTRICT']:"";
$EMP_ADDR_PROVINCE      = isset($_POST['EMP_ADDR_PROVINCE'])?$_POST['EMP_ADDR_PROVINCE']:"";
$EMP_ADDR_POSTAL        = isset($_POST['EMP_ADDR_POSTAL'])?$_POST['EMP_ADDR_POSTAL']:"";
$EMP_EMAIL        = isset($_POST['EMP_EMAIL'])?$_POST['EMP_EMAIL']:"";
$EMP_TEL          = isset($_POST['EMP_TEL'])?$_POST['EMP_TEL']:"";
$EMP_DATE_RETRY   = isset($_POST['EMP_DATE_RETRY'])?$_POST['EMP_DATE_RETRY']:"";
$EMP_PICTURE      = isset($_POST['EMP_PICTURE'])?$_POST['EMP_PICTURE']:"";
$EMP_IS_TRAINER   = isset($_POST['EMP_IS_TRAINER'])?$_POST['EMP_IS_TRAINER']:"";
$EMP_IS_SALE      = isset($_POST['EMP_IS_SALE'])?$_POST['EMP_IS_SALE']:"";
$EMP_IS_STAFF     = isset($_POST['EMP_IS_STAFF'])?$_POST['EMP_IS_STAFF']:"";
$EMP_IS_INSTRUCTOR = isset($_POST['EMP_IS_INSTRUCTOR'])?$_POST['EMP_IS_INSTRUCTOR']:"";
$EMP_WAGE           = isset($_POST['EMP_WAGE'])?$_POST['EMP_WAGE']:"";
$user_id_update   = isset($_POST['user_id_update'])?$_POST['user_id_update']:"";
$branch1          = isset($_POST['branch1'])?$_POST['branch1']:"";
$branch2          = isset($_POST['branch2'])?$_POST['branch2']:"";
$branch3          = isset($_POST['branch3'])?$_POST['branch3']:"";
$branch4          = isset($_POST['branch4'])?$_POST['branch4']:"";
$branch5          = isset($_POST['branch5'])?$_POST['branch5']:"";

$user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
$user_login     = $EMP_USER;
$user_name      = $EMP_NAME;
$user_last      = $EMP_LASTNAME;
$user_email     = $EMP_EMAIL;
$user_password  = @md5($EMP_USER);
$is_active      = "Y";
$role_list      = "3";
$branch_code    = $code;
$department_code  = $EMP_DEPARTMENT;
$note1          = "";
$note2          = "";

$user_img   = "";
$updateImg  = "";


$EMP_PICTURE = str_replace(' ', '', $EMP_PICTURE);


// --ADD EDIT DELETE Module--

if($typeActive == "ADD"){
  $sql2   = "INSERT INTO t_user (user_login,user_password,user_email,user_name,user_last,
            is_active,role_list,user_img,user_since,user_id_update,note1,note2,branch_code,department_code)
            VALUES('$user_login','$user_password','$user_email','$user_name','$user_last',
            '$is_active','$role_list','$EMP_PICTURE',NOW(),'$user_id_update','$note1','$note2','$branch_code','$department_code')";


  $sql   = "INSERT INTO data_mas_employee (
            COMPANY_CODE,
            EMP_CODE,EMP_TITLE,
            EMP_NAME,EMP_LASTNAME,
            EMP_POSITION,EMP_DEPARTMENT,
            EMP_SEX,EMP_BIRTH_DATE,
            EMP_TYPE,EMP_GROUP,
            EMP_NICKNAME,EMP_DATE_INCOME,
            EMP_USER,EMP_PSW,
            EMP_CARD_ID,EMP_ADDRESS1,
            EMP_ADDRESS2,EMP_ADDR_SUBDISTRICT,
            EMP_ADDR_DISTRICT,EMP_ADDR_PROVINCE,
            EMP_ADDR_POSTAL,EMP_EMAIL,EMP_WAGE,
            EMP_TEL,EMP_IS_TRAINER,EMP_IS_SALE,EMP_PICTURE,EMP_IS_STAFF,EMP_IS_INSTRUCTOR,
            branch1,branch2,branch3,branch4,branch5)
           VALUES(
             '$code',
             '$EMP_CODE','$EMP_TITLE',
             '$EMP_NAME','$EMP_LASTNAME',
             '$EMP_POSITION','$EMP_DEPARTMENT',
             '$EMP_SEX','$EMP_BIRTH_DATE',
             '$EMP_TYPE','$EMP_GROUP',
             '$EMP_NICKNAME','$EMP_DATE_INCOME',
             '$EMP_USER','$EMP_PSW',
             '$EMP_CARD_ID','$EMP_ADDRESS1',
             '$EMP_ADDRESS2','$EMP_ADDR_SUBDISTRICT',
             '$EMP_ADDR_DISTRICT','$EMP_ADDR_PROVINCE',
             '$EMP_ADDR_POSTAL','$EMP_EMAIL','$EMP_WAGE',
             '$EMP_TEL','$EMP_IS_TRAINER','$EMP_IS_SALE','$EMP_PICTURE','$EMP_IS_STAFF','$EMP_IS_INSTRUCTOR',
             '$branch1','$branch2','$branch3','$branch4','$branch5'
           )";
}else if($typeActive == "EDIT"){

  if(!empty($EMP_PICTURE)){
      $updateImg    =  "user_img = '$EMP_PICTURE',";
  }

  $sql2 = "UPDATE t_user SET
            user_name      = '$user_name',
            user_last      = '$user_last',
            user_email     = '$user_email',
            note1          = '$note1',
            note2          = '$note2',
            branch_code    = '$branch_code',
            department_code  = '$department_code',
            ".$updateImg."
            is_active      = '$is_active',
            update_date    = NOW(),
            user_id_update = '$user_id_update'
            WHERE user_login = '$user_login' and branch_code = '$code' ";

  $sql = "UPDATE data_mas_employee SET
          EMP_TITLE           ='$EMP_TITLE',
          EMP_NAME            ='$EMP_NAME',
          EMP_LASTNAME        ='$EMP_LASTNAME',
          EMP_POSITION        ='$EMP_POSITION',
          EMP_DEPARTMENT      ='$EMP_DEPARTMENT',
          EMP_SEX             ='$EMP_SEX',
          EMP_BIRTH_DATE      ='$EMP_BIRTH_DATE',
          EMP_USER            = $EMP_USER,
          EMP_TYPE            ='$EMP_TYPE',
          EMP_NICKNAME        ='$EMP_NICKNAME',
          EMP_DATE_INCOME     ='$EMP_DATE_INCOME',
          EMP_CARD_ID         ='$EMP_CARD_ID',
          EMP_ADDRESS1        ='$EMP_ADDRESS1',
          EMP_ADDRESS2        ='$EMP_ADDRESS2',
          EMP_ADDR_SUBDISTRICT  ='$EMP_ADDR_SUBDISTRICT',
          EMP_ADDR_DISTRICT     ='$EMP_ADDR_DISTRICT',
          EMP_ADDR_PROVINCE     ='$EMP_ADDR_PROVINCE',
          EMP_ADDR_POSTAL       ='$EMP_ADDR_POSTAL',
          EMP_EMAIL             ='$EMP_EMAIL',
          EMP_TEL               ='$EMP_TEL',
          EMP_WAGE              = '$EMP_WAGE',
          EMP_IS_TRAINER        ='$EMP_IS_TRAINER',
          EMP_IS_SALE           ='$EMP_IS_SALE',
          EMP_IS_STAFF          ='$EMP_IS_STAFF',
          branch1 = '$branch1',
          branch2 = '$branch2',
          branch3 = '$branch3',
          branch4 = '$branch4',
          branch5 = '$branch5',
          EMP_IS_INSTRUCTOR     = '$EMP_IS_INSTRUCTOR'
          WHERE COMPANY_CODE = '$code' and EMP_CODE = '$EMP_CODE' ";
}
else if($typeActive == "DEL"){
  $sql   = "UPDATE data_mas_employee
          SET DATA_DELETE_STATUS = 'Y',
          DATA_DELETE_DATE = NOW(),
          DATA_DELETE_BY = '$user_id_update'
          WHERE COMPANY_CODE = '$code' and EMP_CODE = '$EMP_CODE' ";

  $sql2 = "UPDATE t_user SET
            is_active      = 'N',
            update_date    = NOW(),
            user_id_update = '$user_id_update'
            WHERE user_login = '$user_login' and branch_code = '$code' ";
}
//echo $sql."<br>";
//echo $sql2;
//--ADD EDIT USER-- //
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

$query2      = DbQuery($sql2,null);
$row2        = json_decode($query2, true);
$errorInfo2  = $row2['errorInfo'];

if(intval($row['errorInfo'][0]) == 0 && intval($row2['errorInfo'][0]) == 0 ){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql."<br>".$sql2)));
}


?>
