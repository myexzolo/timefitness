<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$package_type = isset($_GET['package_type'])?$_GET['package_type']:"";
$companycode = isset($_GET['companycode'])?$_GET['companycode']:"";

if($companycode == "GYMMK01"){
  $package_type = $package_type."01";
}
else if($companycode == "GYMMK02")
{
  $package_type = $package_type."02";
}
else if($companycode == "GYMMK03")
{
  $package_type = $package_type."03";
}
else if($companycode == "GYMMK04")
{
  $package_type = $package_type."04";
}
else if($companycode == "GYMMK05")
{
  $package_type = $package_type."05";
}

$sql = "SELECT max(package_code) as package_code
        FROM data_mas_package
        where package_code like '$package_type%' and company_code = '$companycode' ";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

//echo $sql;

if($dataCount > 0){
  $Code     = $row[0]['package_code'];
  $lastNum  = substr($Code,strlen($package_type));
  $lastNum  = $lastNum + 1;
  $packageCode = $package_type.sprintf("%04d", $lastNum);
}else{
  $packageCode = $package_type."0001";
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $packageCode)));
?>
