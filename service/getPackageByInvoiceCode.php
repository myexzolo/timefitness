<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companyCode    = isset($_GET['companyCode'])?$_GET['companyCode']:"";
$invoice_code   = isset($_GET['invoice_code'])?$_GET['invoice_code']:"";


$sql = "SELECT ps.*, iv.invoice_date FROM trans_package_person ps, tb_invoice iv
where ps.invoice_code = iv.invoice_code and ps.invoice_code =  '$invoice_code' and 	iv.status = 'A'  and iv.company_code =  '$companyCode'
order by ps.date_expire ASC";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else if (intval($errorInfo[0]) == 0 && $dataCount == 0){
  header('Content-Type: application/json');
  exit(json_encode(array()));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
