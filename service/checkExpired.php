<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');


$company_code       = isset($_GET['company_code'])?$_GET['company_code']:"";

$dateNow            = date('Y-m-d H:i:s');
$sqlEx  = "";

///UPDATE trans_package_person EXPIRE

$sql = "SELECT * FROM trans_package_person where company_code  = '$company_code' and date_expire < '$dateNow' and status in ('T','A') order by date_expire";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

for($i=0 ; $i < $dataCount ; $i++)
{
  $id   = $rows[$i]['id'];
  $sqlEx   .= "UPDATE trans_package_person SET status = 'E' WHERE id = '$id';";
}


///UPDATE reserve_class EXPIRE

$sql = "SELECT * FROM t_reserve_class where branch_code  = '$company_code' and expire_date < '$dateNow' and status = 'S' order by expire_date";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

for($i=0 ; $i < $dataCount ; $i++)
{
  $id     = $rows[$i]['reserve_id'];
  $code   = $rows[$i]['PERSON_CODE'];
  $sqlEx   .= "UPDATE t_reserve_class SET status = 'E' WHERE reserve_id = '$id';";
  //$sqlEx   .= "UPDATE person SET BAN_RESERVE = DATE_ADD(NOW(), INTERVAL 7 DAY)  WHERE PERSON_CODE = '$code';";
}


///BAN RESERVE CLASS

$sql = "SELECT * FROM person where BAN_RESERVE is not null and NOW() >= BAN_RESERVE order by BAN_RESERVE";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

for($i=0 ; $i < $dataCount ; $i++)
{
  $code    = $rows[$i]['PERSON_CODE'];
  $sqlEx   .= "UPDATE person SET BAN_RESERVE = null WHERE PERSON_CODE = '$code';";
}


///PERSON_EXPIRE_DATE

$sql = "SELECT * FROM person where COMPANY_CODE  = '$company_code' and PERSON_EXPIRE_DATE < NOW() and PERSON_STATUS in ('A','Y') order by PERSON_EXPIRE_DATE";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

for($i=0 ; $i < $dataCount ; $i++)
{
  $code    = $rows[$i]['PERSON_CODE'];
  $sqlEx   .= "UPDATE person SET PERSON_STATUS = 'E' WHERE PERSON_CODE = '$code';";
}


///PERSON_EXPIRE_DATE

$sql = "SELECT * FROM trans_package_person where company_code  = '$company_code' and PERSON_EXPIRE_DATE < NOW() and PERSON_STATUS in ('A','Y') order by PERSON_EXPIRE_DATE";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

for($i=0 ; $i < $dataCount ; $i++)
{
  $code    = $rows[$i]['PERSON_CODE'];
  $sqlEx   .= "UPDATE person SET PERSON_STATUS = 'E' WHERE PERSON_CODE = '$code';";
}

//echo $sqlEx;
$query      = DbQuery($sqlEx,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail sql :'.$sqlEx)));
}

?>
