<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$flag = true;
$id                   = isset($_GET['id'])?$_GET['id']:"";
$memberCodeTransfer   = isset($_GET['memberCodeTransfer'])?$_GET['memberCodeTransfer']:"";
$memberCodeRecipient  = isset($_GET['memberCodeRecipient'])?$_GET['memberCodeRecipient']:"";
$user_id_update       = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";


$sql = "SELECT * FROM trans_package_person where 	id = '$id' and status not in ('D','T')";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$id                   = "";
$company_code         = "";
$person_code          = "";
$package_code         = "";
$package_name         = "";
$package_detail       = "";
$reg_no               = "";
$use_package          = "";
$num_use              = "";
$package_unit         = "";
$date_start           = "";
$date_expire          = "";
$max_use              = "";
$package_price        = "";
$package_num          = "";
$package_price_total  = "";
$percent_discount     = "";
$discount             = "";
$vat                  = "";
$type_vat             = "";
$net_total            = "";
$notify_num           = "";
$notify_unit          = "";
$type_package         = "";
$trainer_code         = "";
$trainer_name         = "";
$invoice_code         = "";
$receipt_code         = "";


if($dataCount > 0)
{
  $id                   = $row[0]['id'];
  $company_code         = $row[0]['company_code'];
  $person_code          = $row[0]['person_code'];
  $package_code         = $row[0]['package_code'];
  $package_name         = $row[0]['package_name'];
  $package_detail       = $row[0]['package_detail'];
  $reg_no               = $row[0]['reg_no'];
  $use_package          = $row[0]['use_package'];
  $num_use              = $row[0]['num_use'];
  $package_unit         = $row[0]['package_unit'];
  $date_start           = $row[0]['date_start'];
  $date_expire          = $row[0]['date_expire'];
  $max_use              = $row[0]['max_use'];
  $package_price        = $row[0]['package_price'];
  $package_num          = $row[0]['package_num'];
  $package_price_total  = $row[0]['package_price_total'];
  $percent_discount     = $row[0]['percent_discount'];
  $discount             = $row[0]['discount'];
  $vat                  = $row[0]['vat'];
  $type_vat             = $row[0]['type_vat'];
  $notify_num           = $row[0]['notify_num'];
  $notify_unit          = $row[0]['notify_unit'];
  $type_package         = $row[0]['type_package'];
  $net_total            = $row[0]['net_total'];
  $trainer_code         = $row[0]['trainer_code'];
  $trainer_name         = $row[0]['trainer_name'];
  $invoice_code         = $row[0]['invoice_code'];
  $receipt_code         = $row[0]['receipt_code'];


  $sqlUP = "UPDATE trans_package_person SET
          status              = 'T',
          update_by           = '$user_id_update',
          update_date         = NOW()
          WHERE id = '$id'";
  DbQuery($sqlUP,null);


  if($type_package == "MB")
  {
    $sqlPerson = "SELECT * FROM person where 	PERSON_CODE = '$memberCodeRecipient'";

    $queryps      = DbQuery($sqlPerson,null);
    $jsonps       = json_decode($queryps, true);
    $rowps        = $jsonps['data'];

    $PERSON_EXPIRE_DATE = $rowps[0]['PERSON_EXPIRE_DATE'];
    $PERSON_STATUS      = $rowps[0]['PERSON_STATUS'];
    if($PERSON_STATUS == "A")
    {
        $date         = $PERSON_EXPIRE_DATE;
        $date1        = str_replace('-', '/', $date);
        $date_start   = date('Y-m-d',strtotime($date1 . "+1 days"));
        $date_expire  = date('Y-m-d',strtotime($date_start . "+".$num_use." ".$package_unit));
    }

    //echo $date_start." >>> ".$date_expire;

    $sql2 = "UPDATE person SET
             PERSON_STATUS       = 'A',
             PERSON_EXPIRE_DATE  = '$date_expire',
             DATA_MODIFY_BY      = '$user_id_update'
             WHERE COMPANY_CODE  = '$company_code' and PERSON_CODE = '$memberCodeRecipient' ";

    $query2      = DbQuery($sql2,null);
    $row2        = json_decode($query2, true);

    if(intval($row2['errorInfo'][0]) != 0){
      $flag = false;
    }
  }

   $sql   = "INSERT INTO trans_package_person (
     company_code,
     person_code,
     package_code,
     package_name,
     package_detail,
     reg_no,
     use_package,
     num_use,
     package_unit,
     date_start,
     date_expire,
     max_use,
     package_price,
     package_num,
     package_price_total,
     percent_discount,
     discount,
     vat,
     type_vat,
     net_total,
     notify_num,
     notify_unit,
     type_package,
     trainer_code,
     trainer_name,
     invoice_code,
     receipt_code,
     status,
     create_by,
     create_date,
     pakage_transfer_id
    )
    VALUES (
      '$company_code',
      '$memberCodeRecipient',
      '$package_code',
      '$package_name',
      '$package_detail',
      '$reg_no',
      '$use_package',
      '$num_use',
      '$package_unit',
      '$date_start',
      '$date_expire',
      '$max_use',
      '$package_price',
      '$package_num',
      '$package_price_total',
      '$percent_discount',
      '$discount',
      '$vat',
      '$type_vat',
      '$net_total',
      '$notify_num',
      '$notify_unit',
      '$type_package',
      '$trainer_code',
      '$trainer_name',
      '$invoice_code',
      '$receipt_code',
      'A',
      '$user_id_update',
      NOW(),
      '$id'
      )";

  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($row['errorInfo'][0]) != 0){
    $flag = false;
  }
}

if($flag){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql)));
}

?>
