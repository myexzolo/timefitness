<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companyCode    = isset($_GET['branchcode'])?$_GET['branchcode']:"";


$sql = "SELECT ps.*, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as person_name,
p.PERSON_NICKNAME as person_nickname
FROM trans_package_person ps, person p
where ps.person_code = p.PERSON_CODE and ps.status = 'A'  and ps.company_code =  '$companyCode'
and  IF(ps.notify_unit = 'DAYS', (NOW() + INTERVAL ps.notify_num DAY) > ps.date_expire ,
IF(ps.notify_unit = 'TIMES', ps.notify_num > ps.use_package , NOW() >  ps.date_expire ))
order by ps.date_expire ASC";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else if (intval($errorInfo[0]) == 0 && $dataCount == 0){
  header('Content-Type: application/json');
  exit(json_encode(array()));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
