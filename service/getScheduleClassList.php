<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"GYMMK01";
// $date_start = $_GET['date_start'];
// $date_end   = $_GET['date_end'];

$date_start = dateThToEn($_GET['date_start'],"yyyy/mm/dd","/");
$date_end   = dateThToEn($_GET['date_end'],"yyyy/mm/dd","/");


$sql   = "SELECT sc.*,c.name_class,c.image_class,e.EMP_NICKNAME
          FROM tb_schedule_class_day sc, t_classes c, data_mas_employee e, tb_schedule_class s
          where  sc.date_class between '$date_start' and '$date_end' and sc.id_class = c.id_class and sc.EMP_CODE = e.EMP_CODE and s.is_active = 'Y'
          and s.schedule_id = sc.schedule_id and s.branch_code = '$branchcode' order by sc.date_class, sc.day, sc.row ";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail'.$sql)));
}
?>
