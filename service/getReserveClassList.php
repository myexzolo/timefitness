<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode       = isset($_GET['branchcode'])?$_GET['branchcode']:"GYMMK01";
$schedule_day_id  = isset($_GET['schedule_day_id'])?$_GET['schedule_day_id']:"";

$con = "";
$dateNow            = date('Y-m-d');

if($schedule_day_id != ""){
  $con = " and rc.schedule_day_id ='$schedule_day_id' ";
}else{
  $con = " and rc.expire_date >=  '$dateNow' ";
}

$sql = "SELECT rc.*,scd.*,e.EMP_CODE,e.EMP_NICKNAME, c.name_class, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as person_name, p.PERSON_NICKNAME, p.PERSON_TEL_MOBILE
FROM t_reserve_class rc, tb_schedule_class_day scd, t_classes c, data_mas_employee e, person p
WHERE rc.branch_code = '$branchcode' and rc.PERSON_CODE = p.PERSON_CODE and rc.status in ('C','S', 'P')
and rc.schedule_day_id = scd.id and scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE $con
order by scd.date_class, scd.day, scd.row, rc.date_reserve ";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}
?>
