<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$typeActive = isset($_GET['typeActive'])?$_GET['typeActive']:"";
$reserve_id         = isset($_GET['reserve_id'])?$_GET['reserve_id']:"";
$schedule_day_id    = isset($_GET['schedule_day_id'])?$_GET['schedule_day_id']:"";
$PERSON_CODE        = isset($_GET['PERSON_CODE'])?$_GET['PERSON_CODE']:"";
$person_name        = isset($_GET['person_name'])?$_GET['person_name']:"";
$company_code       = isset($_GET['company_code'])?$_GET['company_code']:"";
$user_id_update     = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";
$status             = isset($_GET['status'])?$_GET['status']:"";
$expire_date        = isset($_GET['expire_date'])?$_GET['expire_date']:"";
$person_join        = isset($_GET['person_join'])?$_GET['person_join']:"";

// --ADD EDIT DELETE Module--
$sql = "";
$data = "0";
$message = "Success";
if($typeActive == "ADD"){

  $data = 0;
  if($status == "C"){
    $sqls = "SELECT COUNT(person_code) + 1 as num FROM  t_reserve_class WHERE schedule_day_id = '$schedule_day_id' and status = 'C'";
    $queryr      = DbQuery($sqls,null);
    $jsonr       = json_decode($queryr, true);
    $rowr        = $jsonr['data'];
    $data        = $rowr[0]['num'];
  }

  $sqls = "SELECT * FROM  tb_schedule_class_day  WHERE id = '$schedule_day_id'";
  $queryr      = DbQuery($sqls,null);
  $jsonr       = json_decode($queryr, true);
  $rowr        = $jsonr['data'];
  $unit        = $rowr[0]['unit'];
  $person_join = $rowr[0]['person_join'];


  if($person_join >= $unit)
  {
    $status = "P";
  }

  $sql   = "INSERT INTO t_reserve_class (
    schedule_day_id,PERSON_CODE,date_reserve,
    status,	expire_date,branch_code,create_by,create_date,join_seq,update_by,update_date)
     VALUES(
     '$schedule_day_id','$PERSON_CODE', NOW(),
     '$status','$expire_date','$company_code','$user_id_update',NOW(),'$data','$user_id_update',NOW());";

   $sql  .= "UPDATE tb_schedule_class_day SET person_join = person_join + 1
             WHERE id = '$schedule_day_id';";
}
else if($typeActive == "EMP")
{
  $sql  = "UPDATE tb_schedule_class_day SET sign_emp = 'Y' WHERE id = '$schedule_day_id';";
}
else if($typeActive == "CANCEL")
{
  $sql  = "UPDATE tb_schedule_class_day SET sign_emp = 'C' WHERE id = '$schedule_day_id';";
}
else if($typeActive == "CHANGE")
{
  $sqls = "SELECT COUNT(person_code) + 1 as num FROM  t_reserve_class WHERE schedule_day_id = '$schedule_day_id' and status = 'C'";
  $queryr      = DbQuery($sqls,null);
  $jsonr       = json_decode($queryr, true);
  $rowr        = $jsonr['data'];
  $data        = $rowr[0]['num'];

  $sql = "UPDATE t_reserve_class SET
          status   = 'C',
          join_seq = '$data',
          update_by   = '$user_id_update',
          update_date     = NOW()
          WHERE reserve_id = '$reserve_id';";
}
else if($typeActive == "DEL")
{
  $sqls = "SELECT sc.*, c.name_class
  FROM  tb_schedule_class_day  sc, t_classes c
  WHERE id = '$schedule_day_id' and sc.id_class = c.id_class";
  $queryr      = DbQuery($sqls,null);
  $jsonr       = json_decode($queryr, true);
  $rowr        = $jsonr['data'];
  $unit        = $rowr[0]['unit'];
  $person_join = $rowr[0]['person_join'];
  $date_class  = $rowr[0]['date_class'];
  $time_start  = $rowr[0]['time_start'];
  $time_end    = $rowr[0]['time_end'];
  $name_class  = $rowr[0]['name_class'];
  $timeClass   = $time_start." - ".$time_end;

  $sql = "UPDATE t_reserve_class SET
          status   = 'D',
          update_by   = '$user_id_update',
          update_date     = NOW()
          WHERE reserve_id = '$reserve_id';";

  $sql  .= "UPDATE tb_schedule_class_day SET person_join = person_join - 1
            WHERE id = '$schedule_day_id';";


  if($person_join > $unit)
  {
      $sqls = "SELECT r.reserve_id,r.branch_code,p.PERSON_CODE,p.PERSON_TITLE,
                p.PERSON_NICKNAME, p.PERSON_NAME, p.PERSON_LASTNAME,
                p.PERSON_NAME_MIDDLE, p.PERSON_TITLE_ENG, p.PERSON_NAME_ENG,
                p.PERSON_LASTNAME_ENG, p.PERSON_SEX, p.PERSON_BIRTH_DATE,
                p.PERSON_TEL_MOBILE,p.PERSON_EMAIL
               FROM t_reserve_class r, person p
               WHERE r.PERSON_CODE = p.PERSON_CODE and r.schedule_day_id = '$schedule_day_id' and r.status = 'P' ORDER BY r.date_reserve ASC  Limit 0,1";
      $queryr      = DbQuery($sqls,null);
      $jsonr       = json_decode($queryr, true);
      $dataCountr  = $jsonr['dataCount'];

      if($dataCountr > 0)
      {
        $rowr        = $jsonr['data'];
        $branchCode  = $rowr[0]['branch_code'];
        $personCode  = $rowr[0]['PERSON_CODE'];
        $nickName    = $rowr[0]['PERSON_NICKNAME'];
        $fname       = $rowr[0]['PERSON_NAME']." ".$rowr[0]['PERSON_LASTNAME'];
        $tel         = $rowr[0]['PERSON_TEL_MOBILE'];
        $email       = $rowr[0]['PERSON_EMAIL'];
        $reserveId   = $rowr[0]['reserve_id'];


        $sql .= "UPDATE t_reserve_class SET
                status   = 'S',
                join_seq = '0',
                update_by   = '$user_id_update',
                update_date     = NOW()
                WHERE reserve_id = '$reserveId';";

        $sqlb = "SELECT * FROM t_branch WHERE is_active ='Y' and branch_code = '$branchCode'";

        $queryb      = DbQuery($sqlb,null);
        $jsonb       = json_decode($queryb, true);
        $rowsb       = $jsonb['data'];
        $dataCountb  = $jsonb['dataCount'];

        if($dataCountb > 0){
          $message   = "\r\n"."Class ".$name_class." วันที่ ".DateThai($date_class)." ".$timeClass." ถูกยกเลิกการจอง !!\r\n";
          $message  .= "ติดต่อคุณ : ".$fname." เพื่อยืนยันสิทธิการเข้าคลาส\r\n";
          $message  .= "ชื่อเล่น : ".$nickName."\r\n";
          $message  .= "เบอร์โทรติดต่อ : ".$tel."\r\n";
          $message  .= "E-mail : ".$email;

          $token =  $rowsb[0]['token_line'];

          //echo "message  ".$message;
          LINE_NOTIFY_MESSAGE($message,"",$token);
      }
    }
  }
}
else if($typeActive == "EXP")
{
  $sql = "UPDATE t_reserve_class SET
          status   = 'E',
          update_by   = '$user_id_update',
          update_date     = NOW()
          WHERE reserve_id = '$reserve_id';";

  $sql  .= "UPDATE tb_schedule_class_day SET person_join = person_join - 1
            WHERE id = '$schedule_day_id';";
}
//echo $sql."<br>";
//--ADD EDIT Package-- //

//echo    " sql : ".$sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => $message,'data' => $data)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql)));
}

?>
