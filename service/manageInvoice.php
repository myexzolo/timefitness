<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$typeActive       = isset($_GET['typeActive'])?$_GET['typeActive']:"";
$companyId        = isset($_GET['companyId'])?$_GET['companyId']:"";
$invoice_id       = isset($_GET['invoice_id'])?$_GET['invoice_id']:"";
$company_code     = isset($_GET['company_code'])?$_GET['company_code']:"";
$invoice_code     = isset($_GET['invoice_code'])?$_GET['invoice_code']:"";
$invoice_date     = isset($_GET['invoice_date'])?$_GET['invoice_date']:"";
$name             = isset($_GET['name'])?$_GET['name']:"";
$address          = isset($_GET['address'])?$_GET['address']:"";
$tel              = isset($_GET['tel'])?$_GET['tel']:"";
$fax              = isset($_GET['fax'])?$_GET['fax']:"";
$tax              = isset($_GET['tax'])?$_GET['tax']:"";
$cash             = isset($_GET['cash'])?$_GET['cash']:"";
$transfer         = isset($_GET['transfer'])?$_GET['transfer']:"";
$credit           = isset($_GET['credit'])?$_GET['credit']:"";
$cheque           = isset($_GET['cheque'])?$_GET['cheque']:"";
$type_payment     = isset($_GET['type_payment'])?$_GET['type_payment']:"";
$total_price      = isset($_GET['total_price'])?$_GET['total_price']:"0";
$discount         = isset($_GET['discount'])?$_GET['discount']:"0";
$vat              = isset($_GET['vat'])?$_GET['vat']:"0";
$total_net        = isset($_GET['total_net'])?$_GET['total_net']:"0";
$receipt          = isset($_GET['receipt'])?$_GET['receipt']:"";
$num_print        = isset($_GET['num_print'])?$_GET['num_print']:"0";

$create_by        = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";

$data = "";

if($typeActive == "ADD"){
  $sql   = "INSERT INTO tb_invoice (
            company_code, invoice_code, invoice_date,
            name, address, tel,
            fax, tax, type_payment,
            total_price, discount, vat,
            total_net, create_by, create_date, receipt,status ,cash ,transfer ,credit ,cheque,num_print)
            VALUES (
            '$company_code', '$invoice_code', '$invoice_date',
            '$name', '$address', '$tel',
            '$fax', '$tax','$type_payment',
            '$total_price','$discount', '$vat',
            '$total_net','$create_by', NOW(), '$receipt','A' ,'$cash','$transfer','$credit','$cheque','$num_print')";

}else if($typeActive == "EDIT"){
  $sql = "UPDATE tb_invoice SET
          invoice_date  = '$invoice_date',
          name          = '$name',
          address       = '$address',
          tel           = '$tel',
          fax           = '$fax',
          tax           = '$tax',
          type_payment  = '$type_payment',
          total_price   = '$total_price',
          discount      = '$discount',
          vat           = '$vat',
          total_net     = '$total_net',
          receipt       = '$receipt',
          cash          = '$cash',
          transfer      = '$transfer',
          credit        = '$credit',
          cheque        = '$cheque',
          num_print     = '$num_print',
          update_by     = '$create_by',
          update_date   = NOW()
          WHERE invoice_id = '$invoice_id'";
}else if($typeActive == "PRINT"){
  $sql = "UPDATE tb_invoice SET
          num_print     = '$num_print',
          update_by     = '$create_by',
          update_date   = NOW()
          WHERE invoice_id = '$invoice_id'";
}else if($typeActive == "PRINTINV"){
  if($type_payment == "GM")
  {
    $companyId  = sprintf("%02d", $companyId);
    $code       = "INV".$companyId.date("ym",strtotime($invoice_date));
    $sql = "SELECT max(invoice_code) as invoice_code FROM tb_invoice where company_code ='$company_code' and invoice_code like '$code%' ";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $row        = $json['data'];
    $dataCount  = $json['dataCount'];

    $inv = "";
    if($dataCount > 0){
      $no       = $row[0]['invoice_code'];
      $lastNum  = substr($no,strlen($code));
      $lastNum  = $lastNum + 1;
      $inv = $code.sprintf("%03d", $lastNum);
    }else{
      $inv = $code."001";
    }

    $sql = "SELECT  * FROM tb_invoice where company_code ='$company_code' and invoice_id = '$invoice_id'";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $row        = $json['data'];

    $total_price = $row[0]['total_price'];

    $vat          = round(($total_price*7)/107,2);
    $total_price  = ($total_price - $vat);

    $sql = "UPDATE trans_package_person SET
    	      invoice_code  = '$inv',
            update_by     = '$create_by',
            update_date   = NOW()
            WHERE invoice_code = '$invoice_code';";

   $sql .= "UPDATE tb_invoice SET
  	      invoice_code  = '$inv',
          num_print     = '$num_print',
          total_price   = '$total_price',
          vat           = '$vat',
          type_payment  = 'INV',
          update_by     = '$create_by',
          update_date   = NOW()
          WHERE invoice_id = '$invoice_id';";

    $data = $inv."|".$vat."|".$total_price;
  }else{
    $sql = "UPDATE tb_invoice SET
            num_print     = '$num_print',
            update_by     = '$create_by',
            update_date   = NOW()
            WHERE invoice_id = '$invoice_id'";
  }
}
else if($typeActive == "DEL")
{
  $sql = "UPDATE tb_invoice SET
          status  = 'D',
          update_by       = '$create_by',
          update_date     = NOW()
          WHERE invoice_id = '$invoice_id';";

  $sql .= "UPDATE trans_package_person SET
          status  = 'D',
          update_by       = '$create_by',
          update_date     = NOW()
          WHERE invoice_code = '$invoice_code';";
}
//echo $sql."<br>";
//--ADD EDIT Package-- //
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success :'.$sql, 'data' => $data)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql)));
}

?>
