<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$sql = "SELECT package_name as title,package_detail as content,	start_pro_date as start_date,end_pro_date as end_date,package_price as price,company_code as branch_code,
        CONCAT('https://timefitnessbkk.com/admin/image/promotions/mobile/',img) as image,
        CONCAT('http://timefitnessbkk.com/payment/index.php?pid=',package_id) as web
        FROM data_mas_package WHERE package_status ='Y' and promotion in ('N','M','A') and show_feature = 'Y' ORDER BY create_date DESC Limit 0,3";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];

header('Content-Type: application/json');
exit(json_encode($row));


?>
