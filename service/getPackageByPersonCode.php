<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companyCode  = isset($_GET['companyCode'])?$_GET['companyCode']:"";
$personCode   = isset($_GET['personCode'])?$_GET['personCode']:"";

$dateNow = date("Y/m/d H:i:s");

$branch = "";
if($companyCode == "GYMMK01")
{
  $branch = " mp.branch1 = 'Y' ";
}
else if ($companyCode == "GYMMK02")
{
  $branch = " mp.branch2 = 'Y' ";
}
else if ($companyCode == "GYMMK03")
{
  $branch = " mp.branch3 = 'Y' ";
}
else if ($companyCode == "GYMMK04")
{
  $branch = " mp.branch4 = 'Y' ";
}
else if ($companyCode == "GYMMK05")
{
  $branch = " mp.branch5 = 'Y' ";
}

$sql = "SELECT ps.*, iv.invoice_date,(SELECT COUNT(cp.id)
FROM trans_checkin_person cp
WHERE cp.person_code = '$personCode' and cp.staus_checkin not in ('D') AND cp.package_person_id = ps.id) as num_checkin
FROM trans_package_person ps
INNER JOIN  data_mas_package mp ON ps.package_code = mp.package_code
LEFT JOIN   tb_invoice iv ON ps.invoice_code = iv.invoice_code and iv.status = 'A'
where  (ps.company_code = '$companyCode' or $branch)
and ps.person_code = '$personCode' and 	ps.status in ('A','U') and ps.date_expire > '$dateNow'
order by ps.status ASC, ps.date_expire ASC";


//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else if (intval($errorInfo[0]) == 0 && $dataCount == 0){
  header('Content-Type: application/json');
  exit(json_encode(array()));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail'.$sql)));
}

?>
