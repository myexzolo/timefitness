<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$typeSearch = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";

$con = "";

$dateNow = date("Y-m-d");

$con2 = "";
$con3  = "and pp.type_package = 'MB'";
if($typeSearch == "TODAY"){
  $con .= " and DATE_FORMAT(p.PERSON_REGISTER_DATE,'%Y-%m-%d') = '$dateNow'";
  $con2 .= " and DATE_FORMAT(ps.PERSON_REGISTER_DATE,'%Y-%m-%d') = '$dateNow'";
}else if($typeSearch == "ACTIVE"){
  $con .= " and p.PERSON_STATUS in ('Y','A')";
  $con2 .= " and ps.PERSON_STATUS in ('Y','A')";
}else if($typeSearch == "MEMBER"){
  $con .= " and p.PERSON_STATUS not in ('A')";
  $con2 .= " and ps.PERSON_STATUS not in ('A')";
}else if($typeSearch == "EXPIRE"){
  $con .= " and p.PERSON_STATUS not in ('Y','A')";
  $con2 .= " and ps.PERSON_STATUS not in ('Y','A')";
}else if($typeSearch == "TRAINER"){
  $con3 .= " and pp.type_package = 'PT' AND status in ('A','E')";
}

$branch = "";
if($companycode == "GYMMK01")
{
  $branch = " and mp.branch1 = 'Y' ";
}
else if ($companycode == "GYMMK02")
{
  $branch = " and mp.branch2 = 'Y' ";
}
else if ($companycode == "GYMMK03")
{
  $branch = " and mp.branch3 = 'Y' ";
}
else if ($companycode == "GYMMK04")
{
  $branch = " and mp.branch4 = 'Y' ";
}
else if ($companycode == "GYMMK05")
{
  $branch = " and mp.branch5 = 'Y' ";
}


$sqlPerson = "OR (p.PERSON_CODE in (SELECT  distinct pp.PERSON_CODE
              FROM trans_package_person pp
              INNER JOIN person ps ON pp.PERSON_CODE = ps.PERSON_CODE  $con2
              INNER JOIN data_mas_package mp ON pp.package_code = mp.package_code  $branch
              WHERE  pp.status  = 'A' $con3 and pp.date_expire > '$dateNow' and pp.COMPANY_CODE != '$companycode'))";

if($typeSearch == "TRAINER")
{
  $sqlPerson = " ";

  $sql = "SELECT p.COMPANY_CODE,p.PERSON_CODE,p.PERSON_TITLE,
          p.PERSON_NICKNAME, p.PERSON_NAME, p.PERSON_LASTNAME,
          p.PERSON_NAME_MIDDLE, p.PERSON_TITLE_ENG, p.PERSON_NAME_ENG,
          p.PERSON_LASTNAME_ENG, p.PERSON_SEX, p.PERSON_BIRTH_DATE,
          p.PERSON_GRADE, p.PERSON_PROVINCE_CODE,p.PERSON_ER_TEL,
          p.PERSON_CARD_ID, p.PERSON_CARD_SDATE,p.PERSON_EMAIL,
          p.PERSON_CARD_EDATE,p.PERSON_CARD_CREATE_BY,
          p.PERSON_TEL_MOBILE, p.PERSON_TEL_MOBILE2, p.PERSON_TEL_OFFICE,
          p.PERSON_FAX, p.PERSON_IMAGE,p.PERSON_GROUP_AGE, p.PERSON_GROUP,
          p.PERSON_STATUS, p.PERSON_REGISTER_DATE, p.PERSON_EXPIRE_DATE,
          p.PERSON_NOTE, p.PERSON_LAST_VISIT
          FROM person p
          where p.COMPANY_CODE = '$companycode'
          and p.PERSON_CODE in (SELECT DISTINCT pp.PERSON_CODE FROM trans_package_person pp WHERE pp.type_package = 'PT' AND pp.status in ('A','E') and pp.COMPANY_CODE = '$companycode')
          ORDER BY p.PERSON_LAST_VISIT DESC";

}else{
  $sql = "SELECT p.COMPANY_CODE,p.PERSON_CODE,p.PERSON_TITLE,
          p.PERSON_NICKNAME, p.PERSON_NAME, p.PERSON_LASTNAME,
          p.PERSON_NAME_MIDDLE, p.PERSON_TITLE_ENG, p.PERSON_NAME_ENG,
          p.PERSON_LASTNAME_ENG, p.PERSON_SEX, p.PERSON_BIRTH_DATE,
          p.PERSON_GRADE, p.PERSON_PROVINCE_CODE,
          p.PERSON_CARD_ID, p.PERSON_CARD_SDATE,p.PERSON_ER_TEL,
          p.PERSON_CARD_EDATE,p.PERSON_CARD_CREATE_BY,
          p.PERSON_TEL_MOBILE, p.PERSON_TEL_MOBILE2, p.PERSON_TEL_OFFICE,
          p.PERSON_FAX, p.PERSON_IMAGE,p.PERSON_GROUP_AGE, p.PERSON_GROUP,
          p.PERSON_STATUS, p.PERSON_REGISTER_DATE, p.PERSON_EXPIRE_DATE,
          p.PERSON_NOTE, p.PERSON_LAST_VISIT,p.PERSON_EMAIL,
          p.PERSON_ADDRESS_ZONE, p.PERSON_HOME1_ADDR1, p.PERSON_HOME1_SUBDISTRICT,
          p.PERSON_HOME1_DISTRICT, p.PERSON_HOME1_PROVINCE, p.PERSON_HOME1_COUNTRY,
          p.PERSON_HOME1_POSTAL, p.PERSON_HOME1_ADDR2, p.PERSON_HOME2_SUBDISTRICT,
          p.PERSON_HOME2_DISTRICT, p.PERSON_HOME2_PROVINCE, p.PERSON_HOME2_COUNTRY,
          p.PERSON_HOME2_POSTAL, p.PERSON_BILL_TYPE, p.PERSON_BILL_NAME,
          p.PERSON_BILL_ADDR1, p.PERSON_BILL_ADDR2, p.PERSON_BILL_SUBDISTRICT,
          p.PERSON_BILL_DISTRICT, p.PERSON_BILL_PROVINCE, p.PERSON_BILL_POSTAL,
          p.PERSON_BILL_COUNTRY, p.PERSON_BILL_TAXNO, p.PERSON_ACC_NO,
          p.PERSON_SALE_CODE, p.FNG_TEMPLATE1,
          p.FNG_TEMPLATE2, p.FNG_TEMPLATE3, p.FNG_TEMPLATE4, p.FNG_TEMPLATE5,
          p.DATA_STATUS,p.DATA_CREATE_DATE, p.DATA_CREATE_BY,
          p.DATA_DELETE_DATE,p.DATA_DELETE_BY,p.EMP_CODE_SALE,p.BAN_RESERVE
          FROM person p
          where (p.COMPANY_CODE = '$companycode' $con) $sqlPerson
          ORDER BY p.PERSON_LAST_VISIT DESC";


}

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

//echo ">>>>>>>>>>>>>>>>>>>>>>>".$dataCount;

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
