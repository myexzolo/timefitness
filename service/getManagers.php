<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";


$sql = "SELECT * FROM data_mas_employee
where COMPANY_CODE ='$companycode' AND EMP_IS_STAFF = 'Y' and DATA_DELETE_STATUS = 'N' order by EMP_CODE";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
