<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$typeSearch = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";

$con = "";

$conB = "";

if($companycode == "GYMMK01"){
  $conB = " OR e.branch1 ='Y' ";
}else if($companycode == "GYMMK02"){
  $conB = " OR e.branch2 ='Y' ";
}else if($companycode == "GYMMK03"){
  $conB = " OR e.branch3 ='Y' ";
}else if($companycode == "GYMMK04"){
  $conB = " OR e.branch4 ='Y' ";
}else if($companycode == "GYMMK05"){
  $conB = " OR e.branch5 ='Y' ";
}

$dateNow = date("Y-m-d");


if($typeSearch == "TODAY"){
  $con .= " and DATE_FORMAT(e.EMP_DATE_INCOME,'%Y-%m-%d') = '$dateNow'";
}else if($typeSearch == "ACTIVE"){
  $con .= " and e.EMP_DATE_RETRY  is null and DATA_DELETE_STATUS != 'Y' ";
}else if($typeSearch == "EXPIRE"){
  $con .= " and e.EMP_DATE_RETRY is not null ";
}



$sql = "SELECT
      e.COMPANY_CODE,
      e.EMP_CODE,
      e.EMP_TITLE,
      e.EMP_NAME,
      e.EMP_LASTNAME,
      e.EMP_POSITION,
      e.EMP_DEPARTMENT,
      e.EMP_SEX,
      DATE_FORMAT(e.EMP_BIRTH_DATE, '%d/%m/%Y') as EMP_BIRTH_DATE,
      e.EMP_TYPE,
      e.EMP_GROUP,
      e.EMP_MONEY_CODE,
      e.EMP_PAYROLL,
      e.EMP_TYPE_COMMS,
      e.EMP_RATE_COMMS,
      e.EMP_FIXED_COMMS,
      e.EMP_COMMS_SUM_GROUP,
      e.EMP_GROUP_AUTHEN,
      e.EMP_USER,
      e.EMP_PSW,
      e.EMP_PSW_EDIT,
      e.EMP_PSW_DELETE,
      e.EMP_PSW_CANCEL,
      e.EMP_NICKNAME,
      e.EMP_CARD_ID,
      e.EMP_PKS_NO,
      e.EMP_TAX_NO,
      e.EMP_LICENSE_NO,
      e.EMP_SHIFT,
      e.EMP_NATION,
      e.EMP_RELIGION,
      e.EMP_ADDRESS1,
      e.EMP_ADDRESS2,
      e.EMP_ADDR_SUBDISTRICT,
      e.EMP_ADDR_DISTRICT,
      e.EMP_ADDR_PROVINCE,
      e.EMP_ADDR_PROVINCE_CODE,
      e.EMP_ADDR_POSTAL,
      e.EMP_DOC_NO,
      e.EMP_EMAIL,
      e.EMP_TEL,
      DATE_FORMAT(e.EMP_DATE_INCOME, '%d/%m/%Y') as EMP_DATE_INCOME,
      DATE_FORMAT(e.EMP_DATE_RETRY, '%d/%m/%Y') as EMP_DATE_RETRY,
      e.EMP_PICTURE,
      e.EMP_IS_STAFF,
      e.EMP_IS_TRAINER,
      e.EMP_IS_DOCTOR,
      e.EMP_IS_SALE,
      e.EMP_IS_INSTRUCTOR,
      e.FNG_TEMPLATE1,
      e.FNG_TEMPLATE2,
      e.FNG_TEMPLATE3,
      e.FNG_TEMPLATE4,
      e.FNG_TEMPLATE5,
      e.branch1,
      e.branch2,
      e.branch3,
      e.branch4,
      e.branch5,
      u.user_img
FROM data_mas_employee  e LEFT JOIN t_user u
ON e.EMP_CODE = u.user_login
where (e.COMPANY_CODE = '$companycode' $conB) $con and e.EMP_CODE <> '0000'
and e.DATA_DELETE_STATUS = 'N' order by e.EMP_CODE";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
