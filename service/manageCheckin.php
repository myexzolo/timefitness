<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$typeActive = isset($_GET['typeActive'])?$_GET['typeActive']:"";

$id                 = isset($_GET['id'])?$_GET['id']:"";
$company_code       = isset($_GET['company_code'])?$_GET['company_code']:"";
$person_code        = isset($_GET['person_code'])?$_GET['person_code']:"";
$package_person_id  = isset($_GET['package_person_id'])?$_GET['package_person_id']:"";
$sign_person        = isset($_GET['sign_person'])?$_GET['sign_person']:"";
$sign_emp           = isset($_GET['sign_emp'])?$_GET['sign_emp']:"";
$sign_manager       = isset($_GET['sign_manager'])?$_GET['sign_manager']:"";
$status             = isset($_GET['status'])?$_GET['status']:"";
$use_package        = isset($_GET['use_package'])?$_GET['use_package']:"";
$trainer_code       = isset($_GET['trainer_code'])?$_GET['trainer_code']:"";
$manager_code       = isset($_GET['manager_code'])?$_GET['manager_code']:"";

//typeActive,id,company_code,person_code,package_person_id

// company_code
// person_code
// package_person_id
// sign_person
// sign_emp
// sign_manager
// sign_person_date
// sign_emp_date
// sign_manager_date
// create_date
// create_by
// update_date
// update_by
// staus_checkin


$user_id_update   = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";


// --ADD EDIT DELETE Module--

if($typeActive == "ADD"){
  $sql   = "INSERT INTO trans_checkin_person (
    checkin_date,company_code,person_code,
    package_person_id,create_date,create_by,
    staus_checkin)
     VALUES(
        NOW(),'$company_code','$person_code',
       '$package_person_id',  NOW(),'$user_id_update',
       'CI');";

   $sql2   = "UPDATE person SET PERSON_LAST_VISIT = NOW()
              WHERE COMPANY_CODE = '$company_code' and PERSON_CODE = '$person_code' ";
   $query2 = DbQuery($sql2,null);
}else if($typeActive == "EDIT"){
  $con = "";
  if($sign_person == "Y"){
    $con .= "sign_person = '$sign_person',sign_person_date = NOW(),";
  }
  if($sign_emp == "Y"){
    $con .= "sign_emp = '$sign_emp',sign_emp_date = NOW(),trainer_code ='$trainer_code',";
  }
  if($sign_manager == "Y"){
    $con .= "sign_manager= '$sign_manager',sign_manager_date = NOW(),manager_code ='$manager_code',";

    $sqlpp = "SELECT * FROM trans_package_person WHERE id = '$package_person_id'";

    $querypp      = DbQuery($sqlpp,null);
    $jsonpp       = json_decode($querypp, true);
    $rowpp        = $jsonpp['data'];
    $price_per_use = $rowpp[0]['price_per_use'];

    $con .= "price_per_use= '$price_per_use',";
  }
  if($status == "Y"){
    $con .= "staus_checkin= 'CO',";

    $con2 = "";
    if($use_package == 0){
      $con2 = "status = 'U',";
    }
    $sql2 = "UPDATE trans_package_person SET
            use_package     = '$use_package',
            update_by       = '$user_id_update',
            $con2
            update_date     = NOW()
            WHERE id = '$package_person_id'";
    //echo    " sql2 : ".$sql2;
    DbQuery($sql2,null);
  }

  $sql = "UPDATE trans_checkin_person SET
          $con
          update_by       = '$user_id_update',
          use_pack        = '$use_package',
          update_date     = NOW()
          WHERE id = '$id'";

  //echo    " sql : ".$sql;
}
else if($typeActive == "DEL")
{
  $sql = "UPDATE trans_checkin_person SET
          staus_checkin   = 'D',
          update_by       = '$user_id_update',
          update_date     = NOW()
          WHERE id = '$id'";
}
//echo $sql."<br>";
//--ADD EDIT Package-- //

//echo    " sql : ".$sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql."<br>".$sql2)));
}

?>
