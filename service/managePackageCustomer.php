<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$typeActive           = isset($_GET['typeActive'])?$_GET['typeActive']:"";
$id                   = isset($_GET['id'])?$_GET['id']:"";
$company_code         = isset($_GET['company_code'])?$_GET['company_code']:"";
$person_code          = isset($_GET['person_code'])?$_GET['person_code']:"";
$package_code         = isset($_GET['package_code'])?$_GET['package_code']:"";
$package_name         = isset($_GET['package_name'])?$_GET['package_name']:"";
$package_detail       = isset($_GET['package_detail'])?$_GET['package_detail']:"";
$type_package         = isset($_GET['type_package'])?$_GET['type_package']:"";
$reg_no               = isset($_GET['reg_no'])?$_GET['reg_no']:"";
$num_use              = isset($_GET['num_use'])?$_GET['num_use']:"";
$package_unit         = isset($_GET['package_unit'])?$_GET['package_unit']:"";
$date_start           = isset($_GET['date_start'])?$_GET['date_start']:"";
$date_expire          = isset($_GET['date_expire'])?$_GET['date_expire']:"";
$max_use              = isset($_GET['max_use'])?$_GET['max_use']:"";
$package_price        = isset($_GET['package_price'])?$_GET['package_price']:"";
$package_num          = isset($_GET['package_num'])?$_GET['package_num']:"";
$package_price_total  = isset($_GET['package_price_total'])?$_GET['package_price_total']:"";
$percent_discount     = isset($_GET['percent_discount'])?$_GET['percent_discount']:"0";
$discount             = isset($_GET['discount'])?$_GET['discount']:"0";
$vat                  = isset($_GET['vat'])?$_GET['vat']:"0";
$type_vat             = isset($_GET['type_vat'])?$_GET['type_vat']:"";
$notify_num           = isset($_GET['notify_num'])?$_GET['notify_num']:"";
$notify_unit          = isset($_GET['notify_unit'])?$_GET['notify_unit']:"";
$net_total            = isset($_GET['net_total'])?$_GET['net_total']:"";
$trainer_code         = isset($_GET['trainer_code'])?$_GET['trainer_code']:"";
$trainer_name         = isset($_GET['trainer_name'])?$_GET['trainer_name']:"";
$invoice_code         = isset($_GET['invoice_code'])?$_GET['invoice_code']:"";
$receipt_code         = isset($_GET['receipt_code'])?$_GET['receipt_code']:"";
$create_by            = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";
$type_buy             = isset($_GET['type_buy'])?$_GET['type_buy']:"";
$sale_code            = isset($_GET['sale_code'])?$_GET['sale_code']:"";
$price_per_use        = 0;


// --ADD EDIT DELETE Module--
$flag = true;
if($typeActive == "ADD"){
  if($package_unit == 'TIMES' && $net_total > 0){
    $price_per_use = (chkNum($net_total)/chkNum($num_use));
  }
  //echo $package_unit;
  $sql   = "INSERT INTO trans_package_person (
    company_code,person_code,package_code,
    package_name,package_detail,reg_no,
    num_use,package_unit, date_start,
    date_expire, max_use,type_package,
    package_price, package_num, package_price_total,
    percent_discount, discount, vat, type_vat,
    net_total, trainer_code, trainer_name,
    invoice_code, receipt_code, create_by,
    notify_num, notify_unit,
    create_date, status,use_package,price_per_use,type_buy,sale_code)
    VALUES (
    '$company_code', '$person_code', '$package_code',
    '$package_name', '$package_detail', '$reg_no',
    '$num_use', '$package_unit', '$date_start',
    '$date_expire', '$max_use', '$type_package',
    '$package_price', '$package_num', '$package_price_total',
    '$percent_discount', '$discount', '$vat', '$type_vat',
    '$net_total', '$trainer_code', '$trainer_name',
    '$invoice_code', '$receipt_code', '$create_by',
    '$notify_num', '$notify_unit',
    NOW(), 'A','$num_use','$price_per_use','$type_buy','$sale_code')";


  if($type_package == "MB")
  {
    $sql2 = "UPDATE person SET
            PERSON_STATUS       = 'A',
            PERSON_EXPIRE_DATE  = '$date_expire',
            DATA_MODIFY_BY      = '$create_by'
            WHERE COMPANY_CODE  = '$company_code' and PERSON_CODE = '$person_code' ";

    $query2      = DbQuery($sql2,null);
    $row2        = json_decode($query2, true);
    //$errorInfo2  = $row2['errorInfo'][0];

    if(intval($row2['errorInfo'][0]) != 0){
      $flag = false;
    }
  }

}else if($typeActive == "EDIT"){
  $sql = "UPDATE trans_package_person SET
          company_code        = '$company_code',
          person_code         = '$person_code',
          package_code        = '$package_code',
          package_name        = '$package_name',
          package_detail      = '$package_detail',
          type_package        = '$type_package,',
          reg_no              = '$reg_no',
          use_package         = '$num_use',
          num_use             = '$num_use',
          package_unit        = '$package_unit',
          date_start          = '$date_start',
          date_expire         = '$date_expire',
          max_use             = '$max_use',
          package_price       = '$package_price',
          package_num         = '$package_num',
          package_price_total = '$cpackage_price_total',
          percent_discount    = '$percent_discount',
          discount            = '$discount',
          vat                 = '$vat',
          type_vat            = '$type_vat',
          net_total           = '$net_total',
          notify_unit         = '$notify_unit',
          notify_num          = '$notify_num',
          trainer_code        = '$trainer_code',
          trainer_name        = '$trainer_name',
          invoice_code        = '$invoice_code',
          receipt_code        = '$receipt_code',
          type_buy            = '$type_buy',
          sale_code           = '$sale_code',
          status              = 'E',
          update_by           = '$create_by',
          update_date         = NOW()
          WHERE id = '$id'";
}
else if($typeActive == "DEL")
{
  $sql = "UPDATE trans_package_person SET
          status  = 'D',
          update_by       = '$create_by',
          update_date     = NOW()
          WHERE id = '$id'";
}
//echo $sql."<br>";
//--ADD EDIT Package-- //
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0 && $flag){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success:'.$sql)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql)));
}

?>
