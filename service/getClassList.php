<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"GYMMK01";

date_default_timezone_set("Asia/Bangkok");
$date = date("Y/m/d");

// $date_start = "2019/07/01";
// $date_end = "2019/08/31";

$date_start = $_GET['date_start'];
$date_end = $_GET['date_end'];

$sqlG = "SELECT sc.date_class FROM tb_schedule_class_day sc, tb_schedule_class s
WHERE sc.date_class between '$date_start' and '$date_end'
and sc.schedule_id = s.schedule_id and s.branch_code = '$branchcode'
group by sc.date_class
order by sc.date_class";

$queryG      = DbQuery($sqlG,null);
$jsonG       = json_decode($queryG, true);
$rowG        = $jsonG['data'];
$dataCountG  = $jsonG['dataCount'];


for($i=0;$i<$dataCountG; $i++)
{
  $date    = $rowG[$i]['date_class'];
  $dateStr = date('D d/m/Y',strtotime($date));
  $row[$i]['dayOfClass'] = $dateStr;

  $sql = "SELECT scd.*,c.name_class,e.EMP_NICKNAME
  FROM tb_schedule_class_day scd, t_classes c, data_mas_employee e, tb_schedule_class s
  WHERE scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE  and scd.schedule_id = s.schedule_id
  and s.branch_code = '$branchcode'
  and scd.date_class = '$date' order by scd.date_class, scd.day, scd.row ";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];

  for($j=0;$j<$dataCountG; $j++)
  {
    $subClass[$j]['timeOfClass'] = $row[$j]['time_start'];
    $subClass[$j]['className'] = $row[$j]['name_class'];
    $subClass[$j]['trainerName'] = $row[$j]['EMP_NICKNAME'];
    $subClass[$j]['personJoin'] = $row[$j]['person_join'];
    $subClass[$j]['personTotal'] = $row[$j]['unit'];
  }
  $row[$i]['subClass'] = $subClass;
}


header('Content-Type: application/json');
exit(json_encode($row));


?>
