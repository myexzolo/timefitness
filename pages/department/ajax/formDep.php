<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = isset($_POST['id'])?$_POST['id']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';
$arr_page_list = array();

$department_code  = "";
$department_name  = "";
$is_active        = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql   = "SELECT * FROM t_department WHERE department_id = '$id' ORDER BY department_id DESC";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];

  $department_code  = $row[0]['department_code'];
  $department_name  = $row[0]['department_name'];
  $is_active        = $row[0]['is_active'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="department_id" value="<?=$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Department Code</label>
        <input value="<?=$department_code?>" name="department_code" type="text" maxlength="8" class="form-control text-uppercase" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Department Name</label>
        <input value="<?=$department_name?>" name="department_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=$is_active=='Y'?"selected":""?>>ACTIVE</option>
          <option value="N" <?=$is_active=='N'?"selected":""?>>NO ACTIVE</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
