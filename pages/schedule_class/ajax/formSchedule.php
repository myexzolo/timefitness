<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$date = date('01/m/Y')." - ".date('t/m/Y');
$action = $_POST['action'];
$id     = $_POST['id'];

$schedule_name = "";

if($id != "")
{
  $sql   = "SELECT * FROM tb_schedule_class where schedule_id = '$id' ";
  //echo $sql;
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $rows       = $json['data'][0];

  $schedule_name = $rows['schedule_name'];
  $date          = date("d/m/Y", strtotime($rows['date_start']))." - ".date("d/m/Y", strtotime($rows['date_end']));


  $sql   = "SELECT sc.*,c.name_class,e.EMP_NICKNAME
            FROM tb_schedule_class_det sc, t_classes c, data_mas_employee e
            where schedule_id = '$id' and sc.id_class = c.id_class and sc.EMP_CODE = e.EMP_CODE";
  //echo $sql;
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  for($i =0; $i < $dataCount; $i++ )
  {
    $id_class   = $rows[$i]['id_class'];
    $EMP_CODE   = $rows[$i]['EMP_CODE'];
    $day        = $rows[$i]['day'];
    $row        = $rows[$i]['row'];
    $unit       = $rows[$i]['unit'];
    $name_class = $rows[$i]['name_class'];
    $time_start   = $rows[$i]['time_start'];
    $time_end     = $rows[$i]['time_end'];
    $EMP_NICKNAME = $rows[$i]['EMP_NICKNAME'];
    $EMP_WAGE     = $rows[$i]['EMP_WAGE'];

    $classArr[$day][$row]['id']           = $day."_".$row;
    $classArr[$day][$row]['id_class']     = $id_class;
    $classArr[$day][$row]['EMP_CODE']     = $EMP_CODE;
    $classArr[$day][$row]['unit']         = $unit;
    $classArr[$day][$row]['time_start']   = $time_start;
    $classArr[$day][$row]['time_end']     = $time_end;
    $classArr[$day][$row]['name_class']   = $name_class;
    $classArr[$day][$row]['empNickName']  = $EMP_NICKNAME;
    $classArr[$day][$row]['EMP_WAGE']     = $EMP_WAGE;
  }
  //print_r($classArr);
}
?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 0px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 50px; /* Set a small width */
}
.boxClass {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 0px;  /* Rounded border */
  width: 100%; /* Set a small width */
}
.plus{
  font-size:50px;
  color:#ccc;
}
.plus i{
  margin-top:15px;
}

.plus:hover{
  color:#bbb;
  cursor: pointer;
}

th {
  text-align: center;
}
</style>
<div class="autoOverflow" align="center">
<form id="formAddModule" class="form-horizontal" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
<!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
  <input type="hidden" value="<?= $action ?>" name="action">
  <input type="hidden" value="<?= $id ?>" name="schedule_id">
  <div class="row">
    <div class="col-md-5">
      <div class="form-group">
        <label for="schedule_name" class="col-sm-4 control-label">Schedule Name</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" value="<?= $schedule_name; ?>" id="schedule_name" name="schedule_name" placeholder="Name"  required>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label for="schedule_name" class="col-sm-4 control-label">วันที่เริ่ม - สิ้นสุด</label>
        <div class="input-group col-sm-8">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="date_start" name="date_start" value="<?= $date; ?>" required>
        </div>
      </div>
    </div>
  </div>
<table class="table table-bordered table-striped" id="tableDisplay" style="width:98%">
  <thead>
    <tr class="text-center">
      <th style="width:14%">Monday</th>
      <th style="width:14%">Tuesday</th>
      <th style="width:14%">Wednesday</th>
      <th style="width:14%">Thursday</th>
      <th style="width:14%">Friday</td>
      <th style="width:14%">Saturday</th>
      <th style="width:14%">Sunday</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=1 ; $i<7; $i++)
      {
        // <div class="info-box boxClass">
        //   <div class="pull-right" onclick="clearSc('')">
        //     <button type="button" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
        //   </div>
        //   <div class="box-body">
        //     <div class="info-box-text cut-text">YOGA</div>
        //     <span class="info-box-number">10:30</span>
        //     <div class="pull-left cut-text">70% Increaseccccccccc</div>
        //     <div class="pull-right users-list-date" style="margin-top:2px;">
        //       <i class="fa fa-group"></i> 10
        //     </div>
        //   </div>
        // </div>
        ?>
    <tr>
      <td>
        <input type="hidden" value="<?= @$classArr[1][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[1][$i]['name_class']; ?>" id="name_class_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['empNickName']; ?>" id="empNickName_<?= "1_".$i ?>">
        <input type="hidden" value="1" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[1][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['unit']; ?>" name="unit[]" id="unit_<?= "1_".$i ?>">
        <input type="hidden" value="<?= @$classArr[1][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "1_".$i ?>">
        <div id="cl_<?= "1_".$i ?>" onclick="showForm('<?= "1_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
      </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[2][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[2][$i]['name_class']; ?>" id="name_class_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['empNickName']; ?>" id="empNickName_<?= "2_".$i ?>">
        <input type="hidden" value="2" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[2][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['unit']; ?>" name="unit[]" id="unit_<?= "2_".$i ?>">
        <input type="hidden" value="<?= @$classArr[2][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "2_".$i ?>">
        <div id="cl_<?= "2_".$i ?>" onclick="showForm('<?= "2_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[3][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[3][$i]['name_class']; ?>" id="name_class_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['empNickName']; ?>" id="empNickName_<?= "3_".$i ?>">
        <input type="hidden" value="3" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[3][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['unit']; ?>" name="unit[]" id="unit_<?= "3_".$i ?>">
        <input type="hidden" value="<?= @$classArr[3][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "3_".$i ?>">
        <div id="cl_<?= "3_".$i ?>" onclick="showForm('<?= "3_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[4][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[4][$i]['name_class']; ?>" id="name_class_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['empNickName']; ?>" id="empNickName_<?= "4_".$i ?>">
        <input type="hidden" value="4" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[4][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['unit']; ?>" name="unit[]" id="unit_<?= "4_".$i ?>">
        <input type="hidden" value="<?= @$classArr[4][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "4_".$i ?>">
        <div id="cl_<?= "4_".$i ?>" onclick="showForm('<?= "4_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[5][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[5][$i]['name_class']; ?>" id="name_class_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['empNickName']; ?>" id="empNickName_<?= "5_".$i ?>">
        <input type="hidden" value="5" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[5][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['unit']; ?>" name="unit[]" id="unit_<?= "5_".$i ?>">
        <input type="hidden" value="<?= @$classArr[5][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "5_".$i ?>">
        <div id="cl_<?= "5_".$i ?>" onclick="showForm('<?= "5_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[6][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[6][$i]['name_class']; ?>" id="name_class_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['empNickName']; ?>" id="empNickName_<?= "6_".$i ?>">
        <input type="hidden" value="6" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[6][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['unit']; ?>" name="unit[]" id="unit_<?= "6_".$i ?>">
        <input type="hidden" value="<?= @$classArr[6][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "6_".$i ?>">
        <div id="cl_<?= "6_".$i ?>" onclick="showForm('<?= "6_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
      <td>
        <input type="hidden" value="<?= @$classArr[7][$i]['id']; ?>" class="classes">
        <input type="hidden" value="<?= @$classArr[7][$i]['name_class']; ?>" id="name_class_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['empNickName']; ?>" id="empNickName_<?= "7_".$i ?>">
        <input type="hidden" value="7" name="day[]">
        <input type="hidden" value="<?= $i?>" name="row[]">
        <input type="hidden" value="<?= @$classArr[7][$i]['id_class']; ?>" name="id_class[]" id="id_class_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['EMP_CODE']; ?>" name="EMP_CODE[]" id="empCode_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['time_start']; ?>" name="time_start[]" id="time_start_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['time_end']; ?>" name="time_end[]" id="time_end_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['unit']; ?>" name="unit[]" id="unit_<?= "7_".$i ?>">
        <input type="hidden" value="<?= @$classArr[7][$i]['EMP_WAGE']; ?>" name="EMP_WAGE[]" id="EMP_WAGE_<?= "7_".$i ?>">
        <div id="cl_<?= "7_".$i ?>" onclick="showForm('<?= "7_".$i ?>')">
          <div class="info-box boxClass text-center plus">
              <i class="fa fa-plus "></i>
          </div>
        </div>
      </td>
    </tr>
  <?php } ?>
  </tbody>
</table>
<div class="box-footer">
  <button type="submit" class="btn btn-info pull-right" style="width:100px;">Save</button>
</div>
</form>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   });


   $('#date_start').daterangepicker(
     {
       locale: {
         format: 'DD/MM/YYYY',
         daysOfWeek: [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        monthNames: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
       }
     }
   );

   $( ".classes" ).each(function() {
     var id = $(this).val();
     if(id != "")
     {
        addClass(id);
     }
   });
  })

  $('#formAddModule').on('submit', function(event) {
    event.preventDefault();
    if ($('#formAddModule').smkValidate()) {
      $.ajax({
          url: 'ajax/AED.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar({
          element:'body',
          status:'start',
          bgColor: '#000',
          barColor: '#fff',
          content: 'Loading...'
        });
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formAddModule').smkClear();
          showTable();
          showSlidebar();
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
      });
    }
  });
</script>
