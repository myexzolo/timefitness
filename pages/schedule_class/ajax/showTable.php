<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

if($_SESSION['branchCode'] != ""){
  $con = " and sc.branch_code = '".$_SESSION['branchCode']."' ";
}
?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}
</style>
<div class="autoOverflow">
<table class="table table-bordered table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th>รายการ</th>
      <th>วันที่เริ่มใช้</th>
      <th>วันที่สิ้นสุด</th>
      <th>สาขา</td>
      <th style="width:80px">สถานะ</th>
      <th style="width:40px">Edit</th>
      <th style="width:40px">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT sc.*,b.cname FROM tb_schedule_class sc, t_branch b
                 where sc.branch_code = b.branch_code and sc.is_active not in ('D') $con
                 ORDER BY sc.date_start DESC";
      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {

        // id_class
        // name_class
        // detail_class
        // image_class
        // is_active
        // seq
    ?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td align="left"><?=$rows[$i]['schedule_name'];?></td>
      <td align="center"><?=DateThai($rows[$i]['date_start']);?></td>
      <td align="center"><?=DateThai($rows[$i]['date_end']);?></td>
      <td align="left"><?= $rows[$i]['cname'];?></td>
      <td align="center"><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"NO ACTIVE";?></td>
      <td align="center">
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showFormSchedule('EDIT','<?=$rows[$i]['schedule_id']?>')">Edit</button>
        </td>
      <td align="center">
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="delSchedule('<?=$rows[$i]['schedule_id']?>','<?=$rows[$i]['schedule_name']?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   })
  })
</script>
