<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');



$sql = "";
$action                   = $_POST['action'];
$id                       = isset($_POST['schedule_id'])?$_POST['schedule_id']:"";
$arr['schedule_name']     = @$_POST['schedule_name'];
$dateS                    = @explode("-", $_POST['date_start']);
$arr['date_start']        = @dateThToEn(trim($dateS[0]),"dd/mm/yyyy","/");
$arr['date_end']          = @dateThToEn(trim($dateS[1]),"dd/mm/yyyy","/");
$arr['branch_code']       = $_SESSION['branchCode'];
$arr['create_by']         = $_SESSION['member'][0]['user_id'];
$arr['create_date']       = date("Y-m-d h:i:s");
$arr['update_date']       = date("Y-m-d h:i:s");

$arr['is_active']         = 'Y';

if($action == 'ADD'){
   $json   = json_decode(DBInsertPOST($arr,'tb_schedule_class'), true);
   if($json['message'] == "Success"){
     $sql .= $json['data'];
   }

}else if($action == 'EDIT'){
  $arr['schedule_id']  = $id;
  $json   = json_decode(DBUpdatePOST($arr,'tb_schedule_class','schedule_id'), true);
  if($json['message'] == "Success"){
    $sql = $json['data'];
  }
}
else if($action == 'DEL')
{
    $sql  = "UPDATE tb_schedule_class SET is_active = 'D' WHERE schedule_id = '$id';";
}
//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if($action == 'ADD'){
    $id  = $row['id'];
}

if(intval($errorInfo[0]) == 0){
    $sqlD    = "delete FROM tb_schedule_class_det WHERE schedule_id = '$id';";
    $sqlD   .= "delete FROM tb_schedule_class_day WHERE schedule_id = '$id' and person_join = '0' and sign_emp = 'N';";

    $query      = DbQuery($sqlD,null);
    if($action != 'DEL'){
      $sqlCl  = "";
      $sqlCld = "";
      $a = 0;
      $b = 0;
      $c = 0;
      $d = 0;
      $e = 0;
      $f = 0;
      $g = 0;
      $dateDiff = DateDiff($arr['date_start'],$arr['date_end']);
      for($x=0; $x <= $dateDiff ; $x++)
      {
        $dateSc  = date('Y-m-d',strtotime($arr['date_start'] . "+$x days"));
        $daySc   = date('N',strtotime($dateSc));
        if($daySc == '1'){
          $arrCd[$daySc][$a] = $dateSc;
          $a++;
        }else if($daySc == '2'){
          $arrCd[$daySc][$b] = $dateSc;
          $b++;
        }else if($daySc == '3'){
          $arrCd[$daySc][$c] = $dateSc;
          $c++;
        }else if($daySc == '4'){
          $arrCd[$daySc][$d] = $dateSc;
          $d++;
        }else if($daySc == '5'){
          $arrCd[$daySc][$e] = $dateSc;
          $e++;
        }else if($daySc == '6'){
          $arrCd[$daySc][$f] = $dateSc;
          $f++;
        }else if($daySc == '7'){
          $arrCd[$daySc][$g] = $dateSc;
          $g++;
        }
      }
      for($i=0; $i < count($_POST['day']) ; $i++)
      {
        $arrCl['schedule_id'] = $id;
        $arrCl['id_class']    = $_POST['id_class'][$i];
        $arrCl['EMP_CODE']    = $_POST['EMP_CODE'][$i];
        $arrCl['day']         = $_POST['day'][$i];
        $arrCl['row']         = $_POST['row'][$i];
        $arrCl['unit']        = $_POST['unit'][$i];
        $arrCl['time_start']  = $_POST['time_start'][$i];
        $arrCl['time_end']    = $_POST['time_end'][$i];
        $arrCl['EMP_WAGE']    = $_POST['EMP_WAGE'][$i];


        if($arrCl['id_class'] != "")
        {
          $json   = json_decode(DBInsertPOST($arrCl,'tb_schedule_class_det'), true);
          if($json['message'] == "Success"){
            $sqlCl  = $json['data'];
            $query  = DbQuery($sqlCl,null);
            $dayCl  = $arrCl['day'];

            if(isset($arrCd[$dayCl]))
            {
              //echo "1>>".$sqlCl."<br>";
              for($j=0; $j < count($arrCd[$dayCl]) ; $j++)
              {
                $arrCld['id']          = $arrCl['schedule_id'].str_replace("-","",$arrCd[$dayCl][$j]).$arrCl['day'].$arrCl['row'];
                $arrCld['schedule_id'] = $arrCl['schedule_id'];
                $arrCld['id_class']    = $arrCl['id_class'];
                $arrCld['EMP_CODE']    = $arrCl['EMP_CODE'];
                $arrCld['day']         = $arrCl['day'];
                $arrCld['row']         = $arrCl['row'];
                $arrCld['unit']        = $arrCl['unit'];
                $arrCld['time_start']  = $arrCl['time_start'];
                $arrCld['time_end']    = $arrCl['time_end'];
                $arrCld['EMP_WAGE']    = $arrCl['EMP_WAGE'];
                $arrCld['date_class']  = $arrCd[$dayCl][$j];
                $arrCld['person_join'] = '0';

                $idsd = $arrCld['id'];

                $sqlsc   = "SELECT id FROM tb_schedule_class_day where id = '$idsd' ";
                //echo $sqls;
                $querysc    = DbQuery($sqlsc,null);
                $jsonsc     = json_decode($querysc, true);
                $dataCount  = $jsonsc['dataCount'];

                if($dataCount == 0)
                {
                  $json2   = json_decode(DBInsertPOST($arrCld,'tb_schedule_class_day'), true);
                  if($json2['message'] == "Success"){
                    $sqlCld = $json2['data'];
                    //echo "2>>".$sqlCld."<br>";
                    DbQuery($sqlCld,null);
                  }
                }

              }
            }
          }
        }
      }
    }
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail2')));
}

?>
