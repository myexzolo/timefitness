
showTable();

function showTable(){
  $('#btnBack').hide();
  $('#btnAction').show();
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showFormSchedule(action,id){
  $('#btnAction').hide();
  $('#btnBack').show();
  $.post("ajax/formSchedule.php",{action:action,id:id})
    .done(function( data ) {
      $("#showTable").html( data );
  });
}

function delSchedule(id,name){
  $.smkConfirm({
    text:"Are You Sure Delete Schedule :" + name +"?",
    accept:"Yes",
    cancel:"No"
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:"DEL",schedule_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:"body",
            status:"start",
            bgColor: "#000",
            barColor: "#fff",
            content: "Loading..."
          });
          setTimeout(function(){
            $.smkProgressBar({status:"end"});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function showForm(id){
  //var cl          = $('#cl_'+id ).val();
  var id_class    = $('#id_class_'+id ).val();
  var empCode     = $('#empCode_'+id ).val();
  var unit        = $('#unit_'+id ).val();
  var time_start  = $('#time_start_'+id ).val();
  var time_end    = $('#time_end_'+id ).val();
  var EMP_WAGE    = $('#EMP_WAGE_'+id ).val();
  //alert(id);
  $.post("ajax/formModule.php",{id:id,id_class:id_class,empCode:empCode,unit:unit,time_start:time_start,time_end:time_end,EMP_WAGE:EMP_WAGE})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function clearSc(id)
{
  var val = "";
  val +="<div class=\"info-box boxClass text-center plus\">";
  val +="    <i class=\"fa fa-plus \"></i>";
  val +="</div>";

  $('#cl_'+id ).html(val);
  $('#id_class_'+id ).val("");
  $('#empCode_'+id ).val("");
  $('#unit_'+id ).val("");
  $('#time_start_'+id ).val("");
  $('#time_end_'+id ).val("");
  $('#EMP_WAGE_'+id ).val("");
  event.stopPropagation();
}


function addClass(id)
{
  var classText   = $('#name_class_'+id ).val();
  var nickname    = $('#empNickName_'+id ).val();
  var unit        = $('#unit_'+id ).val();
  var timeStart   = $('#time_start_'+id ).val();
  var timeEnd     = $('#time_end_'+id ).val();
  var empWage     = $('#EMP_WAGE_'+id ).val();
  var val = "";

  val +="<div class=\"info-box boxClass\">";
  val +="  <div class=\"pull-right \" style=\"z-index: 100;\" onclick=\"clearSc('"+id+"')\">";
  val +="    <button type=\"button\" class=\"btn btn-box-tool\"><i class=\"fa fa-times\"></i></button>";
  val +="  </div>";
  val +="  <div class=\"box-body\">";
  val +="    <div class=\"info-box-text cut-text\" ><b>"+classText+"</b></div>";
  val +="    <div style=\"border: 1px solid #ccc;height:1px;\"></div>";
  val +="    <div style=\"font-size:14px;margin-bottom:5px;margin-top:2px;width:75%;\"class=\"pull-left cut-text\"><b>"+ timeStart +" - "+ timeEnd +"</b></div>";
  val +="    <div class=\"pull-right users-list-date\" align=\"right\" style=\"margin-top:2px;width:25%;\">";
  val +="      <i class=\"fa fa-group\"></i> "+unit;
  val +="    </div>";
  val +="    <div class=\"pull-left cut-text\" style=\"width:70%\">"+nickname+"</div>";
  val +="    <div class=\"pull-right users-list-date\" align=\"right\" style=\"margin-top:2px;width:30%\">";
  val +="      <i class=\"fa fa-btc\"></i> "+empWage;
  val +="    </div>";
  val +="  </div>";
  val +="</div>";

  $('#cl_'+id ).html(val);
}

function createClass(id)
{
  event.preventDefault();
  if ($('#formSc').smkValidate())
  {
      var id_class    = $('#id_class option:selected').val();
      var classText   = $('#id_class option:selected').text();
      var empCodeVal  = $('#EMP_CODE option:selected').val().split(":");
      var empCodeText = $('#EMP_CODE option:selected').text().split(":");
      var unit        = $('#unit').val();
      var timeStart   = $('#timeStart').val();
      var timeEnd     = $('#timeEnd').val();
      var empWage     = $('#EMP_WAGE').val();

      var nickname    = empCodeText[0];
      var empCode     = empCodeVal[0];
      var val = "";

      val +="<div class=\"info-box boxClass\">";
      val +="  <div class=\"pull-right \" style=\"z-index: 100;\" onclick=\"clearSc('"+id+"')\">";
      val +="    <button type=\"button\" class=\"btn btn-box-tool\"><i class=\"fa fa-times\"></i></button>";
      val +="  </div>";
      val +="  <div class=\"box-body\">";
      val +="    <div class=\"info-box-text cut-text\" ><b>"+classText+"</b></div>";
      val +="    <div style=\"border: 1px solid #ccc;height:1px;\"></div>";
      val +="    <div style=\"font-size:14px;margin-bottom:5px;margin-top:2px;width:75%;\"class=\"pull-left cut-text\"><b>"+ timeStart +" - "+ timeEnd +"</b></div>";
      val +="    <div class=\"pull-right users-list-date\" align=\"right\" style=\"margin-top:2px;width:25%;\">";
      val +="      <i class=\"fa fa-group\"></i> "+unit;
      val +="    </div>";
      val +="    <div class=\"pull-left cut-text\" style=\"width:70%\">"+nickname+"</div>";
      val +="    <div class=\"pull-right users-list-date\" align=\"right\" style=\"margin-top:2px;width:30%\">";
      val +="      <i class=\"fa fa-btc\"></i> "+empWage;
      val +="    </div>";
      val +="  </div>";
      val +="</div>";


      $('#cl_'+id ).html(val);
      $('#id_class_'+id ).val(id_class);
      $('#empCode_'+id ).val(empCode);
      $('#unit_'+id ).val(unit);
      $('#time_start_'+id ).val(timeStart);
      $('#time_end_'+id ).val(timeEnd);
      $('#EMP_WAGE_'+id ).val(empWage);

      //console.log($('#cl_'+id ).html());

      $('#myModal').modal('toggle');
  }
}
