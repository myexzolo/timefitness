<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Time Fitness | GROUP SCHEDULE CLESSES</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/scheduleClass.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              GROUP CLASS SCHEDULE
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Schedule Classes</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //nclude("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right" id="btnAction">
                      <button onclick="showFormSchedule('ADD','')" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus"></i> เพิ่ม Schedule Classes</button>
                    </div>
                    <div class="box-tools pull-right" id="btnBack" style="display:none;">
                      <button onclick="showTable()" class="btn btn-success btn-flat pull-right" style="width:100px;"><i class="fa fa-chevron-left"></i> กลับ</button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Management Clesses</h4>
              </div>
                <div id="show-form"></div>
            </div>
          </div>
        </div>
        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/scheduleClass.js"></script>
    </body>
  </html>
