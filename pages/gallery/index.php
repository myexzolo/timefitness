<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Time Fitness | Gallery</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/gallery.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              อัมบั้มรูปภาพ
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Gallery</li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary" style="min-height: 58vh;">
                  <div class="box-header with-border">
                    <h3 class="box-title">Gallery Controller</h3>

                    <!-- <div class="box-tools pull-right">
                      <button type="button" onclick="showForm('ADD','')" class="btn btn-success btn-flat pull-right">Create Gallery</button>
                    </div> -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body" style="min-height:45vh;">

                    <div class="row" id="showTable"></div>

                  </div>
                </div>
                <!-- /.box -->
                <!-- <div style="margin-top:75px;padding:20px;">
                  <form action="ajax/AEDModule.php" class="dropzone" id="dropzoneFrom" enctype="multipart/form-data"></form>
                </div> -->

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Management Gallery</h4>
                      </div>
                      <!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data"> -->
                      <!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                        <div id="show-form"></div>
                      <!-- </form> -->
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/gallery.js"></script>
    </body>
  </html>
