<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = @$_POST['value'];
$id = @$_POST['id'];

$gallery_name = "";
$is_active    = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_gallery WHERE gallery_id = '$id'";
  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];

  $gallery_id   = $rows[0]['gallery_id'];
  $gallery_name = $rows[0]['gallery_name'];
  $branch_code  = $rows[0]['branch_code'];
  $is_active    = $rows[0]['is_active'];


}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" id="gallery_id" name="gallery_id" value="<?= $gallery_id ?>">
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-5">
      <div class="form-group">
        <label>Gallery Name</label>
        <input value="<?=$gallery_name?>" id="gallery_name" name="gallery_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>สาขา</label>
        <select name="branch_code" id="branch_code" class="form-control" style="width: 100%;" required>
          <option value="ALL">ทุกสาขา</option>
          <?php
            $sqls   = "SELECT * FROM t_branch where is_active = 'Y' ORDER BY branch_id DESC";
            $querys     = DbQuery($sqls,null);
            $json       = json_decode($querys, true);
            $dataCount  = $json['dataCount'];
            $rows       = $json['data'];
            if($dataCount > 0){
              foreach ($rows as $value) {
          ?>
          <option value="<?=$value['branch_code']?>" <?=$value['branch_code'] ==@$branch_code?"selected":""; ?>><?=$value['branch_name']?></option>
          <?php } } ?>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" id="is_active" class="form-control" style="width: 100%;" required>
          <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>

    <div style="margin-top:75px;padding:20px;">
      <!-- <div id="myDropzone" class="dropzone"></div> -->
      <form action="ajax/AEDModule.php" class="dropzone" id="myDropzone">
        <input type="hidden" id="id" name="id" value="">
      </form>
    </div>

    <?php
      if($action == 'EDIT'){
      $sqls   = "SELECT * FROM t_gallert_image WHERE gallery_id = '$id' ORDER BY gallery_id DESC";
      $query      = DbQuery($sqls,null);
      $row        = json_decode($query, true);
      $rows       = $row['data'];

      if($dataCount > 0){
        foreach ($rows as $value) {

    ?>
    <div class="col-md-2">
      <a href="upload/<?=$value['tg_image']?>" data-lightbox="roadtrip">
        <div class="img-list" style="background-image: url(upload/<?=$value['tg_image']?>);"></div>
      </a>
      <label class="toggle" style="position: absolute;top: 0px;left: 15px;">
        <input class="toggle__input" name="remove[]" value="<?=$value['tg_id']?>" type="checkbox">
        <span class="toggle__label">
          <span class="toggle__text"></span>
        </span>
      </label>
    </div>
    <?php } } } ?>

  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
<?php if($action != "SHOW"){ ?>
<button type="button" id="submit-all" class="btn btn-primary btn-flat"><?=$btn?></button>
<?php } ?>
</div>


<script type="text/javascript">

// Dropzone.autoDiscover = false;
var id = $('#id').val();
var myDropzone = new Dropzone("#myDropzone",
  {
    autoProcessQueue: false,
    addRemoveLinks:true,
    thumbnailWidth: 100,
    thumbnailHeight: 100,
    parallelUploads: 100,
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    dictDefaultMessage: "ลากและวางไฟล์รูปภาพ",
    init: function(){
       var submitButton = document.querySelector('#submit-all');
       var myDropzone = this;

       submitButton.addEventListener("click", function(){
          var arr = [];
          $('input[name="remove[]"]:checkbox:checked').each(function () {
            arr.push(this.value);
          });
          if(arr == ''){
            arr = '';
          }
         if ($('#gallery_name').smkValidate()) {
            var gallery_name = $('#gallery_name').val();
            var branch_code  = $('#branch_code').val();
            var is_active    = $('#is_active').val();
            var action       = $('#action').val();
            var gallery_id   = $('#gallery_id').val();
            $.post("ajax/AEDModuleGallery.php",{
              remove:arr,
              gallery_id:gallery_id,
              gallery_name:gallery_name,
              branch_code:branch_code,
              is_active:is_active,
              action:action
            })
              .done(function( data ) {
                $('#id').val(data.id);

                if(myDropzone.getQueuedFiles().length == 0){
                  $.smkProgressBar({
                    element:'body',
                    status:'start',
                    bgColor: '#000',
                    barColor: '#fff',
                    content: 'Loading...'
                  });
                  setTimeout(function(){
                    $.smkProgressBar({status:'end'});
                    showTable();
                    showSlidebar();
                    $.smkAlert({text: 'success',type: 'success'});
                    $('#myModal').modal('toggle');
                  }, 1000);
                }
                myDropzone.processQueue();
            });
          }
       });
       this.on("complete", function(data){
         if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
          {
           var _this = this;
           _this.removeAllFiles();
           $.smkProgressBar({
             element:'body',
             status:'start',
             bgColor: '#000',
             barColor: '#fff',
             content: 'Loading...'
           });
           setTimeout(function(){
             $.smkProgressBar({status:'end'});
             showTable();
             showSlidebar();
             $.smkAlert({text: 'success',type: 'success'});
             $('#myModal').modal('toggle');
           }, 1000);
          }


       });
      },
  });

</script>
