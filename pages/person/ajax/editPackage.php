<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$id           = isset($_POST['id'])?$_POST['id']:"";
$person_code  = isset($_POST['person_code'])?$_POST['person_code']:"";
$note         = isset($_POST['note'])?$_POST['note']:"";
$type_package = isset($_POST['type_package'])?$_POST['type_package']:"";
$transfer     = isset($_POST['transfer'])?$_POST['transfer']:"";
$person_code_trans = isset($_POST['person_code_trans'])?$_POST['person_code_trans']:"";

$dateNow = date("Y-m-d");

$date_start   = isset($_POST['date_start'])?dateThToEn($_POST['date_start'],"dd/mm/yyyy","/"):"";
$date_expire  = isset($_POST['date_expire'])?dateThToEn($_POST['date_expire'],"dd/mm/yyyy","/"):"";

$diif = DateDiff($dateNow,$date_expire);
$con  = "";
//echo $diif." ,".$dateNow." ,".$date_expire;
if($diif > 0){
  $con = "status = 'A',";
}

if($transfer == "Y"){
  $sql = "UPDATE trans_package_person SET
          date_start    = '$date_start',
          date_expire    = '$date_expire',
          person_code    = '$person_code_trans',
          $con
          note = '$note'
          WHERE id = '$id'";
}else{
  $sql = "UPDATE trans_package_person SET
          date_start     = '$date_start',
          date_expire    = '$date_expire',
          $con
          note = '$note'
          WHERE id = '$id'";
}


$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];


if($type_package == "MB" &&  $diif > 0)
{
  $sqlp = "SELECT person_expire_date from person WHERE PERSON_CODE = '$person_code'";
  //echo $sqlp;
  $queryp      = DbQuery($sqlp,null);
  $json       = json_decode($queryp, true);
  $rows       = $json['data'];

  $person_expire_date = $rows[0]['person_expire_date'];



  $diff = DateDiff($person_expire_date,$date_expire);
  //echo $diif." ,".$person_expire_date." ,".$date_expire;
  if($diff > 0){
    $sqlu = "UPDATE person SET person_status = 'A' , person_expire_date = '$date_expire'
    WHERE PERSON_CODE = '$person_code'";
    //echo $sqlu;
    DbQuery($sqlu,null);
  }
}
//echo intval($errorInfo[0]);

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success', 'code' => $person_code)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail'.$sql, 'code' => $person_code)));
}

?>
