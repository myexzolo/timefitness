<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = $_POST['value'];
$code         = isset($_POST['code'])?$_POST['code']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';

$COMPANY_CODE     = $_SESSION['branchCode'];
$PERSON_CODE      = "";
$PERSON_CODE_INT  = "";
$PERSON_TITLE     = "";
$PERSON_NAME      = "";
$PERSON_LASTNAME  = "";
$PERSON_SEX       = "";
$PERSON_ER_TEL    = "";
$PERSON_BIRTH_DATE = "";
$PERSON_NOTE = "";
$PERSON_GROUP = "";
$PERSON_GROUP_AGE = "";
$PERSON_NICKNAME  = "";
$PERSON_CARD_ID   = "";
$PERSON_HOME1_ADDR1 = "";
$PERSON_HOME1_ADDR2 = "";
$PERSON_HOME1_SUBDISTRICT = "";
$PERSON_HOME1_DISTRICT    = "";
$PERSON_HOME1_PROVINCE    = "";
$EMP_CODE_SALE            = "";
$PERSON_HOME1_POSTAL      = "";
$PERSON_TEL_MOBILE        = "";
$PERSON_TEL_MOBILE2       = "";
$PERSON_EMAIL             = "";
$PERSON_BILL_NAME         = "";
$PERSON_BILL_TAXNO        = "";
$PERSON_BILL_ADDR1        = "";
$PERSON_BILL_SUBDISTRICT  = "";
$PERSON_BILL_DISTRICT     = "";
$PERSON_BILL_PROVINCE     = "";
$PERSON_BILL_POSTAL       = "";
$PERSON_FAX               = "";
$PERSON_TEL_OFFICE        = "";
$PERSON_IMAGE             = "";
$BAN_RESERVE              = "";



$branch_id = "";
$user_id   = "";

$disabled = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql = "SELECT * FROM  person
  where COMPANY_CODE ='$COMPANY_CODE' and PERSON_CODE = '$code' order by PERSON_CODE";

  //echo $sql;

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];


  $PERSON_CODE              = $row[0]['PERSON_CODE'];
  $PERSON_TITLE             = $row[0]['PERSON_TITLE'];
  $PERSON_NAME              = $row[0]['PERSON_NAME'];
  $PERSON_LASTNAME          = $row[0]['PERSON_LASTNAME'];
  $PERSON_SEX               = $row[0]['PERSON_SEX'];
  $PERSON_ER_TEL            = $row[0]['PERSON_ER_TEL'];
  $PERSON_BIRTH_DATE        = DateThai($row[0]['PERSON_BIRTH_DATE']);
  $PERSON_NOTE              = $row[0]['PERSON_NOTE'];
  $PERSON_GROUP             = $row[0]['PERSON_GROUP'];
  $PERSON_GROUP_AGE         = $row[0]['PERSON_GROUP_AGE'];
  $PERSON_NICKNAME          = $row[0]['PERSON_NICKNAME'];
  $PERSON_CARD_ID           = $row[0]['PERSON_CARD_ID'];
  $PERSON_HOME1_ADDR1       = $row[0]['PERSON_HOME1_ADDR1'];
  $PERSON_HOME1_ADDR2       = $row[0]['PERSON_HOME1_ADDR2'];
  $PERSON_HOME1_SUBDISTRICT = $row[0]['PERSON_HOME1_SUBDISTRICT'];
  $PERSON_HOME1_DISTRICT    = $row[0]['PERSON_HOME1_DISTRICT'];
  $PERSON_HOME1_PROVINCE    = $row[0]['PERSON_HOME1_PROVINCE'];
  $EMP_CODE_SALE            = $row[0]['EMP_CODE_SALE'];
  $PERSON_HOME1_POSTAL      = $row[0]['PERSON_HOME1_POSTAL'];
  $PERSON_TEL_MOBILE        = $row[0]['PERSON_TEL_MOBILE'];
  $PERSON_TEL_MOBILE2       = $row[0]['PERSON_TEL_MOBILE2'];
  $PERSON_EMAIL             = $row[0]['PERSON_EMAIL'];
  $PERSON_BILL_NAME         = $row[0]['PERSON_BILL_NAME'];
  $PERSON_BILL_TAXNO        = $row[0]['PERSON_BILL_TAXNO'];
  $PERSON_BILL_ADDR1        = $row[0]['PERSON_BILL_ADDR1'];
  $PERSON_BILL_SUBDISTRICT  = $row[0]['PERSON_BILL_SUBDISTRICT'];
  $PERSON_BILL_DISTRICT     = $row[0]['PERSON_BILL_DISTRICT'];
  $PERSON_BILL_PROVINCE     = $row[0]['PERSON_BILL_PROVINCE'];
  $PERSON_BILL_POSTAL       = $row[0]['PERSON_BILL_POSTAL'];
  $PERSON_FAX               = $row[0]['PERSON_FAX'];
  $PERSON_TEL_OFFICE        = $row[0]['PERSON_TEL_OFFICE'];
  $PERSON_IMAGE             = $row[0]['PERSON_IMAGE'];
  $BAN_RESERVE              = $row[0]['BAN_RESERVE'];

  if($PERSON_IMAGE != ""){
    if(strpos($PERSON_IMAGE,"data:image") === false)
    {
      $PERSON_IMAGE = "data:image/png;base64,".$PERSON_IMAGE;
    }

  }

  if(!empty($BAN_RESERVE)){
    $banReserve   = "Y";
    $banResOpt    = '<option value="C">ยกเลิกโทษแบน</option>';
    $BAN_RESERVE  = DateThai($BAN_RESERVE);
  }else{
    $banReserve = "";
    $banResOpt  = "";
    $BAN_RESERVE = "";
  }
}

$optionDistrict = "";

$optionTitle          = getoptionDataMaster('PRENAME',$PERSON_TITLE);
$optionProvince       = getoptionProvince($PERSON_HOME1_PROVINCE);
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<input type="hidden" name="COMPANY_CODE" value="<?=$COMPANY_CODE?>">
<input type="hidden" id="EMP_ADDR_DISTRICT_NAME" value="<?=$EMP_ADDR_DISTRICT?>">
<input type="hidden" id="EMP_ADDR_SUBDISTRICT_NAME" value="<?=$EMP_ADDR_SUBDISTRICT?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสสมาชิก</label>
        <input value="<?= $PERSON_CODE ?>" name="PERSON_CODE" type="text" maxlength="15" class="form-control text-uppercase" placeholder="Code" required readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>คำนำหน้า</label>
        <select name="PERSON_TITLE" data-smk-msg="&nbsp;" id="PERSON_TITLE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="" selected></option>
          <?= $optionTitle; ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ</label>
          <input value="<?= $PERSON_NAME ?>" name="PERSON_NAME" type="text" class="form-control" placeholder="Name" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สกุล</label>
          <input value="<?= $PERSON_LASTNAME ?>" name="PERSON_LASTNAME" type="text" class="form-control" placeholder="LastName" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อเล่น</label>
          <input value="<?= $PERSON_NICKNAME ?>" name="PERSON_NICKNAME" type="text" class="form-control text-uppercase" placeholder="NickName" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เพศ</label>
        <div class="form-control" style="border-style: hidden;">
          <input type="radio" data-smk-msg="&nbsp;" name="PERSON_SEX" id="gender_m" class="minimal" value="ชาย" <?= $PERSON_SEX == 'ชาย'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
          <input type="radio" data-smk-msg="&nbsp;" name="PERSON_SEX" id="gender_f" class="minimal" value="หญิง" <?= $PERSON_SEX == 'หญิง'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่บัตรประชาชน</label>
        <input value="<?= $PERSON_CARD_ID ?>" name="PERSON_CARD_ID" type="text"  onkeyup="checknumber(this)" class="form-control" placeholder="ID Card" data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปีเกิด</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $PERSON_BIRTH_DATE ?>" required name="PERSON_BIRTH_DATE" type="text" data-provide="datepicker" data-date-language="th-th" readonly style="background-color:#fff;">
      </div>
  </div>
  <div class="col-md-7">
    <div class="form-group">
      <label>อีเมล์</label>
      <input value="<?=$PERSON_EMAIL?>" name="PERSON_EMAIL" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>เบอร์โทร</label>
      <input value="<?=$PERSON_TEL_MOBILE?>" name="PERSON_TEL_MOBILE" type="tel" class="form-control" placeholder="Tel" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Image</label>
      <input name="PERSON_IMAGE" type="file" class="form-control" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>
        <img src="<?= $PERSON_IMAGE ?>" onerror="this.onerror='';this.src='../../image/user.png'" style="height: 60px;">
      </label>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>โทษแบน</label>
      <select name="BAN_RESERVE" class="form-control select2" style="width: 100%;" required>
        <option value="" <?= ($banReserve == '' ? 'selected="selected"':'') ?> ></option>
        <option value="Y" <?= ($banReserve == 'Y' ? 'selected="selected"':'') ?> >โทษแบน (<?=$BAN_RESERVE?>)</option>
        <?= $banResOpt ?>
      </select>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
