<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id     = isset($_POST['id'])?$_POST['id']:"";

$sql = "SELECT * FROM trans_package_person where id = '$id'";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$person_code      = "";
$package_name     = "";
$use_package      = "";
$num_use          = "";
$date_start       = "";
$date_expire      = "";
$type_package      = "";

if($dataCount > 0)
{
  $person_code      = $row[0]['person_code'];
  $package_name     = $row[0]['package_name'];
  $use_package      = $row[0]['use_package'];
  $num_use          = $row[0]['num_use'];
  $package_unit     = $row[0]['package_unit'];
  $type_package     = $row[0]['type_package'];
  $date_start       = DateThai($row[0]['date_start']);
  $date_expire      = DateThai($row[0]['date_expire']);
}
?>
<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="person_code" value="<?=$person_code?>">
<input type="hidden" name="type_package" value="<?=$type_package?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-7">
      <div class="form-group">
        <label>Package Name</label>
        <input value="<?= $package_name ?>" type="text" class="form-control" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>คงเหลือ</label>
        <input value="<?= $use_package."/".$num_use ?>" type="text" class="form-control" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>หน่วย</label>
        <input value="<?= $package_unit ?>" type="text" class="form-control" placeholder="Code" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่เริ่ม</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $date_start ?>" required name="date_start" type="text" data-provide="datepicker" data-date-language="th-th" >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่สิ้นสุด</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $date_expire ?>" required name="date_expire" type="text" data-provide="datepicker" data-date-language="th-th" >
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>หมายเหตุ</label>
        <textarea class="form-control" name="note" rows="3" placeholder="Enter ..." required data-smk-msg="&nbsp;"></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-info btn-flat" onclick="showPackageMember('<?=$person_code ?>')">Cancel</button>
  <button type="submit" class="btn btn-primary btn-flat">Update changes</button>
</div>
