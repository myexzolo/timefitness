<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action             = $_POST['action'];
$id_class           = $_POST['id_class'];
$name_class         = $_POST['name_class'];
$detail_class       = $_POST['detail_class'];
$is_active          = $_POST['is_active'];
$seq                = $_POST['seq'];
$vid                = $_POST['vid'];
$user_id            = $_SESSION['member'][0]['user_id'];


$updateImg = "";
$image_class = "";

$path = "images/";
$image_class = resizeImageToBase64($_FILES["image_class"],'128','128','100',$user_id,$path);

$sql = "";
// --ADD EDIT DELETE Module-- //
if($action == 'ADD'){
  $sql = "INSERT INTO t_classes(
            name_class,
            detail_class,
            image_class,
            is_active,
            seq,
            vid,
            create_date,
            update_date,
            user_update
        )VALUES(
           '$name_class',
           '$detail_class',
           '$image_class',
           '$is_active',
           '$seq',
           '$vid',
           now(),
           now(),
           '$user_id'
         )";
}else if($action == 'EDIT'){

  if(!empty($image_class)){
      $updateImg    =  "image_class = '$image_class',";
  }

  $sql = "UPDATE t_classes SET
                name_class = '$name_class',
                detail_class = '$detail_class',
                ".$updateImg."
                is_active = '$is_active',
                seq = '$seq',
                vid = '$vid',
                update_date = now(),
                user_update = '$user_id'
            WHERE id_class = '$id_class'";
}
else if($action == 'DEL')
{
  $sql   = "UPDATE t_classes SET is_active = 'D' WHERE id_class = '$id_class'";
}

//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
//echo intval($errorInfo[0]);

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
