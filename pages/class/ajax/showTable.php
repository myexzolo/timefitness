<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}
</style>
<div class="autoOverflow">
<table class="table table-bordered table-hover table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th>Name</th>
      <th>รายละเอียด</th>
      <th>ลำดับการแสดง</th>
      <th>รูป</td>
      <th style="width:80px">สถานะ</th>
      <th style="width:40px">Edit</th>
      <th style="width:40px">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_classes where is_active not in ('D') ORDER BY seq DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {

        // id_class
        // name_class
        // detail_class
        // image_class
        // is_active
        // seq
    ?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td align="left"><?=$rows[$i]['name_class'];?></td>
      <td align="left"><?=$rows[$i]['detail_class'];?></td>
      <td align="right"><?=$rows[$i]['seq'];?></td>
      <td align="center"> <img src="<?= $rows[$i]['image_class'];?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></td>
      <td align="center"><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"NO ACTIVE";?></td>
      <td align="center">
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['id_class']?>')">Edit</button>
        </td>
      <td align="center">
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['id_class']?>','<?=$rows[$i]['name_class']?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   })
  })
</script>
