<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];

$id_class       = "";
$name_class     = "";
$detail_class   = "";
$image_class    = "";
$is_active      = "";
$seq            = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_classes WHERE  id_class = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];

  $id_class       = $rows[0]['id_class'];
  $name_class     = $rows[0]['name_class'];
  $detail_class   = $rows[0]['detail_class'];;
  $image_class    = $rows[0]['image_class'];
  $is_active      = $rows[0]['is_active'];
  $seq            = $rows[0]['seq'];
  $vid            = $rows[0]['vid'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="id_class" value="<?= $id_class ?>">
<input type="hidden" name="action" value="<?= $action ?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-5">
      <div class="form-group">
        <label>Class Name</label>
        <input value="<?= $name_class ?>" name="name_class" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?= $seq ?>" name="seq" type="number" class="form-control" placeholder="Seq" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Watch Id Youtube</label>
        <input value="<?= $vid ?>" name="vid" type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=<Id Youtube>" >
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Class Image</label>
        <input value="" name="image_class" type="file" class="form-control" placeholder="" >
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>
          <img src="<?= $image_class ?>" onerror="this.onerror='';this.src='../../image/picture.png'" style="height: 100px;">
        </label>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Class Description</label>
        <textarea class="form-control" rows="3" name="detail_class" placeholder="รายละเอียด ..."><?= $detail_class ?></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
<?php } ?>
</div>
