<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 250px; /* Set a small width */
}

.datepicker {
    padding: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 0px;
    direction: ltr;
}
th {
  text-align: center;
}
</style>
<div class="autoOverflow">
<table class="table table-bordered table-hover table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th>รูป</td>
      <th style="width:100px">ลำดับการแสดง</td>
      <th style="width:100px">สถานะ</th>
      <th style="width:40px">Edit</th>
      <th style="width:40px">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_slide where is_active != 'D' ORDER BY create_date DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
        $img = "../../image/slides/mobile/".$rows[$i]['img'];
    ?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td align="center"> <img src="<?= $img;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></td>
      <td align="center"><?=$rows[$i]['seq'];?></td>
      <td align="center"><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"INACTIVE";?></td>
      <td align="center">
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['id']?>')">Edit</button>
        </td>
      <td align="center">
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['id']?>','<?=$rows[$i]['img']?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   })
  })
</script>
