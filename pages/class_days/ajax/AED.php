<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$empArr = explode(":", $_POST['EMP_CODE']);
$_POST['EMP_CODE'] = $empArr[0];

$json   = json_decode(DBUpdatePOST($_POST,'tb_schedule_class_day','id'), true);
$sql = $json['data'];
//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
