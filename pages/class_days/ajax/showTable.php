<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$date_start  = $_POST['date_start'];
$date_end    = $_POST['date_end'];

$con= "";
if($_SESSION['branchCode'] != ""){
  $con = " and s.branch_code = '".$_SESSION['branchCode']."' ";
}
?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}
</style>
<div class="autoOverflow">
<table class="table table-bordered table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th>วันที่</th>
      <th>เวลา</th>
      <th>Class Image</th>
      <th>Class Name</th>
      <th>Trainer Name</td>
      <th>Trainer Nickname</td>
      <th style="width:100px">จำนวน</th>
      <th style="width:100px">ค่าจ้าง</th>
      <th style="width:40px">Edit</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT sc.*,c.name_class,c.image_class, CONCAT(e.EMP_TITLE, e.EMP_NAME, ' ', e.EMP_LASTNAME) AS EMP_NAME , EMP_NICKNAME
      FROM tb_schedule_class_day sc, t_classes c, data_mas_employee e, tb_schedule_class s
      where sc.id_class = c.id_class and sc.EMP_CODE = e.EMP_CODE and sc.date_class between '$date_start' and '$date_end'
      and s.schedule_id = sc.schedule_id $con
      ORDER BY sc.date_class,sc.time_start";

      //echo $sqls;

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td align="center"><?= DateThai($rows[$i]['date_class'])?></td>
      <td align="center"><?=$rows[$i]['time_start']." - ".$rows[$i]['time_end'];?></td>
      <td align="center"> <img src="<?= $rows[$i]['image_class'];?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></td>
      <td align="left"><?=$rows[$i]['name_class'];?></td>
      <td align="left"><?=$rows[$i]['EMP_NAME'];?></td>
      <td align="left"><?=$rows[$i]['EMP_NICKNAME'];?></td>
      <td align="right"><?=$rows[$i]['unit'];?></td>
      <td align="right"><?=$rows[$i]['EMP_WAGE'];?></td>
      <td align="center">
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('<?=$rows[$i]['id']?>')">Edit</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   })
  })
</script>
