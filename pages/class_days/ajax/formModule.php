<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$id = $_POST['id'];

$optionClasses  = "";
$optionTrainer  = "";

$sqls   = "SELECT * FROM tb_schedule_class_day where id = '$id'";

$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$rows       = $json['data'];

$id         = $rows[0]['id'];
$id_class   = $rows[0]['id_class'];
$empCode    = $rows[0]['EMP_CODE'];
$unit       = $rows[0]['unit'];
$time_start = $rows[0]['time_start'];
$time_end   = $rows[0]['time_end'];
$EMP_WAGE   = $rows[0]['EMP_WAGE'];
$date_class   = $rows[0]['date_class'];


$sql      = "SELECT * FROM t_classes where is_active = 'Y' order by seq";

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

for($i=0 ; $i < $dataCount ; $i++) {

  $idClass      = $rows[$i]['id_class'];
  $name_class   = $rows[$i]['name_class'];

  $selected =  ' ';
  if($idClass == $id_class && $id_class != ""){
    $selected = 'selected="selected"';
  }
  $optionClasses .= '<option value="'.$idClass.'" '.$selected.'>'.$name_class.'</option>';
}

$sql      = "SELECT * FROM data_mas_employee where 	(EMP_IS_TRAINER = 'Y' OR EMP_IS_INSTRUCTOR = 'Y') and DATA_DELETE_STATUS = 'N' order by EMP_CODE";
$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

for($i=0 ; $i < $dataCount ; $i++) {

  $EMP_CODE     = $rows[$i]['EMP_CODE'];
  $name         = $rows[$i]['EMP_NICKNAME'].":".$rows[$i]['EMP_NAME']." ".$rows[$i]['EMP_LASTNAME'];

  $val  = $rows[$i]['EMP_CODE'].":".$rows[$i]['EMP_WAGE'];

  $selected =  ' ';
  if($empCode == $EMP_CODE && $empCode != ""){
    $selected = 'selected="selected"';
  }
  $optionTrainer .= '<option value="'.$val.'" '.$selected.'>'.$name.'</option>';
}

$btn = 'Add';
?>
<input type='hidden' name="id" value="<?=$id  ?>" />
<div class="modal-body" id="formSc">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>Class Name</label>
        <select id="id_class" class="form-control select2" name="id_class" style="width: 100%;" required >
          <option value="" >&nbsp;</option>
          <?= $optionClasses ?>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่</label>
            <input type='text' class="form-control py" value="<?= DateThai($date_class); ?>"  style="background-color:#fff;" required readonly />
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เริ่ม เวลา</label>
        <div class='input-group timepicker' >
            <input type='text' class="form-control py" name="time_start" id='timeStart'  value="<?=$time_start?>"  style="background-color:#fff;" required />
            <span class="input-group-addon">
              <i class="fa fa-clock-o py"></i>
            </span>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ถึง เวลา</label>
        <div class='input-group timepicker' >
            <input type='text' class="form-control"  name="time_end" id='timeEnd'  value="<?=$time_end?>" style="background-color:#fff;" required/>
            <span class="input-group-addon">
              <i class="fa fa-clock-o py"></i>
            </span>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Trainer</label>
        <select id="EMP_CODE" class="form-control select2" name="EMP_CODE" style="width: 100%;" required >
          <option value="" >&nbsp;</option>
          <?= $optionTrainer ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
          <label>จำนวน</label>
          <input type="number" value="<?=$unit?>" name="unit" class="form-control" id="unit" style="text-align:right;" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
          <label>ราคาค่าจ้าง</label>
          <input type="number" value="<?=$EMP_WAGE ?>" name="EMP_WAGE" class="form-control" id="EMP_WAGE" style="text-align:right;" required>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary btn-flat" style="width:130px;">Update changes</button>
</div>
<script>
var defaults = {
calendarWeeks: true,
showClear: true,
showClose: true,
allowInputToggle: true,
useCurrent: false,
ignoreReadonly: true,
toolbarPlacement: 'top',
locale: 'nl',
icons: {
  time: 'fa fa-clock-o',
  date: 'fa fa-calendar',
  up: 'fa fa-angle-up',
  down: 'fa fa-angle-down',
  previous: 'fa fa-angle-left',
  next: 'fa fa-angle-right',
  today: 'fa fa-dot-circle-o',
  clear: 'fa fa-trash',
  close: 'fa fa-times'
}
};
$(function () {
  setWAGE();
  var optionsTime = $.extend({}, defaults, {format:'HH:mm'});
  $('.timepicker').datetimepicker(optionsTime);
  $('.select2').select2();

  $( "#EMP_CODE" ).change(function() {
    setWAGE();
  });
})

function setWAGE()
{
  var empCodeVal  = $('#EMP_CODE option:selected').val().split(":");
  var wage = empCodeVal[1];
  //alert(wage);
  if(wage > 0){
    $("#EMP_WAGE").val(wage);
  }
}
</script>
