$(function () {

    showTable();

    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   });


   $('#daterage').daterangepicker(
     {
       locale: {
         format: 'DD/MM/YYYY',
         daysOfWeek: [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        monthNames: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
       }
     }
   );
  })

  $('#formAddModule').on('submit', function(event) {
    event.preventDefault();
    if ($('#formAddModule').smkValidate()) {
      $.ajax({
          url: 'ajax/AED.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar({
          element:'body',
          status:'start',
          bgColor: '#000',
          barColor: '#fff',
          content: 'Loading...'
        });
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formAddModule').smkClear();
          showTable();
          showSlidebar();
          $.smkAlert({text: data.message,type: data.status});
          $('#myModal').modal('toggle');
        }, 1000);
      });
    }
  });


  function showTable(){
    var daterage = $('#daterage').val();
    var res = daterage.split("-");
    var date_start = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
    var date_end = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");
    $.post("ajax/showTable.php",{date_start:date_start,date_end:date_end})
    .done(function( data ) {
      $("#showTable").html( data );
    });
  }

 function showForm(id)
 {
   $.post("ajax/formModule.php",{id:id})
     .done(function( data ) {
       $('#myModal').modal({backdrop:'static'});
       $('#show-form').html(data);
   });
 }
