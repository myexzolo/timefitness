<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Time Fitness | CLASS SCHEDULE</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/class_days.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
            CLASS SCHEDULE
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Schedule Classes</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //nclude("../../inc/boxes.php");
              $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <div align="center">
                      <table>
                        <tr>
                          <td style="padding:5px;"><div class="form-group">วันที่เริ่ม - สิ้นสุด</div></td>
                          <td style="padding:5px;">
                            <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="daterage"  value="<?= $date; ?>" required>
                              </div>
                            </div>
                          </td>
                          <td style="padding:5px;">
                          <div class="form-group">
                            <button onclick="showTable()" class="btn btn-success btn-flat"><i class="fa fa-search"></i> ค้นหา</button>
                          </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                    </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>
              </div>
              </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Management Clesses</h4>
              </div>
              <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
              <!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                <div id="show-form"></div>
              </form>
            </div>
            </div>
          </div>
        </div>
        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/class_days.js"></script>
    </body>
  </html>
