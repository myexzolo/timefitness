<!DOCTYPE html>
<?php
if(!isset($_SESSION))
{
    session_start();
}
include('../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$empName    = $_POST['empName'];
$repName    = $_POST['repName'];
$branchCode = $_SESSION['branchCode'];

// $date_start = '2019/08/01';
// $date_end   = '2019/08/31';
// $branchCode = $_SESSION['branchCode'];
// $empCode    = "";

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}

?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GYM Monkey | Report</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <?php
    include("../../inc/css-header.php");
    include("../../inc/js-footer.php");
    $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
  ?>
  <link rel="stylesheet" href="css/rep01.css">
</head>
<style>

body {
  color: #fff;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    border: 0px solid #fff;
    font-size: 12px;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
  border-top: 0px solid #fff;
}

@page {
  margin: 1cm;
  size: landscape;
}

@page:right{
  @bottom-right {
   content: counter(page) " of " counter(pages);
  }
}

@media print {
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
      border: 1px solid #000;
  }
  .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 1px solid #000;
  }
}

th {
  text-align: center;
}

</style>
<body>
  <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">
    <?=$repName ?>
  </div>
  <div align="center" style="font-weight:bold;font-size:15px;padding:5px;">
    <?=$empName ?>
  </div>
  <br>
  <table class="table" id="tableDisplay" style="width:100%" >
    <thead>
      <tr class="text-center">
        <th style="width:50px;vertical-align: middle;">No</th>
        <th style="vertical-align: middle;">พนักงาน</th>
        <th style="width:100px;vertical-align: middle;">ยอดขาย NEW</th>
        <th style="width:85px;vertical-align: middle;">คอมขาย %</th>
        <th style="width:100px;vertical-align: middle;">ค่าคอมขาย NEW</th>
        <th style="width:110px;vertical-align: middle;">ยอดขาย RENEW</th>
        <th style="width:90px;vertical-align: middle;">คอมขาย %</th>
        <th style="width:120px;vertical-align: middle;">ค่าคอมขาย RENEW</th>
        <th style="width:130px;vertical-align: middle;">ยอดเงินรวมค่าคอม</td>
      </tr>
    </thead>
    <tbody>
      <?php
          $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
          FROM data_mas_employee e
          WHERE EMP_IS_SALE = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
          //echo $sqls;
          $querys     = DbQuery($sql,null);
          $json       = json_decode($querys, true);
          $errorInfo  = $json['errorInfo'];
          $dataCount  = $json['dataCount'];
          $rows       = $json['data'];

          $x = 0;
          $sumsaleComNew = 0;
          $sumsaleComReNew = 0;
          $sumsaleTotalCom = 0;

          for($i=0 ; $i < $dataCount ; $i++) {

            $EMP_CODE = $rows[$i]['EMP_CODE'];

            $sql ="SELECT pp.*,CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME,p.PERSON_NICKNAME
                   FROM trans_package_person pp, person p
                   where pp.sale_code = '$EMP_CODE' and  pp.status not in ('D') and pp.pakage_transfer_id = 0 and  p.PERSON_CODE = pp.person_code
                   and pp.create_date between '$date_start' and '$date_end' and pp.company_code = '$branchCode' ";

            $query      = DbQuery($sql,null);
            $js         = json_decode($query, true);
            $dataCountP = $js['dataCount'];
            $rowP       = $js['data'];

            $sumNew   = 0;
            $sumReNew = 0;
            for($x=0 ; $x < $dataCountP ; $x++)
            {
              $type_buy                 = $rowP[$x]['type_buy'];
              $package_price_total      = $rowP[$x]['package_price_total'];
              $package_name             = $rowP[$x]['package_name'];
              $person_code              = $rowP[$x]['person_code'];
              $person_name              = $rowP[$x]['PERSON_NAME'];
              $person_nickName          = $rowP[$x]['PERSON_NICKNAME'];

              if($type_buy == "N"){
                $sumNew += $package_price_total;
              }else if($type_buy == "R")
              {
                $sumReNew += $package_price_total;
              }
            }
            if($sumNew > 0){
              $sumNew = ($sumNew/1.07); //ถอด vat
            }

            if($sumReNew > 0){
              $sumReNew = ($sumReNew/1.07); //ถอด vat
            }


            $sqlp ="SELECT * FROM tb_com_sale WHERE $sumNew BETWEEN start AND end and company_code = '$branchCode'";
            $queryp      = DbQuery($sqlp,null);
            $jsp         = json_decode($queryp, true);
            $rowp        = $jsp['data'];
            $percentNew  = isset($rowp[0]['percent_new'])?$rowp[0]['percent_new']:0;

            $saleComNew  = ($sumNew*$percentNew)/100;


            $sqlp ="SELECT * FROM tb_com_sale WHERE $sumReNew BETWEEN start AND end and company_code = '$branchCode'";
            $queryp     = DbQuery($sqlp,null);
            $jsp        = json_decode($queryp, true);
            $rowp       = $jsp['data'];
            $percentRenew  = isset($rowp[0]['percent_renew'])?$rowp[0]['percent_renew']:0;

            $saleComReNew  = ($sumReNew*$percentRenew)/100;

            $saleTotalCom     = $saleComReNew + $saleComNew;

            $sumsaleComNew   += $saleComNew;
            $sumsaleComReNew += $saleComReNew;
            $sumsaleTotalCom += $saleTotalCom;
      ?>
        <tr class="text-center">
          <td align="center"><?=$i+1 ?></td>
          <td align="left"><?= $rows[$i]['EMP_NAME']." ($EMP_CODE)" ?></td>
          <td align="right"><?= number_format($sumNew,2);?></td>
          <td align="right"><?= $percentNew ?></td>
          <td align="right"><?= number_format($saleComNew,2);?></td>
          <td align="right"><?= number_format($sumReNew,2);?></td>
          <td align="right"><?= $percentRenew ?></td>
          <td align="right"><?= number_format($saleComReNew,2);?></td>
          <td align="right"><?= number_format($saleTotalCom,2);?></td>
        </tr>
      <?php
      }
      if($dataCount > 0){
      ?>

      <tr class="text-center">
        <td colspan="4" align="right"><b>รวม</b></td>
        <td align="right"><b><?= number_format($sumsaleComNew,2);?></b></td>
        <td align="right"></td>
        <td align="right"></td>
        <td align="right"><b><?= number_format($sumsaleComReNew,2);?></b></td>
        <td align="right"><b><?= number_format($sumsaleTotalCom,2);?></b></td>
      </tr>
      <?php
      }
      ?>

    </tbody>
  </table>
</body>
</html>
<script>
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 100);
  });
</script>
