<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$branchCode = $_SESSION['branchCode'];

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}
?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
  vertical-align: middle;

}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px;vertical-align: middle;">No</th>
      <th style="vertical-align: middle;">พนักงาน</th>
      <th style="width:130px;vertical-align: middle;">ยอดขาย NEW</th>
      <th style="width:90px;vertical-align: middle;">คอมขาย %</th>
      <th style="width:100px;vertical-align: middle;">ค่าคอมขาย NEW</th>
      <th style="width:150px;vertical-align: middle;">ยอดขาย RENEW</th>
      <th style="width:90px;vertical-align: middle;">คอมขาย %</th>
      <th style="width:120px;vertical-align: middle;">ค่าคอมขาย RENEW</th>
      <th style="width:150px;vertical-align: middle;">ยอดเงินรวมค่าคอม</td>
    </tr>
  </thead>
  <tbody>
<?php
    $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
    FROM data_mas_employee e
    WHERE EMP_IS_SALE = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
    //echo $sqls;
    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    $x = 0;
    $sumsaleComNew = 0;
    $sumsaleComReNew = 0;
    $sumsaleTotalCom = 0;

    for($i=0 ; $i < $dataCount ; $i++) {

      $EMP_CODE = $rows[$i]['EMP_CODE'];

      $sql ="SELECT pp.*,CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME,p.PERSON_NICKNAME
             FROM trans_package_person pp, person p
             where pp.sale_code = '$EMP_CODE' and  pp.status not in ('D') and pp.pakage_transfer_id = 0 and  p.PERSON_CODE = pp.person_code
             and pp.create_date between '$date_start' and '$date_end' and pp.company_code = '$branchCode' ";

      $query      = DbQuery($sql,null);
      $js         = json_decode($query, true);
      $dataCountP = $js['dataCount'];
      $rowP       = $js['data'];

      $sumNew   = 0;
      $sumReNew = 0;
      for($x=0 ; $x < $dataCountP ; $x++)
      {
        $type_buy                 = $rowP[$x]['type_buy'];
        $net_total                = $rowP[$x]['net_total'];
        $package_name             = $rowP[$x]['package_name'];
        $person_code              = $rowP[$x]['person_code'];
        $person_name              = $rowP[$x]['PERSON_NAME'];
        $person_nickName          = $rowP[$x]['PERSON_NICKNAME'];

        if($type_buy == "N"){
          $sumNew += $net_total;
        }else if($type_buy == "R")
        {
          $sumReNew += $net_total;
        }
      }
      if($sumNew > 0){
        $sumNew = ($sumNew/1.07); //ถอด vat
      }

      if($sumReNew > 0){
        $sumReNew = ($sumReNew/1.07); //ถอด vat
      }


      $sqlp ="SELECT * FROM tb_com_sale WHERE $sumNew BETWEEN start AND end and company_code = '$branchCode'";
      $queryp      = DbQuery($sqlp,null);
      $jsp         = json_decode($queryp, true);
      $rowp        = $jsp['data'];
      $percentNew  = isset($rowp[0]['percent_new'])?$rowp[0]['percent_new']:0;

      $saleComNew  = ($sumNew*$percentNew)/100;


      $sqlp ="SELECT * FROM tb_com_sale WHERE $sumReNew BETWEEN start AND end and company_code = '$branchCode'";
      $queryp     = DbQuery($sqlp,null);
      $jsp        = json_decode($queryp, true);
      $rowp       = $jsp['data'];
      $percentRenew  = isset($rowp[0]['percent_renew'])?$rowp[0]['percent_renew']:0;

      $saleComReNew  = ($sumReNew*$percentRenew)/100;

      $saleTotalCom     = $saleComReNew + $saleComNew;

      $sumsaleComNew   += $saleComNew;
      $sumsaleComReNew += $saleComReNew;
      $sumsaleTotalCom += $saleTotalCom;
?>
  <tr class="text-center">
    <td align="center"><?=$i+1 ?></td>
    <td align="left"><?= $rows[$i]['EMP_NAME']." ($EMP_CODE)" ?></td>
    <td align="right"><?= number_format($sumNew,2);?></td>
    <td align="right"><?= $percentNew ?></td>
    <td align="right"><?= number_format($saleComNew,2);?></td>
    <td align="right"><?= number_format($sumReNew,2);?></td>
    <td align="right"><?= $percentRenew ?></td>
    <td align="right"><?= number_format($saleComReNew,2);?></td>
    <td align="right"><?= number_format($saleTotalCom,2);?></td>
  </tr>
<?php
}
if($dataCount > 0){
?>

<tr class="text-center">
  <td colspan="4" align="right"><b>รวม</b></td>
  <td align="right"><b><?= number_format($sumsaleComNew,2);?></b></td>
  <td align="right"></td>
  <td align="right"></td>
  <td align="right"><b><?= number_format($sumsaleComReNew,2);?></b></td>
  <td align="right"><b><?= number_format($sumsaleTotalCom,2);?></b></td>
</tr>
<?php
}
?>

  </tbody>
</table>
<div align="right">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
  <button onclick="" class="btn btn-info btn-flat" style="width:150px;display:none"><i class="fa fa-print"></i>  พิมพ์แยกรายคน</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });

   $('.group',this).hide();
  })

  function changetr(obj,trId)
  {
    if($(obj).hasClass("fa-plus-square")){
      $('.'+ trId).show();
      $(obj).addClass("fa-minus-square");
      $(obj).removeClass("fa-plus-square");
    }else{
      $('.'+ trId).hide();
      $(obj).addClass("fa-plus-square");
      $(obj).removeClass("fa-minus-square");
    }
  }

</script>
