<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Time Fitness | Report</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/rep01.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              REPORT
              <small>รายงานค่าสอน Class</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">รายงานค่าสอน Class</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
                $optionTrainer = "";
                $branchCode = $_SESSION['branchCode'];

                $sql      = "SELECT * FROM data_mas_employee where 	((EMP_IS_TRAINER = 'Y' and COMPANY_CODE ='$branchCode') OR EMP_IS_INSTRUCTOR = 'Y') and DATA_DELETE_STATUS = 'N'  order by EMP_CODE";
                $querys     = DbQuery($sql,null);
                $json       = json_decode($querys, true);
                $dataCount  = $json['dataCount'];
                $rows       = $json['data'];

                for($i=0 ; $i < $dataCount ; $i++) {

                  $EMP_CODE     = $rows[$i]['EMP_CODE'];
                  $name         = $rows[$i]['EMP_NICKNAME']." : ".$rows[$i]['EMP_NAME']." ".$rows[$i]['EMP_LASTNAME'];

                  $val  = $rows[$i]['EMP_CODE'];

                  $optionTrainer .= '<option value="'.$val.'">'.$name.'</option>';
                }
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <div align="center">
                      <table>
                        <tr>
                          <td style="padding:5px;width:120px;"><div class="form-group">วันที่เริ่ม - สิ้นสุด : </div></td>
                          <td style="padding:5px;">
                            <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="daterage"  value="<?= $date; ?>" required>
                              </div>
                            </div>
                          </td>
                          <td style="padding:5px;width:100px;"><div class="form-group" align="right">ครูผู้สอน : </div></td>
                          <td style="padding:5px;">
                            <div class="form-group">
                              <select id="empCode" class="form-control select2" style="width: 250px;" >
                                <option value="" selected>&nbsp;</option>
                                <?= $optionTrainer; ?>
                              </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:5px;" colspan="4" align="center">
                              <div class="form-group">
                                <button onclick="showTable()" class="btn btn-success btn-flat" style="width:100px"><i class="fa fa-search"></i> ค้นหา</button>
                                <button onclick="reset('<?= $date?>')" class="btn btn-flat" style="width:100px">Reset</button>
                              </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                    </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">
                      รายงานสรุปค่าจ้างสอน Class ตั้งแต่วันที่ <span id="startDate"></span> ถึง <span id="endDate"></span>
                    </div>
                    <div align="center" style="font-weight:bold;font-size:15px;padding:5px;">
                      ครูผู้สอน <span id="nameTrainer"></span>
                    </div>
                    <div align="right">
                      <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
                      <button onclick="" class="btn btn-info btn-flat" style="width:150px;display:none"><i class="fa fa-print"></i>  พิมพ์แยกรายคน</button>
                    </div>
                    <div id="showTable"></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/rep01.js"></script>
    </body>
  </html>
