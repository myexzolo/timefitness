<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start'];
$date_end   = $_POST['date_end'];
$empCode    = $_POST['empCode'];
$branchCode = $_SESSION['branchCode'];

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}



?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:30px"></th>
      <th style="width:50px">No</th>
      <th>วันที่</th>
      <th>เวลา</th>
      <th>รายการ</th>
      <th style="width:150px">สถานะ</th>
      <th style="width:100px">ค่าจ้าง</td>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql ="SELECT scd.*,c.name_class,e.EMP_NICKNAME,CONCAT(e.EMP_TITLE, e.EMP_NAME, ' ', e.EMP_LASTNAME) as EMP_NAME
      FROM tb_schedule_class_day scd, t_classes c, data_mas_employee e, tb_schedule_class s
      WHERE scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE  and scd.schedule_id = s.schedule_id
      and s.branch_code = '$branchCode' $con and s.is_active = 'Y'
      and scd.date_class between '$date_start' and '$date_end' order by e.EMP_CODE,scd.date_class";
      //echo $sql;
      $querys     = DbQuery($sql,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      $x = 0;
      $nameEmpTmp = "";
      $sumWage = 0;
      $sumWageAll = 0;
      for($i=0 ; $i < $dataCount ; $i++) {

        // id_class
        $name_class   = $rows[$i]['name_class'];
        $EMP_NAME     = $rows[$i]['EMP_NAME'];
        $EMP_NICKNAME = $rows[$i]['EMP_NICKNAME'];
        $sign_emp     = $rows[$i]['sign_emp'];
        $EMP_WAGE     = $rows[$i]['EMP_WAGE'];
        // image_class
        // is_active
        // seq
        if($nameEmpTmp != $EMP_NAME)
        {
          if($x > 0){
        ?>
        <script>
            var sumWh = 0;
            $( ".wage_<?=$x;?>").each(function() {
              sumWh += isNumeric($(this).html().replace(",", ""));
            });
            $('#sum_<?=$x; ?>').html(addCommas(sumWh.toFixed(2)));
        </script>
        <?php
          }
          $nameEmpTmp = $EMP_NAME;
          $x++;
    ?>
      <tr>
        <td align="center"><i class="fa fa-minus-square" style="font-size:18px;" onclick="changetr(this,'tr_<?=$x;?>')"></i></td>
        <td align="center"><?= $x; ?></td>
        <td colspan="4" ><?= $EMP_NAME."  (".$EMP_NICKNAME.")"; ?></td>
        <td id="sum_<?=$x; ?>" align="right"></td>
      </tr>
    <?php
        }
        $status = "";
        if($sign_emp == "Y"){
          $sumWage += $EMP_WAGE;
          $sumWageAll += $EMP_WAGE;
          $status = "บันทึกแล้ว";
        }else{
          $status = "<span class=\"alert\">รอการบันทึก</span>";
          $EMP_WAGE = 0;
        }
    ?>
    <tr class="tr_<?=$x;?> group">
      <td align="center"></td>
      <td align="center"></td>
      <td align="center"><?=DateThai($rows[$i]['date_class']);?></td>
      <td align="center"><?= $rows[$i]['time_start']." - ".$rows[$i]['time_end'];?></td>
      <td><?=$name_class;?></td>
      <td align="center"><?=$status;?></td>
      <td align="right" align="center" class="wage_<?=$x;?>">
        <?= number_format($EMP_WAGE,2);?>
      </td>
    </tr>
    <?php
      if($dataCount == ($i + 1)){
    ?>
    <script>
        var sumWh = 0;
        $( ".wage_<?=$x;?>").each(function() {
          sumWh += isNumeric($(this).html().replace(",", ""));
        });
        $('#sum_<?=$x; ?>').html(addCommas(sumWh.toFixed(2)));
    </script>
    <tr>
      <td colspan="6" align="right"><b>ยอดรวม</b></td>
      <td align="right"><b><?= number_format($sumWageAll,2);?></b></td>
    </tr>
    <?php
      }

    } ?>
  </tbody>
</table>
<div align="right">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
  <button onclick="" class="btn btn-info btn-flat" style="width:150px;display:none"><i class="fa fa-print"></i>  พิมพ์แยกรายคน</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });

   $('.group',this).hide();
  })

  function changetr(obj,trId)
  {
    if($(obj).hasClass("fa-plus-square")){
      $('.'+ trId).show();
      $(obj).addClass("fa-minus-square");
      $(obj).removeClass("fa-plus-square");
    }else{
      $('.'+ trId).hide();
      $(obj).addClass("fa-plus-square");
      $(obj).removeClass("fa-minus-square");
    }
  }

</script>
