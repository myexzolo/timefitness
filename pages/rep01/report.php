<!DOCTYPE html>
<?php
if(!isset($_SESSION))
{
    session_start();
}
include('../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$date_start = $_POST['date_start'];
$date_end   = $_POST['date_end'];
$empCode    = $_POST['empCode'];
$empName    = $_POST['empName'];
$repName    = $_POST['repName'];
$branchCode = $_SESSION['branchCode'];

// $date_start = '2019/08/01';
// $date_end   = '2019/08/31';
// $branchCode = $_SESSION['branchCode'];
// $empCode    = "";

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}

?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GYM Monkey | Report</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <?php
    include("../../inc/css-header.php");
    include("../../inc/js-footer.php");
    $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
  ?>
  <link rel="stylesheet" href="css/rep01.css">
  <link rel="stylesheet" href="../../dist/css/paper.css">
</head>
<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<style>

body {
  color: #fff;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    border: 0px solid #fff;
    font-size: 12px;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
  border-top: 0px solid #fff;
}

@page {
  margin: 1cm;
  size: portrait;
}

@page :first {
  margin-top: 1cm; /* Top margin on first page 10cm */
  size: portrait;
}

@media print {
  body
  {
    margin: 0;
    border: solid 0px #fff;
  }
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
      border: 1px solid #000;
  }
  .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 1px solid #000;
  }
}

th {
  text-align: center;
}

</style>
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="padding-10mm">

    <!-- Write HTML just like a web page -->
    <article>
      <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">
        <?=$repName ?>
      </div>
      <div align="center" style="font-weight:bold;font-size:15px;padding:5px;">
        <?=$empName ?>
      </div>
      <br>
      <table class="table"  id="tableDisplay" style="width:100%" >
        <thead>
          <tr>
            <th style="width:50px">No</th>
            <th >วันที่</th>
            <th >เวลา</th>
            <th >รายการ</th>
            <th style="width:120px">สถานะ</th>
            <th style="width:100px">ค่าจ้าง</td>
          </tr>
        </thead>
        <tbody>
          <?php
            $sql ="SELECT scd.*,c.name_class,e.EMP_NICKNAME,CONCAT(e.EMP_TITLE, e.EMP_NAME, ' ', e.EMP_LASTNAME) as EMP_NAME
            FROM tb_schedule_class_day scd, t_classes c, data_mas_employee e, tb_schedule_class s
            WHERE scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE  and scd.schedule_id = s.schedule_id
            and s.branch_code = '$branchCode' $con
            and scd.date_class between '$date_start' and '$date_end' order by e.EMP_CODE,scd.date_class";
            //echo $sqls;
            $querys     = DbQuery($sql,null);
            $json       = json_decode($querys, true);
            $errorInfo  = $json['errorInfo'];
            $dataCount  = $json['dataCount'];
            $rows       = $json['data'];

            $x = 0;
            $num = 0;
            $nameEmpTmp = "";
            $sumWage = 0;
            $sumWageAll = 0;
            for($i=0 ; $i < $dataCount ; $i++) {

              // id_class
              $name_class   = $rows[$i]['name_class'];
              $EMP_NAME     = $rows[$i]['EMP_NAME'];
              $EMP_NICKNAME = $rows[$i]['EMP_NICKNAME'];
              $sign_emp     = $rows[$i]['sign_emp'];
              $EMP_WAGE     = $rows[$i]['EMP_WAGE'];
              // image_class
              // is_active
              // seq
              if($nameEmpTmp != $EMP_NAME)
              {
                if($x > 0){
              ?>
              <tr>
                <td colspan="5" align="right"><b>รวม</b></td>
                <td align="right"><b><?= number_format($sumWage,2);?></b></td>
              </tr>
              <?php
                  $sumWage = 0;
                }
                $nameEmpTmp = $EMP_NAME;
                $x++;
                $num = 0;
          ?>
            <tr>
              <td colspan="6" ><b><?= $EMP_NAME."  (".$EMP_NICKNAME.")"; ?></b></td>
            </tr>
          <?php
              }
              $status = "";
              if($sign_emp == "Y"){
                $sumWage += $EMP_WAGE;
                $sumWageAll += $EMP_WAGE;
                $status = "บันทึกแล้ว";
              }else{
                $status = "<span class=\"alert\">รอการบันทึก</span>";
                $EMP_WAGE = 0;
              }
              $num++;
          ?>
          <tr>
            <td align="center"><?= $num ?></td>
            <td align="center"><?=DateThai($rows[$i]['date_class']);?></td>
            <td align="center"><?= $rows[$i]['time_start']." - ".$rows[$i]['time_end'];?></td>
            <td><?=$name_class;?></td>
            <td align="center"><?=$status;?></td>
            <td align="right" align="center" class="wage_<?=$x;?>">
              <?= number_format($EMP_WAGE,2);?>
            </td>
          </tr>
          <?php
            if($dataCount == ($i + 1)){
          ?>
          <tr>
            <td colspan="5" align="right"><b>รวม</b></td>
            <td align="right"><b><?= number_format($sumWage,2);?></b></td>
          </tr>
          <tr>
            <td colspan="5" align="right"><b>ยอดรวมทั้งหมด</b></td>
            <td align="right"><b><?= number_format($sumWageAll,2);?></b></td>
          </tr>
          <?php
            }
          } ?>
        </tbody>
      </table>
    </article>
  </section>
</body>
</html>
<script>
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 1000);
  });
</script>
