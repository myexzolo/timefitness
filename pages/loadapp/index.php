<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Time Fitness | Load QR</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); ?>
  <link rel="stylesheet" href="css/login.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">
    <b>GET THE APP</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo">
      <img src="../../image/logo_ym.png" style="width:120px;">
    </div>
    <br>
    <div align="center">
        <a class="sh-store-link" href="https://apps.apple.com/th/app/time-fitness-studio/id1497419913?l=th" target="_blank">
            <img class="sh-store-link__image" src="../../image/apple-en.png" alt="">
        </a>
    </div>
    <div align="center">
        <a class="sh-store-link" href="https://play.google.com/store/apps/details?id=com.onesittichok.timeFitnessStudio" target="_blank" >
            <img class="sh-store-link__image" src="../../image/google-en.png" alt="">
        </a>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/login.js"></script>
</body>
</html>
