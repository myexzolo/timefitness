<!DOCTYPE html>

<?php

if(!isset($_SESSION))

{

    session_start();

}

include('../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");

header("Cache-Control: no-store, no-cache, must-revalidate");

header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";



$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";

$empCode    = $_POST['empCode'];
$empName    = $_POST['empName'];
$repName    = $_POST['repName'];

$sortData   = $_POST['sortData'];

$branchCode = $_SESSION['branchCode'];



// $date_start = '2019/08/01';

// $date_end   = '2019/08/31';

// $branchCode = $_SESSION['branchCode'];

// $empCode    = "";



if($empCode != "")

{

  $con = " and e.EMP_CODE = '$empCode' ";

}



if($sortData == 'date')

{

  $sort = " cp.checkin_date";

}else if($sortData == 'customer')

{

  $sort = " cp.person_code ";

}





?>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>GYM Monkey | Report</title>

  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php

    include("../../inc/css-header.php");

    include("../../inc/js-footer.php");

    $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];

  ?>

  <link rel="stylesheet" href="css/rep01.css">

  <link rel="stylesheet" href="../../dist/css/paper.css">

</head>

<!-- Set "A5", "A4" or "A3" for class name -->

<!-- Set also "landscape" if you need -->

<style>



body {

  color: #fff;

}



.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {

    border: 0px solid #fff;

    font-size: 12px;

}

.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {

  border-top: 0px solid #fff;
}



@page {

  margin: 1cm;

  size: portrait;

}



@page :first {

  margin-top: 1cm; /* Top margin on first page 10cm */

  size: portrait;

}



@media print {

  body

  {

    margin: 0;

    border: solid 0px #fff;

    font-size: 10px;

  }

  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {

      border: 1px solid #000;

      font-size: 10px;

  }

  .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {

    border-top: 1px solid #000;

    font-size: 10px;

  }



  th

  {

    text-align: center;

    background-color: #ccc !important;

  }

}





th {

  text-align: center;

}



</style>

<body class="A4">



  <!-- Each sheet element should have the class "sheet" -->

  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

  <section class="padding-10mm">



    <!-- Write HTML just like a web page -->

    <article>

      <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">

        <?=$repName ?>

      </div>

      <div align="center" style="font-weight:bold;font-size:15px;padding:5px;">

        <?=$empName ?>

      </div>

      <br>

      <table class="table"  id="tableDisplay" style="width:100%" >

        <thead>

          <tr class="text-center">

            <th style="width:40px">No</th>

            <th style="width:115px;vertical-align: middle;">วันที่เทรน</th>
            <th style="width:115px;vertical-align: middle;">วันที่ตรวจสอบ</th>

            <th style="width:125px;vertical-align: middle;">ชื่อ - สกุล ลูกค้า</th>

            <th style="width:55px;vertical-align: middle;">ชื่อเล่น</th>

            <th style="vertical-align: middle;">รายการ Package</th>

            <th style="width:70px;vertical-align: middle;">ราคา</th>

            <th style="width:70px;vertical-align: middle;">ราคาก่อนภาษี</th>

            <th style="width:60px;vertical-align: middle;">ค่าเทรน</th>

          </tr>

        </thead>

        <tbody>

          <?php

              $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME

              FROM data_mas_employee e

              WHERE EMP_IS_TRAINER = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";

              //echo $sql;

              $querys     = DbQuery($sql,null);

              $json       = json_decode($querys, true);

              $errorInfo  = $json['errorInfo'];

              $dataCount  = $json['dataCount'];

              $rows       = $json['data'];





              $sumTotal = 0;

              $sumTotals = 0;



              for($i=0 ; $i < $dataCount ; $i++) {

                  $EMP_CODE = $rows[$i]['EMP_CODE'];

                  $EMP_NAME = $rows[$i]['EMP_NAME'];

                  if($i > 0)

                  {

              ?>

              <tr>

                <td colspan="8" align="right"><b>รวม (<?= $numUse."/".$numUseTotal;?>)</b></td>

                <td align="right"><b><?= number_format($sumTotal,2) ?></b></td>

              </tr>

              <?php

                    $sumTotal = 0;



                  }

              ?>

              <tr class="text-center  group" >

                <td colspan="9" align="left"><?=$EMP_NAME." (".$EMP_CODE.")" ?></td>

              </tr>

              <?php





                $sql  ="SELECT cp.*,pp.package_name,pp.net_total,p.PERSON_NICKNAME, CONCAT(p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME,PERSON_NICKNAME

                        FROM trans_checkin_person cp, person p, trans_package_person pp

                        where cp.package_person_id = pp.id and cp.sign_person = 'Y' and  cp.sign_emp = 'Y' and  cp.sign_manager = 'Y' and p.PERSON_CODE = cp.person_code

                        and cp.staus_checkin = 'CO' and cp.sign_person_date BETWEEN '$date_start' AND  '$date_end'

                        and cp.trainer_code = '$EMP_CODE' and cp.company_code = '$branchCode' order by $sort";



               //echo $sql."<br>";



                $query      = DbQuery($sql,null);

                $js         = json_decode($query, true);

                $row        = $js['data'];

                $dataCounts = $js['dataCount'];





                $numUse = 0;

                $numUseTotal = 0;



                for($x=0 ; $x < $dataCounts ; $x++)

                {

                  $price_per_use  = $row[$x]['price_per_use'];

                  $net_total      = $row[$x]['net_total'];

                  if($price_per_use == 0)

                  {

                    $price_per_use = 100;

                  }else{

                    $numUse++;

                    $price_per_use = ($price_per_use/1.07);

                  }



                  $numUseTotal++;



                  $vat = ($net_total/1.07);

                  $sumTotal += $price_per_use;

                  $sumTotals += $price_per_use;





          ?>

                <tr class="tr_<?=$i;?>">

                  <td align="center"><?=$x+1 ?></td>

                  <td align="center"><?= DateTimeThai($row[$x]['checkin_date']); ;?></td>

                  <td align="center"><?= DateTimeThai($row[$x]['sign_person_date']); ;?></td>

                  <td align="left"><?= $row[$x]['PERSON_NAME'] ?></td>

                  <td align="left"><?= $row[$x]['PERSON_NICKNAME'] ;?></td>

                  <td align="left"><?= $row[$x]['package_name'] ?></td>

                  <td align="right"><?= number_format($net_total,2);?></td>

                  <td align="right"><?= number_format($vat,2);?></td>

                  <td align="right"><?= number_format($price_per_use,2);?></td>

                </tr>

          <?php

                }

                if($dataCounts == 0){

          ?>

          <!-- <tr class="text-center">

            <td colspan="7" align="center">&nbsp;</td>

          </tr> -->

          <?php

                }

          }

          if($dataCount > 0){

          ?>

          <tr>

            <td colspan="8" align="right"><b>รวม (<?= $numUse."/".$numUseTotal;?>)</b></td>

            <td align="right"><b><?= number_format($sumTotal,2) ?></b></td>

          </tr>

          <?php

          }

          ?>

          <tr class="group">

            <td colspan="8" align="right"><b>รวมทั้งหมด</b></td>

            <td align="right"><b><?= number_format($sumTotals,2) ?></b></td>

          </tr>

        </tbody>

      </table>

    </article>

  </section>

</body>

</html>

<script>

  $(document).ready(function(){

    setTimeout(function(){

      window.print();

      window.close();

    }, 1000);

  });

</script>
