<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$sortData   = $_POST['sortData'];
$branchCode = $_SESSION['branchCode'];

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}

if($sortData == 'date')
{
  $sort = " cp.checkin_date";
}else if($sortData == 'customer')
{
  $sort = " cp.person_code ";
}

?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
  vertical-align: middle;

}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th style="width:170px;vertical-align: middle;">วันที่เทรน</th>
      <th style="width:100px;vertical-align: middle;">วันที่ตรวจสอบ</th>
      <th style="width:250px;vertical-align: middle;">ชื่อ - สกุล ลูกค้า</th>
      <th style="width:120px;vertical-align: middle;">ชื่อเล่น</th>
      <th style="vertical-align: middle;">รายการ Package</th>
      <th style="width:120px;vertical-align: middle;">ราคา Package </th>
      <th style="width:150px;vertical-align: middle;">ราคา Package ก่อนภาษี</th>
      <th style="width:120px;vertical-align: middle;">ค่าเทรน</th>
    </tr>
  </thead>
  <tbody>
<?php
    $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
    FROM data_mas_employee e
    WHERE EMP_IS_TRAINER = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
    //echo $sql;
    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];


    $sumTotal = 0;
    $sumTotals = 0;

    for($i=0 ; $i < $dataCount ; $i++) {
        $EMP_CODE = $rows[$i]['EMP_CODE'];
        $EMP_NAME = $rows[$i]['EMP_NAME'];
        if($i > 0)
        {
    ?>
    <tr>
      <td colspan="8" align="right"><b>รวม (<?= $numUse."/".$numUseTotal;?>)</b></td>
      <td align="right"><b><?= number_format($sumTotal,2) ?></b></td>
    </tr>
    <?php
          $sumTotal = 0;

        }
    ?>
    <tr class="text-center  group" >
      <td align="center"><i class="fa fa-minus-square" style="font-size:18px;" onclick="changetr(this,'tr_<?=$i;?>')"></i></td>
      <td colspan="8" align="left"><?=$EMP_NAME." (".$EMP_CODE.")" ?></td>
    </tr>
    <?php


      $sql  ="SELECT cp.*,pp.package_name,pp.net_total,p.PERSON_NICKNAME, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME,PERSON_NICKNAME
              FROM trans_checkin_person cp, person p, trans_package_person pp
              where cp.package_person_id = pp.id and cp.sign_person = 'Y' and  cp.sign_emp = 'Y' and  cp.sign_manager = 'Y' and p.PERSON_CODE = cp.person_code
              and cp.staus_checkin = 'CO' and cp.sign_manager_date BETWEEN '$date_start' AND  '$date_end'
              and cp.trainer_code = '$EMP_CODE' and cp.company_code = '$branchCode' order by $sort";

     //echo $sql."<br>";

      $query      = DbQuery($sql,null);
      $js         = json_decode($query, true);
      $row        = $js['data'];
      $dataCounts = $js['dataCount'];


      $numUse = 0;
      $numUseTotal = 0;

      for($x=0 ; $x < $dataCounts ; $x++)
      {
        $price_per_use  = $row[$x]['price_per_use'];
        $net_total      = $row[$x]['net_total'];
        if($price_per_use == 0)
        {
          $price_per_use = 100;
        }else{
          $numUse++;
          $price_per_use = ($price_per_use/1.07);
        }

        $numUseTotal++;

        $vat = ($net_total/1.07);
        $sumTotal += $price_per_use;
        $sumTotals += $price_per_use;


?>
      <tr class="tr_<?=$i;?>">
        <td align="center"><?=$x+1 ?></td>
        <td align="center"><?= DateTimeThai($row[$x]['checkin_date']); ;?></td>
        <td align="center"><?= DateThai($row[$x]['sign_manager_date']); ;?></td>
        <td align="left"><?= $row[$x]['PERSON_NAME'] ?></td>
        <td align="left"><?= $row[$x]['PERSON_NICKNAME'] ;?></td>
        <td align="left"><?= $row[$x]['package_name'] ?></td>
        <td align="right"><?= number_format($net_total,2);?></td>
        <td align="right"><?= number_format($vat,2);?></td>
        <td align="right"><?= number_format($price_per_use,2);?></td>
      </tr>
<?php
      }
      if($dataCounts == 0){
?>
<!-- <tr class="text-center">
  <td colspan="7" align="center">&nbsp;</td>
</tr> -->
<?php
      }
}
if($dataCount > 0){
?>
<tr>
  <td colspan="8" align="right"><b>รวม (<?= $numUse."/".$numUseTotal;?>)</b></td>
  <td align="right"><b><?= number_format($sumTotal,2) ?></b></td>
</tr>
<?php
}
?>
<tr class="group">
  <td colspan="8" align="right"><b>รวมทั้งหมด</b></td>
  <td align="right"><b><?= number_format($sumTotals,2) ?></b></td>
</tr>
  </tbody>
</table>
<div class="pull-right" align="right" style="width:50%;margin-bottom:6px;">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });

   $('.group',this).hide();
  })

  function changetr(obj,trId)
  {
    if($(obj).hasClass("fa-plus-square")){
      $('.'+ trId).show();
      $(obj).addClass("fa-minus-square");
      $(obj).removeClass("fa-plus-square");
    }else{
      $('.'+ trId).hide();
      $(obj).addClass("fa-plus-square");
      $(obj).removeClass("fa-minus-square");
    }
  }

</script>
