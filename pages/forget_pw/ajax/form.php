<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


//$strongPass = 'strong';
$strongPass = 'weak';


?>

<div class="row mar-btn-20" id="action">
</div>
<div class="form-group has-feedback">
  <input type="text" id="username" name="username" class="form-control" placeholder="รหัสพนักงาน" autocomplete="new-password" required>
  <span class="glyphicon glyphicon-user form-control-feedback icons"></span>
</div>
<div class="form-group has-feedback">
  <input type="text" id="idcard" name="id_card" class="form-control" placeholder="เลขที่บัตรประชาชน" maxlength="13" autocomplete="new-password" data-smk-type="decimal" required>
  <span class="fa fa-credit-card form-control-feedback icons"></span>
</div>
<div class="form-group has-feedback">
  <input type="password" id="pass1" name="pass" class="form-control" placeholder="New Password" autocomplete="new-password" required>
  <span class="glyphicon glyphicon-lock form-control-feedback icons"></span>
</div>
<div class="form-group has-feedback">
  <input type="password" id="pass2" class="form-control" placeholder="Re-enter new password" autocomplete="new-password" required>
  <span class="glyphicon glyphicon-lock form-control-feedback icons"></span>
</div>
<div class="row">
  <!-- /.col -->
  <div class="col-xs-12">
    <button type="button" onclick="location.replace('../login/');" class="btn btn-flat pull-left" >Home</button>
    <button type="submit" id="sendForm" class="btn btn-primary btn-flat pull-right" >Reset Password</button>
  </div>
  <!-- /.col -->
</div>
