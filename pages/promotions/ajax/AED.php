<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action           = $_POST['action'];

$package_id       = isset($_POST['package_id'])?$_POST['package_id']:"";
$package_code     = isset($_POST['package_code'])?$_POST['package_code']:"";
$package_name     = isset($_POST['package_name'])?$_POST['package_name']:"";
$package_detail   = isset($_POST['package_detail'])?$_POST['package_detail']:"";
$package_type     = isset($_POST['package_type'])?$_POST['package_type']:"";
$num_use          = isset($_POST['num_use'])?$_POST['num_use']:"0";
$package_unit     = isset($_POST['package_unit'])?$_POST['package_unit']:"";
$max_use          = isset($_POST['max_use'])?$_POST['max_use']:"0";
$package_price    = isset($_POST['package_price'])?$_POST['package_price']:"";
$type_vat         = isset($_POST['type_vat'])?$_POST['type_vat']:"";
$package_status   = isset($_POST['package_status'])?$_POST['package_status']:"";
$notify_num       = isset($_POST['notify_num'])?$_POST['notify_num']:"0";
$notify_unit      = isset($_POST['notify_unit'])?$_POST['notify_unit']:$package_unit;
$seq              = isset($_POST['seq'])?$_POST['seq']:"0";
$branch1          = isset($_POST['branch1'])?$_POST['branch1']:"";
$branch2          = isset($_POST['branch2'])?$_POST['branch2']:"";
$branch3          = isset($_POST['branch3'])?$_POST['branch3']:"";
$branch4          = isset($_POST['branch4'])?$_POST['branch4']:"";
$branch5          = isset($_POST['branch5'])?$_POST['branch5']:"";
$promotion        = isset($_POST['promotion'])?$_POST['promotion']:"";
$show_feature     = isset($_POST['show_feature'])?$_POST['show_feature']:"";
$start_pro_date   = isset($_POST['start_pro_date'])?dateThToEn($_POST['start_pro_date'],"dd/mm/yyyy","/"):"";
$end_pro_date     = isset($_POST['end_pro_date'])?dateThToEn($_POST['end_pro_date'],"dd/mm/yyyy","/"):"";
$img              = isset($_POST['img'])?$_POST['img']:"";
$img_web          = isset($_POST['img_web'])?$_POST['img_web']:"";
$is_active        = isset($_POST['is_active'])?$_POST['is_active']:"";


if($package_unit == "MONTHS"){
  $notify_unit = "DAYS";
}


// $title        = isset($_POST['title'])?$_POST['title']:"";
// $content      = isset($_POST['content'])?$_POST['content']:"";
// $img          = isset($_POST['img'])?$_POST['img']:"";
// $img_web      = isset($_POST['img_web'])?$_POST['img_web']:"";
// $web          = isset($_POST['web'])?$_POST['web']:"";
// $price        = isset($_POST['price'])?$_POST['price']:"";
// $start_date   = isset($_POST['start_date'])?dateThToEn($_POST['start_date'],"dd/mm/yyyy","/"):"";
// $end_date     = isset($_POST['end_date'])?dateThToEn($_POST['end_date'],"dd/mm/yyyy","/"):"";
// $is_active    = isset($_POST['is_active'])?$_POST['is_active']:"";

$company_code   = $_SESSION['branchCode'];
$user_id_update = $_SESSION['member'][0]['user_id'];


if($action != "DEL")
{
  $file = $_FILES['file'];

  if(trim($file["tmp_name"]) != "")
  {
      $pathWeb    = "../../../image/promotions/web/";
      $pathMobile = "../../../image/promotions/mobile/";

      $img        = resizeImageToUpload($file,"","800",$pathMobile,"m_");

      $uploadfile = uploadfile($file,$pathWeb,"w_");
      $img_web    = $uploadfile['image'];

  }
}
$sql = "";
if($action == 'ADD'){
  $sql   = "INSERT INTO data_mas_package (
            package_code,
            package_name,package_detail,
            package_type,num_use,
            package_unit,max_use,
            package_price,type_vat,company_code,
            package_status,notify_num,notify_unit,
            create_by,create_date,seq,branch1,branch2,branch3,branch4,branch5,
            promotion,start_pro_date,end_pro_date,
            img,img_web,show_feature)
           VALUES(
             '$package_code',
             '$package_name','$package_detail',
             '$package_type','$num_use',
             '$package_unit','$max_use',
             '$package_price','$type_vat','$company_code',
             '$package_status','$notify_num','$notify_unit',
             '$user_id_update',NOW(),'$seq','$branch1','$branch2','$branch3','$branch4','$branch5',
             '$promotion','$start_pro_date','$end_pro_date',
             '$img','$img_web','$show_feature'
           )";
}else if($action == 'EDIT'){
  $imgUp = "";
  if(trim($file["tmp_name"]) != "")
  {
    $imgUp =  "web = '$web',img = '$img',";

  }

  $sql = "UPDATE data_mas_package SET
          package_name    = '$package_name',
          package_code    = '$package_code',
          package_detail  = '$package_detail',
          package_type    = '$package_type',
          num_use         = '$num_use',
          package_unit    = '$package_unit',
          max_use         = '$max_use',
          package_price   = '$package_price',
          type_vat        = '$type_vat',
          package_status  = '$package_status',
          company_code    = '$company_code',
          notify_num      = '$notify_num',
          notify_unit     = '$notify_unit',
          seq             = '$seq',
          branch1         = '$branch1',
          branch2         = '$branch2',
          branch3         = '$branch3',
          branch4         = '$branch4',
          branch5         = '$branch5',
          show_feature    = '$show_feature',
          update_date     = NOW(),".$imgUp."
          promotion       = '$promotion',
          start_pro_date  = '$start_pro_date',
          end_pro_date    = '$end_pro_date'
          WHERE package_id = '$package_id'";


  // $sql = "UPDATE t_promotions SET
  //         title = '$title',
  //         content = '$content',".$imgUp."
  //         img_web = '$img_web',
  //         start_date = '$start_date',
  //         end_date = '$end_date',
  //         price = '$price',
  //         is_active = '$is_active'
  //         WHERE id = '$pid'";


}
else if($action == 'DEL')
{
  $sql   = "UPDATE data_mas_package SET package_status = 'D' WHERE package_id = '$package_id'";
}

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
//echo intval($errorInfo[0]);

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
