<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$branchCode = $_SESSION['branchCode'];

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}



?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
  vertical-align: middle;

}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:80px;vertical-align: middle;">No</th>
      <th style="vertical-align: middle;">พนักงาน</th>
      <th style="width:130px;vertical-align: middle;">ยอดขาย</th>
      <th style="width:80px;vertical-align: middle;">คอมขาย %</th>
      <th style="width:80px;vertical-align: middle;">ค่าคอมขาย</th>
      <th style="width:100px;vertical-align: middle;">จำนวนครั้งเทรน เทรน/รวม</th>
      <th style="width:120px;vertical-align: middle;">ค่าเทรน</th>
      <th style="width:85px;vertical-align: middle;">คอมเทรน %</th>
      <th style="width:100px;vertical-align: middle;">ค่าคอมเทรน</th>
      <th style="width:80px;vertical-align: middle;">ค่าหัว</td>
        <th style="width:130px;vertical-align: middle;">ยอดเงินรวมค่าคอม</td>
    </tr>
  </thead>
  <tbody>
<?php
    $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
    FROM data_mas_employee e
    WHERE EMP_IS_TRAINER = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
    //echo $sqls;
    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    $x = 0;
    $sumSaleCom     = 0;
    $sumSaleTrainer = 0;
    $sumSaleFree    = 0;
    $sumNet         = 0;

    for($i=0 ; $i < $dataCount ; $i++) {

      $EMP_CODE = $rows[$i]['EMP_CODE'];

      $sql ="SELECT sale_code,SUM(net_total) as total FROM trans_package_person
             where sale_code = '$EMP_CODE' and  status not in ('D') and pakage_transfer_id = 0 and create_date between '$date_start' and '$date_end' and company_code = '$branchCode'
             GROUP BY sale_code";

      $query      = DbQuery($sql,null);
      $js         = json_decode($query, true);
      $row        = $js['data'];
      $total      = isset($row[0]['total'])?$row[0]['total']:0;

      if($total > 0){
        $total = ($total/1.07); //ถอด vat
      }

      $sqlp ="SELECT * FROM tb_com_trainer_sale WHERE $total BETWEEN start AND end and COMPANY_CODE = '$branchCode'";
      $queryp     = DbQuery($sqlp,null);
      $jsp        = json_decode($queryp, true);
      $rowp       = $jsp['data'];
      $percent    = isset($rowp[0]['percent'])?$rowp[0]['percent']:0;

      $saleCom    = ($total*$percent)/100;

      $sqlc       ="SELECT *
                    FROM trans_checkin_person
                    where  sign_person = 'Y' and  sign_emp = 'Y' and  sign_manager = 'Y'
                    and staus_checkin = 'CO' and sign_manager_date BETWEEN '$date_start' AND  '$date_end'
                    and trainer_code = '$EMP_CODE' and company_code = '$branchCode' ";

      $queryC     = DbQuery($sqlc,null);
      $jsC        = json_decode($queryC, true);
      $dataCountC = $jsC['dataCount'];
      $rowC       = $jsC['data'];

      $numUse      = 0;
      $numUseTotal = 0;
      $numUseFree  = 0;
      $numTrain    = 0;


      for($x=0 ; $x < $dataCountC ; $x++)
      {
          $price_per_use = $rowC[$x]['price_per_use'];
          if($price_per_use > 0){
            $numUse++;
            $numTrain += $price_per_use;
          }else{
            $numUseFree++;
          }
          $numUseTotal++;
      }
      if($numTrain > 0){
        $numTrain = ($numTrain/1.07); //ถอด vat
      }

      $sqlct      ="SELECT * FROM tb_com_trainer WHERE $numUse BETWEEN start AND end and COMPANY_CODE = '$branchCode'";
      $queryct    = DbQuery($sqlct,null);
      $jsct        = json_decode($queryct, true);
      $rowct      = $jsct['data'];
      $percentct  = isset($rowct[0]['percent'])?$rowct[0]['percent']:0;

      $saleTrainer  = ($numTrain*$percentct)/100;

      $saleFree     = ($numUseFree * 100);
      $net          = $saleCom + $saleTrainer + $saleFree;

      $sumSaleCom  += $saleCom;
      $sumSaleTrainer += $saleTrainer;
      $sumSaleFree += $saleFree;
      $sumNet       += $net;

?>
  <tr class="text-center">
    <td align="center"><?=$i+1 ?></td>
    <td align="left"><?= $rows[$i]['EMP_NAME']." ($EMP_CODE)" ?></td>
    <td align="right"><?= number_format($total,2);?></td>
    <td align="right"><?= $percent ;?></td>
    <td align="right"><?= number_format($saleCom,2);?></td>
    <td align="right"><?= $numUse."/".$numUseTotal;?></td>
    <td align="right"><?= number_format($numTrain,2);?></td>
    <td align="right"><?= $percentct ;?></td>
    <td align="right"><?= number_format($saleTrainer,2);?></td>
    <td align="right"><?= number_format($saleFree,2);?></td>
    <td align="right"><?= number_format($net,2);?></td>
  </tr>
<?php
}
if($dataCount > 0){
?>
  <tr class="text-center">
    <td colspan="4" align="right"><b>รวม</b></td>
    <td align="right"><b><?= number_format($sumSaleCom,2);?></b></td>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right"><b><?= number_format($sumSaleTrainer,2);?></b></td>
    <td align="right"><b><?= number_format($sumSaleFree,2);?></b></td>
    <td align="right"><b><?= number_format($sumNet,2);?></b></td>
  </tr>
<?php
}
?>

  </tbody>
</table>
<div align="right">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });

   $('.group',this).hide();
  })

  function changetr(obj,trId)
  {
    if($(obj).hasClass("fa-plus-square")){
      $('.'+ trId).show();
      $(obj).addClass("fa-minus-square");
      $(obj).removeClass("fa-plus-square");
    }else{
      $('.'+ trId).hide();
      $(obj).addClass("fa-plus-square");
      $(obj).removeClass("fa-minus-square");
    }
  }

</script>
