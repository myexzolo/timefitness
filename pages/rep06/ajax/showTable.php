<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$branchCode = $_SESSION['branchCode'];

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}



?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
  vertical-align: middle;

}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px;vertical-align: middle;">No</th>
      <th style="width:100px;vertical-align: middle;">วันที่</th>
      <th style="vertical-align: middle;">รายการ Package</th>
      <th style="width:80px;vertical-align: middle;">รหัสลูกค้า</th>
      <th style="width:200px;vertical-align: middle;">ชื่อ - สกุล ลูกค้า</th>
      <th style="width:80px;vertical-align: middle;">ชื่อเล่น</th>
      <th style="width:100px;vertical-align: middle;">ราคาขาย<br>(New)</th>
      <th style="width:80px;vertical-align: middle;">ภาษีมูลเพิ่ม<br>(New)</th>
      <th style="width:100px;vertical-align: middle;">ราคาก่อนภาษี<br>(New)</th>
      <th style="width:100px;vertical-align: middle;">ราคาขาย<br>(Renew)</th>
      <th style="width:80px;vertical-align: middle;">ภาษีมูลเพิ่ม<br>(Renew)</th>
      <th style="width:110px;vertical-align: middle;">ราคาก่อนภาษี<br>(Renew)</th>
    </tr>
  </thead>
  <tbody>
<?php
    $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
    FROM data_mas_employee e
    WHERE EMP_IS_SALE = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
    //echo $sql;
    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];


    $sumSale = 0;
    $sumvat   = 0;
    $sumbuy   = 0;

    $sumSaleR = 0;
    $sumvatR   = 0;
    $sumbuyR   = 0;

    $sumSales  = 0;
    $sumvats   = 0;
    $sumbuys   = 0;

    $sumSalesR  = 0;
    $sumvatsR   = 0;
    $sumbuysR   = 0;

    for($i=0 ; $i < $dataCount ; $i++) {
        $EMP_CODE = $rows[$i]['EMP_CODE'];
        $EMP_NAME = $rows[$i]['EMP_NAME'];


        if($i > 0)
        {
    ?>
    <tr>
      <td colspan="6" align="right"><b>รวม</b></td>
      <td align="right"><b><?= number_format($sumSale,2) ?></b></td>
      <td align="right"><b><?= number_format($sumvat,2) ?></b></td>
      <td align="right"><b><?= number_format($sumbuy,2) ?></b></td>
      <td align="right"><b><?= number_format($sumSaleR,2) ?></b></td>
      <td align="right"><b><?= number_format($sumvatR,2) ?></b></td>
      <td align="right"><b><?= number_format($sumbuyR,2) ?></b></td>
    </tr>
    <?php
          $sumSale  = 0;
          $sumvat   = 0;
          $sumbuy   = 0;

          $sumSaleR  = 0;
          $sumvatR   = 0;
          $sumbuyR   = 0;

        }
    ?>
    <tr class="text-center  group" >
      <td align="center"><i class="fa fa-minus-square" style="font-size:18px;" onclick="changetr(this,'tr_<?=$i;?>')"></i></td>
      <td colspan="12" align="left"><?=$EMP_NAME." (".$EMP_CODE.")" ?></td>
    </tr>
    <?php

      $sql ="SELECT pp.*,p.PERSON_NICKNAME, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME
             FROM trans_package_person pp, person p
             where p.PERSON_CODE = pp.person_code and pp.sale_code = '$EMP_CODE' and  pp.status not in ('D') and pp.pakage_transfer_id = 0
             and pp.create_date between '$date_start' and '$date_end' and pp.company_code = '$branchCode' order by pp.create_date";
      //echo $sql."<br>";
      $query      = DbQuery($sql,null);
      $js         = json_decode($query, true);
      $row        = $js['data'];
      $dataCounts = $js['dataCount'];

      for($x=0 ; $x < $dataCounts ; $x++)
      {
        $net_total  = $row[$x]['net_total'];
        $type_buy   = $row[$x]['type_buy'];

        $total  = 0;
        $vat    = 0;

        $net_totalR  = 0;
        $totalR  = 0;
        $vatR    = 0;

        if($type_buy == 'N')
        {
          if($net_total > 0)
          {
            $total = ($net_total/1.07); //ถอด vat
            $vat   = $net_total - $total;
          }

          $sumSale  += $net_total;
          $sumvat   += $vat;
          $sumbuy   += $total;

          $sumSales  += $net_total;
          $sumvats   += $vat;
          $sumbuys   += $total;
        }
        else if($type_buy == 'R')
        {
          $net_totalR = $net_total;
          if($net_totalR > 0)
          {
            $totalR = ($net_totalR/1.07); //ถอด vat
            $vatR   = $net_totalR - $totalR;
          }

          $sumSaleR  += $net_totalR;
          $sumvatR   += $vatR;
          $sumbuyR   += $totalR;

          $sumSalesR  += $net_totalR;
          $sumvatsR   += $vatR;
          $sumbuysR   += $totalR;
        }
?>
      <tr class="tr_<?=$i;?>">
        <td align="center"><?=$x+1 ?></td>
        <td align="center"><?= DateThai($row[$x]['create_date']); ;?></td>
        <td align="left"><?= $row[$x]['package_name'] ?></td>
        <td align="left"><?= $row[$x]['person_code'] ?></td>
        <td align="left"><?= $row[$x]['PERSON_NAME'] ?></td>
        <td align="left"><?= $row[$x]['PERSON_NICKNAME'] ;?></td>
        <td align="right"><?= number_format($net_total,2);?></td>
        <td align="right"><?= number_format($vat,2);?></td>
        <td align="right"><?= number_format($total,2);?></td>
        <td align="right"><?= number_format($net_totalR,2);?></td>
        <td align="right"><?= number_format($vatR,2);?></td>
        <td align="right"><?= number_format($totalR,2);?></td>
      </tr>
<?php
      }
      if($dataCounts == 0){
?>
<!-- <tr class="text-center">
  <td colspan="7" align="center">&nbsp;</td>
</tr> -->
<?php
      }
}
if($dataCount > 0){
?>
<tr>
  <td colspan="6" align="right"><b>รวม</b></td>
  <td align="right"><b><?= number_format($sumSale,2) ?></b></td>
  <td align="right"><b><?= number_format($sumvat,2) ?></b></td>
  <td align="right"><b><?= number_format($sumbuy,2) ?></b></td>
  <td align="right"><b><?= number_format($sumSaleR,2) ?></b></td>
  <td align="right"><b><?= number_format($sumvatR,2) ?></b></td>
  <td align="right"><b><?= number_format($sumbuyR,2) ?></b></td>
</tr>
<?php
}
?>
<tr class="group">
  <td colspan="6" align="right"><b>รวมทั้งหมด</b></td>
  <td align="right"><b><?= number_format($sumSales,2) ?></b></td>
  <td align="right"><b><?= number_format($sumvats,2) ?></b></td>
  <td align="right"><b><?= number_format($sumbuys,2) ?></b></td>
  <td align="right"><b><?= number_format($sumSalesR,2) ?></b></td>
  <td align="right"><b><?= number_format($sumvatsR,2) ?></b></td>
  <td align="right"><b><?= number_format($sumbuysR,2) ?></b></td>
</tr>
  </tbody>
</table>
<div class="pull-right" align="right" style="width:50%;margin-bottom:6px;">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });

   $('.group',this).hide();
  })

  function changetr(obj,trId)
  {
    if($(obj).hasClass("fa-plus-square")){
      $('.'+ trId).show();
      $(obj).addClass("fa-minus-square");
      $(obj).removeClass("fa-plus-square");
    }else{
      $('.'+ trId).hide();
      $(obj).addClass("fa-plus-square");
      $(obj).removeClass("fa-minus-square");
    }
  }

</script>
