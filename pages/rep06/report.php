<!DOCTYPE html>
<?php
if(!isset($_SESSION))
{
    session_start();
}
include('../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$date_start = $_POST['date_start']." 00:00:01";
$date_end   = $_POST['date_end']." 23:59:59";
$empCode    = $_POST['empCode'];
$empName    = $_POST['empName'];
$repName    = $_POST['repName'];
$branchCode = $_SESSION['branchCode'];

// $date_start = '2019/08/01';
// $date_end   = '2019/08/31';
// $branchCode = $_SESSION['branchCode'];
// $empCode    = "";

if($empCode != "")
{
  $con = " and e.EMP_CODE = '$empCode' ";
}

?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GYM Monkey | Report</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <?php
    include("../../inc/css-header.php");
    include("../../inc/js-footer.php");
    $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
  ?>
  <link rel="stylesheet" href="css/rep01.css">
  <link rel="stylesheet" href="../../dist/css/paper.css">
</head>
<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<style>

body {
  color: #fff;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    border: 0px solid #fff;
    font-size: 12px;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
  border-top: 0px solid #fff;
}

@page {
  margin: 1cm;
  size: landscape;
}

@page :first {
  margin-top: 1cm; /* Top margin on first page 10cm */
  size: landscape;
}

@media print {
  body
  {
    margin: 0;
    border: solid 0px #fff;
  }
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
      border: 1px solid #000;
  }
  .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 1px solid #000;
  }

  th
  {
    text-align: center;
    background-color: #ccc !important;
  }
}


th {
  text-align: center;
}

</style>
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="padding-10mm">

    <!-- Write HTML just like a web page -->
    <article>
      <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">
        <?=$repName ?>
      </div>
      <div align="center" style="font-weight:bold;font-size:15px;padding:5px;">
        <?=$empName ?>
      </div>
      <br>
      <table class="table"  id="tableDisplay" style="width:100%" >
        <thead>
          <tr class="text-center">
            <th style="width:36px;vertical-align: middle;">No</th>
            <th style="width:80px;vertical-align: middle;">วันที่</th>
            <th style="vertical-align: middle;">รายการ Package</th>
            <th style="width:170px;vertical-align: middle;">ชื่อ - สกุล ลูกค้า</th>
            <th style="width:75px;vertical-align: middle;">ชื่อเล่น</th>
            <th style="width:75px;vertical-align: middle;">ราคาขาย<br>(New)</th>
            <th style="width:75px;vertical-align: middle;">ภาษีมูลเพิ่ม<br>(New)</th>
            <th style="width:75px;vertical-align: middle;">ราคาก่อนภาษี<br>(New)</th>
            <th style="width:75px;vertical-align: middle;">ราคาขาย<br>(Renew)</th>
            <th style="width:75px;vertical-align: middle;">ภาษีมูลเพิ่ม<br>(Renew)</th>
            <th style="width:75px;vertical-align: middle;">ราคาก่อนภาษี<br>(Renew)</th>
          </tr>
        </thead>
        <tbody>
          <?php
              $sql ="SELECT EMP_CODE,EMP_NICKNAME,CONCAT(EMP_TITLE, EMP_NAME, ' ', EMP_LASTNAME) as EMP_NAME
              FROM data_mas_employee e
              WHERE EMP_IS_SALE = 'Y' and e.DATA_DELETE_STATUS != 'Y' and COMPANY_CODE ='$branchCode' $con ORDER BY EMP_CODE";
              //echo $sql;
              $querys     = DbQuery($sql,null);
              $json       = json_decode($querys, true);
              $errorInfo  = $json['errorInfo'];
              $dataCount  = $json['dataCount'];
              $rows       = $json['data'];


              $sumSale = 0;
              $sumvat   = 0;
              $sumbuy   = 0;

              $sumSaleR = 0;
              $sumvatR   = 0;
              $sumbuyR   = 0;

              $sumSales  = 0;
              $sumvats   = 0;
              $sumbuys   = 0;

              $sumSalesR  = 0;
              $sumvatsR   = 0;
              $sumbuysR   = 0;

              for($i=0 ; $i < $dataCount ; $i++) {
                  $EMP_CODE = $rows[$i]['EMP_CODE'];
                  $EMP_NAME = $rows[$i]['EMP_NAME'];


                  if($i > 0)
                  {
              ?>
              <tr>
                <td colspan="5" align="right"><b>รวม</b></td>
                <td align="right"><b><?= number_format($sumSale,2) ?></b></td>
                <td align="right"><b><?= number_format($sumvat,2) ?></b></td>
                <td align="right"><b><?= number_format($sumbuy,2) ?></b></td>
                <td align="right"><b><?= number_format($sumSaleR,2) ?></b></td>
                <td align="right"><b><?= number_format($sumvatR,2) ?></b></td>
                <td align="right"><b><?= number_format($sumbuyR,2) ?></b></td>
              </tr>
              <!-- <tr>
                <td colspan="11" style="border-left:0px;border-right:0px;"></td>
              </tr> -->
              <?php
                    $sumSale  = 0;
                    $sumvat   = 0;
                    $sumbuy   = 0;

                    $sumSaleR  = 0;
                    $sumvatR   = 0;
                    $sumbuyR   = 0;

                  }
              ?>
              <tr class="text-center  group" >
                <td colspan="12" align="left"><b><?=$EMP_NAME." (".$EMP_CODE.")" ?></b></td>
              </tr>
              <?php

                $sql ="SELECT pp.*,p.PERSON_NICKNAME, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as PERSON_NAME
                       FROM trans_package_person pp, person p
                       where p.PERSON_CODE = pp.person_code and pp.sale_code = '$EMP_CODE' and  pp.status not in ('D') and pp.pakage_transfer_id = 0
                       and pp.create_date between '$date_start' and '$date_end' and pp.company_code = '$branchCode' order by pp.create_date";
                //echo $sql."<br>";
                $query      = DbQuery($sql,null);
                $js         = json_decode($query, true);
                $row        = $js['data'];
                $dataCounts = $js['dataCount'];

                for($x=0 ; $x < $dataCounts ; $x++)
                {
                  $net_total  = $row[$x]['net_total'];
                  $type_buy   = $row[$x]['type_buy'];

                  $total  = 0;
                  $vat    = 0;

                  $net_totalR  = 0;
                  $totalR  = 0;
                  $vatR    = 0;

                  if($type_buy == 'N')
                  {
                    if($net_total > 0)
                    {
                      $total = ($net_total/1.07); //ถอด vat
                      $vat   = $net_total - $total;
                    }

                    $sumSale  += $net_total;
                    $sumvat   += $vat;
                    $sumbuy   += $total;

                    $sumSales  += $net_total;
                    $sumvats   += $vat;
                    $sumbuys   += $total;
                  }
                  else if($type_buy == 'R')
                  {
                    $net_totalR = $net_total;
                    if($net_totalR > 0)
                    {
                      $totalR = ($net_totalR/1.07); //ถอด vat
                      $vatR   = $net_totalR - $totalR;
                    }

                    $sumSaleR  += $net_totalR;
                    $sumvatR   += $vatR;
                    $sumbuyR   += $totalR;

                    $sumSalesR  += $net_totalR;
                    $sumvatsR   += $vatR;
                    $sumbuysR   += $totalR;
                  }
          ?>
                <tr class="tr_<?=$i;?>">
                  <td align="center"><?=$x+1 ?></td>
                  <td align="center"><?= DateThai($row[$x]['create_date']); ;?></td>
                  <td align="left"><?= $row[$x]['package_name'] ?></td>
                  <td align="left"><?= $row[$x]['PERSON_NAME'] ?></td>
                  <td align="left"><?= $row[$x]['PERSON_NICKNAME'] ;?></td>
                  <td align="right"><?= number_format($net_total,2);?></td>
                  <td align="right"><?= number_format($vat,2);?></td>
                  <td align="right"><?= number_format($total,2);?></td>
                  <td align="right"><?= number_format($net_totalR,2);?></td>
                  <td align="right"><?= number_format($vatR,2);?></td>
                  <td align="right"><?= number_format($totalR,2);?></td>
                </tr>
          <?php
                }
                if($dataCounts == 0){
          ?>
          <!-- <tr class="text-center">
            <td colspan="7" align="center">&nbsp;</td>
          </tr> -->
          <?php
                }
          }
          if($dataCount > 0){
          ?>
          <tr>
            <td colspan="5" align="right"><b>รวม</b></td>
            <td align="right"><b><?= number_format($sumSale,2) ?></b></td>
            <td align="right"><b><?= number_format($sumvat,2) ?></b></td>
            <td align="right"><b><?= number_format($sumbuy,2) ?></b></td>
            <td align="right"><b><?= number_format($sumSaleR,2) ?></b></td>
            <td align="right"><b><?= number_format($sumvatR,2) ?></b></td>
            <td align="right"><b><?= number_format($sumbuyR,2) ?></b></td>
          </tr>
          <?php
          }
          ?>
          <tr class="group">
            <td colspan="5" align="right"><b>รวมทั้งหมด</b></td>
            <td align="right"><b><?= number_format($sumSales,2) ?></b></td>
            <td align="right"><b><?= number_format($sumvats,2) ?></b></td>
            <td align="right"><b><?= number_format($sumbuys,2) ?></b></td>
            <td align="right"><b><?= number_format($sumSalesR,2) ?></b></td>
            <td align="right"><b><?= number_format($sumvatsR,2) ?></b></td>
            <td align="right"><b><?= number_format($sumbuysR,2) ?></b></td>
          </tr>
        </tbody>
      </table>
    </article>
  </section>
</body>
</html>
<script>
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 1000);
  });
</script>
