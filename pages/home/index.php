<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="google-site-verification" content="HL-4q2f8iiaLDlSvEOkSSsQuQxuPpDzXxHcOvdGBc3c" />
      <title>Time Fitness | Dashboard</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Dashboard
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php
            //include("../../inc/boxes.php");

            //$d = strtotime("today");

            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12 classShow">
                <div class="box box-warning">
                  <div class="box-header with-border" >
                    <h3 class="box-title">Class Schedule</h3>
                    <div class="pull-right">
                    <button onclick="getclassWeek(1)" class="btn btn-success btn-flat pull-right"><i class="fa fa-chevron-right"></i></button>
                    <button onclick="getclassWeek(0)" class="btn btn-success btn-flat pull-right">Today</button>
                    <button onclick="getclassWeek(-1)" class="btn btn-success btn-flat pull-right"><i class="fa fa-chevron-left"></i></button>
                    <input type="hidden" id="weekofDay" value="0">
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showClass"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 classShowMini">
                <?php

                $branchcode = $_SESSION['branchCode'];

                date_default_timezone_set("Asia/Bangkok");

                $date = date("Y-m-d");
                $endDate = strtotime($date . "+7 days");

                $date_start  = $date;
                $date_end    = date("Y-m-d",$endDate);

                  $sqlG = "SELECT sc.date_class FROM tb_schedule_class_day sc, tb_schedule_class s
                  WHERE sc.date_class between '$date_start' and '$date_end'
                  and sc.schedule_id = s.schedule_id and s.branch_code = '$branchcode'
                  group by sc.date_class
                  order by sc.date_class";

                  echo $sqlG;

                  $queryG      = DbQuery($sqlG,null);
                  $jsonG       = json_decode($queryG, true);
                  $rowG        = $jsonG['data'];
                  $dataCountG  = $jsonG['dataCount'];


                  for($i=0;$i<$dataCountG; $i++)
                  {
                    $date    = $rowG[$i]['date_class'];
                    $dateStr = date('D d/m/Y',strtotime($date));
                    $dayOfClass = $dateStr;
                  ?>
                  <table class="tableClass">
                    <thead>
                      <tr>
                        <td colspan="4" class="text-center" style="background-color: #3a4342;"><?= $dayOfClass ?></td>
                      </tr>
                    </thead>
                    <tbody>
                  <?php
                    $sql = "SELECT scd.*,c.name_class,e.EMP_NICKNAME
                    FROM tb_schedule_class_day scd, t_classes c, data_mas_employee e, tb_schedule_class s
                    WHERE scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE  and scd.schedule_id = s.schedule_id
                    and s.branch_code = '$branchcode'
                    and scd.date_class = '$date' order by scd.date_class, scd.day, scd.row ";

                    $query      = DbQuery($sql,null);
                    $json       = json_decode($query, true);
                    $row        = $json['data'];
                    $dataCount  = $json['dataCount'];


                    for($j=0;$j<$dataCount; $j++)
                    {
                        $timeOfClass  = $row[$j]['time_start'];
                        $className    = $row[$j]['name_class'];
                        $trainerName  = $row[$j]['EMP_NICKNAME'];
                        $personJoin   = $row[$j]['person_join'];
                        $personTotal  = $row[$j]['unit'];
                ?>
                <tr>
                  <td class="text-center" width="20%"><?= $timeOfClass ?></td>
                  <td width="55%"><?= $className ?><br><span><?= $trainerName ?></span></td>
                  <td text-center width="20%" class="icon-user">
                    <i class="fa fa-users"></i>&nbsp;<?= $personJoin."/".$personTotal ?>
                  </td>
                </tr>
                <?php } ?>
                  </tbody>
                </table>
              <?php }?>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script>
      showClass(0);
      function getclassWeek(week)
      {
        var num =  parseInt($("#weekofDay").val());
        if(week != 0){
          num = (num + week);
        }
        $("#weekofDay").val(num);
        showClass(num);
      }
      </script>
    </body>
  </html>
