<!DOCTYPE html>
<?php
if(!isset($_SESSION))
{
    session_start();
}
include('../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$date_start = $_POST['date_start'];
$date_end   = $_POST['date_end'];
$repName    = $_POST['repName'];
$branchCode = $_SESSION['branchCode'];

// $date_start = '2019/08/01';
// $date_end   = '2019/08/31';
// $branchCode = $_SESSION['branchCode'];
// $empCode    = "";

?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GYM Monkey | Report</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <?php
    include("../../inc/css-header.php");
    include("../../inc/js-footer.php");
    $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
  ?>
  <link rel="stylesheet" href="css/rep08.css">
  <link rel="stylesheet" href="../../dist/css/paper.css">
</head>
<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<style>

body {
  color: #fff;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    border: 0px solid #fff;
    font-size: 12px;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
  border-top: 0px solid #fff;
}

@page {
  margin: 1cm;
  size: portrait;
}

@page :first {
  margin-top: 1cm; /* Top margin on first page 10cm */
  size: portrait;
}

@media print {
  body
  {
    margin: 0;
    border: solid 0px #fff;
  }
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
      border: 1px solid #000;
  }
  .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 1px solid #000;
  }
}

th {
  text-align: center;
}

</style>
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="padding-10mm">

    <!-- Write HTML just like a web page -->
    <article>
      <div align="center" style="font-weight:bold;font-size:16px;padding:5px;">
        <?=$repName ?>
      </div>
      <br>
      <table class="table"  id="tableDisplay" style="width:100%" >
        <thead>
          <tr>
            <th style="width:50px">No</th>
            <th >วันที่</th>
            <th >ชื่อ - สกุล</th>
            <th >ชื่อเล่น</th>
            <th >วันเดือนปีเกิด</th>
            <th >เบอร์โทร</th>
            <th >วันหมดอายุ</th>
            <th >เวลาเข้า</th>
            <th >เวลาออก</th>
          </tr>
        </thead>
        <tbody>
    <?php
    $sql = "SELECT tc.*,p.person_name,p.person_lastname,p.person_nickname,
            p.person_expire_date ,p.person_birth_date, p.person_tel_mobile
            FROM tb_checkin tc, person p
            WHERE tc.person_code = p.person_code and p.company_code = '$branchCode'
            and tc.date_checkin between '$date_start' and '$date_end' ";

    //echo $sql;

    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    $x = 0;
    for($i=0 ; $i < $dataCount ; $i++) {
      $person_name        = $rows[0]['person_name'];
      $person_lastname    = $rows[0]['person_lastname'];
      $person_nickname    = $rows[0]['person_nickname'];
      $person_tel_mobile  = $rows[0]['person_tel_mobile'];
      $person_expire_date = DateThai($rows[0]['person_expire_date']);
      $person_birth_date  = DateThai($rows[0]['person_birth_date']);
      $date_checkin       = DateThai($rows[0]['date_checkin']);
      $time_checkin       = $rows[0]['time_checkin'];
      $time_checkout      = $rows[0]['time_checkout'];

      ?>
      <tr class="tr_<?=$x;?>">
        <td align="center"><?=$i+1 ;?></td>
        <td align="center"><?=$date_checkin;?></td>
        <td align="left"><?=$person_name." ".$person_lastname ;?></td>
        <td align="left"><?=$person_nickname;?></td>
        <td align="center"><?=$person_birth_date; ?></td>
        <td align="center"><?=$person_tel_mobile; ?></td>
        <td align="center"><?=$person_expire_date;?></td>
        <td align="center"><?=$time_checkin;?></td>
        <td align="center"><?=$time_checkout;?></td>
      </tr>
      <?php
        }
       ?>
        </tbody>
      </table>
    </article>
  </section>
</body>
</html>
<script>
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 1000);
  });
</script>
