$(function () {
    $('.select2').select2();
    showTable();

    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50,100,150],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   });




   $('#daterage').daterangepicker(
     {
       locale: {
         format: 'DD/MM/YYYY',
         daysOfWeek: [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        monthNames: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
       }
     }
   );
  })

  function reset(date)
  {
    $('#daterage').val(date);
    $('#empCode').val("");
    $('#empCode').trigger('change');
    showTable();
  }

  function print()
  {
    var daterage = $('#daterage').val();
    var res = daterage.split("-");
    var date_start = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
    var date_end = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");

    var repName = "รายงานประวัติการเข้า-ออก Member ตั้งแต่วันที่ " +res[0]+ " ถึง "+res[1];

    var url = "report.php?date_start="+date_start+"&date_end="+date_end+"&repName="+repName;
    console.log(url);
    postUrlBlank(url, null);
  }


  function showTable(){
    var daterage = $('#daterage').val();
    var res = daterage.split("-");
    $('#startDate').html(res[0]);
    $('#endDate').html(res[1]);

    var date_start = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
    var date_end = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");

    $.post("ajax/showTable.php",{date_start:date_start,date_end:date_end})
    .done(function( data ) {
      $("#showTable").html( data );
    });
  }
