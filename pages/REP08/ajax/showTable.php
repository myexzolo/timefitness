<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$con = "";

$date_start = $_POST['date_start'];
$date_end   = $_POST['date_end'];
$branchCode = $_SESSION['branchCode'];


?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

.alert {
  color: red;
  font-weight: bold;
}
</style>
<table class="table table-bordered table-hover" id="tableDisplay" style="width:100%">
  <thead>
    <tr>
      <th style="width:50px">No</th>
      <th >วันที่</th>
      <th >ชื่อ - สกุล</th>
      <th >ชื่อเล่น</th>
      <th >วันเดือนปีเกิด</th>
      <th >เบอร์โทร</th>
      <th >วันหมดอายุ</th>
      <th >เวลาเข้า</th>
      <th >เวลาออก</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql = "SELECT tc.*,p.person_name,p.person_lastname,p.person_nickname,
      p.person_expire_date ,p.person_birth_date, p.person_tel_mobile
      FROM tb_checkin tc, person p
      WHERE tc.person_code = p.person_code and p.company_code = '$branchCode'
      and tc.date_checkin between '$date_start' and '$date_end' ";
      //echo $sql;
      $querys     = DbQuery($sql,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      $x = 0;
      for($i=0 ; $i < $dataCount ; $i++) {
        $person_name        = $rows[$i]['person_name'];
        $person_lastname    = $rows[$i]['person_lastname'];
        $person_nickname    = $rows[$i]['person_nickname'];
        $person_tel_mobile  = $rows[$i]['person_tel_mobile'];
        $person_expire_date = DateThai($rows[$i]['person_expire_date']);
        $person_birth_date  = DateThai($rows[$i]['person_birth_date']);
        $date_checkin       = DateThai($rows[$i]['date_checkin']);
        $time_checkin       = $rows[$i]['time_checkin'];
        $time_checkout      = $rows[$i]['time_checkout'];

    ?>
    <tr class="tr_<?=$x;?>">
      <td align="center"><?=$i+1 ;?></td>
      <td align="center"><?=$date_checkin;?></td>
      <td align="left"><?=$person_name." ".$person_lastname ;?></td>
      <td align="left"><?=$person_nickname;?></td>
      <td align="center"><?=$person_birth_date; ?></td>
      <td align="center"><?=$person_tel_mobile; ?></td>
      <td align="center"><?=$person_expire_date;?></td>
      <td align="center"><?=$time_checkin;?></td>
      <td align="center"><?=$time_checkout;?></td>
    </tr>
    <?php
      }
     ?>
  </tbody>
</table>
<div align="right">
  <button onclick="print()" class="btn btn-primary btn-flat" style="width:150px"><i class="fa fa-print"></i> พิมพ์</button>
  <button onclick="" class="btn btn-info btn-flat" style="width:150px;display:none"><i class="fa fa-print"></i>  พิมพ์แยกรายคน</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthChange': true,
     'searching'   : false,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   });
  })
</script>
