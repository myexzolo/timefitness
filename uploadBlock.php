<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Kosummarket | Upload Block</title>
      <link rel="shortcut icon" type="image/png" href="image/favicon.png"/>
      <link rel="stylesheet" href="css/management_block.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <div class="content-wrapper">

          <section class="content">
            <form action="upload/uploadBlock.php" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box boxBlack">
                          <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <div class="form-group col-md-12">
                                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                                    <div class="col-sm-8">
                                      <input type="file" name="filepath" accept=".xlsx,.xls,.txt" required>
                                    </div>
                                  </div>
                                  <div class="text-center col-md-12">
                                    <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                                  </div>
                              </div>
                            </div>
                          </div>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
              </div>
            </form>
          </section>
        </div>
      </div>
    </body>
  </html>
