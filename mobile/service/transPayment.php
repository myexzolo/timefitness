<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$transaction_id     = isset($_GET['transaction_id'])?$_GET['transaction_id']:"";
$payment_type       = isset($_GET['payment_type'])?$_GET['payment_type']:"";
$person_code        = isset($_GET['person_code'])?$_GET['person_code']:"";
$package_id         = isset($_GET['package_id'])?$_GET['package_id']:"";
$amount             = isset($_GET['amount'])?$_GET['amount']:"";
$status_payment     = isset($_GET['status_payment'])?$_GET['status_payment']:"";
$description        = isset($_GET['description'])?$_GET['description']:"";

$type_active        = isset($_GET['type_active'])?$_GET['type_active']:"";

$company_code     = "GYMMK02";
$create_by        = "2001";


$status  = true;

if($type_active == "ADD")
{
  $sql   = "INSERT INTO  trans_payment (
            transaction_id,payment_type,person_code,
            package_id,status_payment,
            amount,description
            )
            VALUES (
            '$transaction_id','$payment_type','$person_code',
            '$package_id','$status_payment',
            '$amount','$description');";


}
else if($type_active == "PAYSUCCESS")
{
  $typeVat   = "INV";
  $companyId = sprintf("%02d", 2);
  $code = $typeVat.$companyId.date("ym");

  $sqlNo      = "SELECT max(invoice_code) as invoice_code FROM tb_invoice where company_code ='$company_code' and invoice_code like '$code%' ";
  $queryNo    = DbQuery($sqlNo,null);
  $json       = json_decode($queryNo, true);
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];

  $invoice_code = "";
  if($dataCount > 0){
    $no       = $row[0]['invoice_code'];
    $lastNum  = substr($no,strlen($code));
    $lastNum  = $lastNum + 1;
    $invoice_code = $code.sprintf("%03d", $lastNum);
  }else{
    $invoice_code = $code."001";
  }

  $sqlDetailPay = "SELECT tp.*,p.PERSON_TEL_MOBILE,p.PERSON_TITLE,p.PERSON_NAME,p.PERSON_EXPIRE_DATE,
                   pg.package_name, pg.package_unit, pg.package_code, pg.package_detail, pg.num_use, pg.max_use,
                   pg.package_type, pg.notify_num, pg.notify_unit
                   FROM trans_payment tp, person p, data_mas_package pg
                   WHERE transaction_id ='$transaction_id' and p.PERSON_CODE = tp.person_code and pg.package_id = tp.package_id";

  $queryDP      = DbQuery($sqlDetailPay,null);
  $jsonDP       = json_decode($queryDP, true);
  $rowDP        = $jsonDP['data'];

  $person_code        = $rowDP[0]['person_code'];
  $package_id         = $rowDP[0]['package_id'];

  $PERSON_TEL_MOBILE  = $rowDP[0]['PERSON_TEL_MOBILE'];
  $name               = $rowDP[0]['PERSON_TITLE'].$rowDP[0]['PERSON_NAME']." ".$rowDP[0]['PERSON_LASTNAME'];
  $address            = $rowDP[0]['PERSON_HOME1_ADDR1'];
  $PERSON_EXPIRE_DATE = $rowDP[0]['PERSON_EXPIRE_DATE'];

  $package_unit       = $rowDP[0]['package_unit'];
  $package_name       = $rowDP[0]['package_name'];
  $package_type       = $rowDP[0]['package_type'];


  $amount             = $rowDP[0]['amount'];
  $total_price        = ($amount*100)/107;
  $vat                = ($amount - $total_price);

  $name             = $name;
  $address          = $address;
  $tel              = $PERSON_TEL_MOBILE;
  $fax              = "";
  $tax              = "";
  $cash             = "0";
  $transfer         = "0";
  $credit           = $amount;
  $cheque           = "0";
  $type_payment     = $typeVat;
  $total_price      = $total_price;
  $discount         = "0";
  $vat              = $vat;
  $total_net        = $amount;
  $receipt          = "";
  $num_print        = "0";


$sql   .= "INSERT INTO tb_invoice (
          company_code, invoice_code, invoice_date,
          name, address, tel,
          fax, tax, type_payment,
          total_price, discount, vat,
          total_net, create_by, create_date, receipt,status ,cash ,transfer ,credit ,cheque,num_print)
          VALUES (
          '$company_code', '$invoice_code', NOW(),
          '$name', '$address', '$tel',
          '$fax', '$tax','$type_payment',
          '$total_price','$discount', '$vat',
          '$total_net','$create_by', NOW(), '$receipt','A' ,'$cash','$transfer','$credit','$cheque','$num_print');";

  $dateNow = date('Y-m-d');
  //echo "type_package :".$package_type."    ";
  //echo "PERSON_EXPIRE_DATE :".$PERSON_EXPIRE_DATE."    ";
  if($PERSON_EXPIRE_DATE != "" && $package_type == "MB")
  {
    $dateNow = $PERSON_EXPIRE_DATE;
  }
  //echo "dateNow :".$dateNow."    ";

  $package_code       = $rowDP[0]['package_code'];
  $package_name       = $rowDP[0]['package_name'];
  $package_detail     = $rowDP[0]['package_detail'];
  $reg_no             = "";
  $num_use            = $rowDP[0]['num_use'];
  $package_unit       = $rowDP[0]['package_unit'];
  $date_start         = $dateNow;
  $max_use            = $rowDP[0]['max_use'];
  $package_price      = $amount;
  $package_num        = "1";
  $package_price_total = $amount;
  $percent_discount   = "0";
  $discount           = "0";
  $vat                = "0";
  $type_vat           = "";
  $net_total          = $amount;
  $trainer_code       = "";
  $trainer_name       = "";
  $receipt_code       = "";
  $notify_num         = $rowDP[0]['notify_num'];
  $notify_unit        = $rowDP[0]['notify_unit'];
  $type_buy           = "N";
  $sale_code          = "";
  $price_per_use      = "0";

  if($package_unit == 'TIMES' && $net_total > 0){
    $price_per_use = (chkNum($total_net)/chkNum($num_use));
  }else if($package_unit == 'DAYS'){
    $date_expire = strtotime(date("Y-m-d", strtotime($dateNow)) . " +".$num_use." days");
  }else if($package_unit == 'MONTHS'){
    $date_expire = strtotime(date("Y-m-d", strtotime($dateNow)) . " +".$num_use." month");
  }else if($package_unit == 'YEAR'){
    $date_expire = strtotime(date("Y-m-d", strtotime($dateNow)) . " +".$num_use." year");
  }
  $date_expire = date("Y-m-d", $date_expire);
  // echo "date_expire :".$date_expire."    ";
  $sql   .= "INSERT INTO trans_package_person (
    company_code,person_code,package_code,
    package_name,package_detail,reg_no,
    num_use,package_unit, date_start,
    date_expire, max_use,type_package,
    package_price, package_num, package_price_total,
    percent_discount, discount, vat, type_vat,
    net_total, trainer_code, trainer_name,
    invoice_code, receipt_code, create_by,
    notify_num, notify_unit,
    create_date, status,use_package,price_per_use,type_buy,sale_code)
    VALUES (
    '$company_code', '$person_code', '$package_code',
    '$package_name', '$package_detail', '$reg_no',
    '$num_use', '$package_unit', '$date_start',
    '$date_expire', '$max_use', '$package_type',
    '$package_price', '$package_num', '$package_price_total',
    '$percent_discount', '$discount', '$vat', '$type_vat',
    '$net_total', '$trainer_code', '$trainer_name',
    '$invoice_code', '$receipt_code', '$create_by',
    '$notify_num', '$notify_unit',
    NOW(), 'A','$num_use','$price_per_use','$type_buy','$sale_code');";


  if($package_type == "MB")
  {
    $sql .= "UPDATE person SET
            PERSON_STATUS       = 'A',
            PERSON_EXPIRE_DATE  = '$date_expire',
            DATA_MODIFY_BY      = '$create_by'
            WHERE COMPANY_CODE  = '$company_code' and PERSON_CODE = '$person_code';";
  }

  $status_payment = 'S';

  $sql .= "UPDATE trans_payment SET status_payment = '$status_payment' WHERE transaction_id = '$transaction_id';";

}

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];

if(intval($errorInfo[0]) == 0)
{
  $message = "Success";
}else{
  $message = "Fail";
  $status  = false;
}


header('Content-Type: application/json');
exit(json_encode(array('status' => $status,'message' => $message)));

?>
