<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

$schedule_day_id    = isset($_GET['id'])?$_GET['id']:"";
$personCode         = isset($_GET['personCode'])?$_GET['personCode']:"";
$branchCode         = isset($_GET['branchCode'])?$_GET['branchCode']:"";
$user_id_update     = "1";
$status             = "S";
$expire_date        = isset($_GET['expireDate'])?$_GET['expireDate']:"";


$sql = "";

$sql = "SELECT * FROM tb_schedule_class_day WHERE id = '$schedule_day_id'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

$num = 0;
if($dataCount > 0)
{
  $unit         = $rows[0]['unit'];
  $person_join  = $rows[0]['person_join'];
  $num = $unit - ($person_join + 2);

  if($num <= 0)
  {
    $status = 'P';
  }
}

$sql  = "INSERT INTO t_reserve_class (schedule_day_id,PERSON_CODE,date_reserve,status,expire_date,branch_code,create_by,create_date,join_seq,update_by,update_date)
     VALUES('$schedule_day_id','$personCode', NOW(),'$status','$expire_date','$branchCode','$user_id_update',NOW(),0, 1,NOW());";

$sql  .= "UPDATE tb_schedule_class_day SET person_join = person_join + 1
         WHERE id = '$schedule_day_id';";

//echo    " sql : ".$sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){

  // $sql = "SELECT * FROM t_branch WHERE is_active ='Y' and branch_code = '$branchcode'";
  //
  // $query      = DbQuery($sql,null);
  // $json       = json_decode($query, true);
  // $errorInfo  = $json['errorInfo'];
  // $rows       = $json['data'];
  // $dataCount  = $json['dataCount'];
  //
  // if($dataCount > 0 && $rows[0]['token_line'] != "")
  // {
  //
  //   $message   = "มีผู้จองคลาส ผ่านช่องทาง Mobile App"."\r\n";
  //   $message  .= "ชื่อผู้จอง : ".$name."\r\n";
  //   $message  .= "Class Name : ".$name."\r\n";
  //   $message  .= "วัน เวลา : ".$name."\r\n";
  //
  //
  //   LINE_NOTIFY_MESSAGE($message,"",$token);
  // }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'บันทึกสำเร็จ')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'ไม่สามารถบันทึกได้ กรุณาติดต่อเจ้าหน้าที่')));
}

?>
