<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"GYMMK01";

$sql = "SELECT e.*, u.user_img FROM data_mas_employee e
        LEFT JOIN t_user u
        ON e.EMP_CODE = u.user_login
        WHERE EMP_IS_TRAINER = 'Y' and DATA_DELETE_STATUS != 'Y' and e.COMPANY_CODE = '$branchcode' order by  e.EMP_CODE";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];
$class = array();

if($dataCount > 0){
  for($j=0;$j<$dataCount; $j++)
  {
    if($row[$j]['EMP_PICTURE'] == "" && $row[$j]['user_img'] == ""){
      $image = "https://timefitnessbkk.com/admin/mobile/images/picture.png";
    }else{
      if($row[$j]['user_img'] == ""){
        $image = $row[$j]['EMP_PICTURE'];
      }else{
        $image = $row[$j]['user_img'];
      }
      $pos = strrpos($image, "data:image");
      if ($pos === false) {
          $image = "data:image/png;base64,".$image;
      }

    }

    $class[$j]['empCode']     = $row[$j]['EMP_CODE'];
    $class[$j]['image']       = $image;
    $class[$j]['trainerName'] = $row[$j]['EMP_TITLE'].$row[$j]['EMP_NAME']." ".$row[$j]['EMP_LASTNAME'];
    $class[$j]['nickName']    = $row[$j]['EMP_NICKNAME'];
  }
}

header('Content-Type: application/json');
exit(json_encode($class));

?>
