<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"";
$name       = isset($_GET['name'])?$_GET['name']:"";
$lname      = isset($_GET['lname'])?$_GET['lname']:"";
$nickname   = isset($_GET['nickname'])?$_GET['nickname']:"";
$email      = isset($_GET['email'])?$_GET['email']:"";
$tel        = isset($_GET['tel'])?$_GET['tel']:"";
$bod        = isset($_GET['bod'])?$_GET['bod']:"";
$idcard     = isset($_GET['idcard'])?$_GET['idcard']:"";
$type       = isset($_GET['type'])?$_GET['type']:"";

$fullname   = "คุณ".$name." ".$lname;

$status     = true;


if($type == "WEB"){
    $regTxt = "REG WEB";
    $txt    = "ลงทะเบียน ผ่านช่องทาง Website";
    $bod    = dateThToEn($bod,"dd/mm/yyyy","/");
}else if($type == "MOBILE"){
    $regTxt = "REG MOBILE";
    $txt    = "ลงทะเบียน ผ่านช่องทาง Mobile App";
}

$sql = "SELECT * FROM t_branch WHERE is_active ='Y' and branch_code = '$branchcode'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$rows       = $json['data'];
$dataCount  = $json['dataCount'];

$branch_id  = $rows[0]['branch_id'];


$sql2 = "SELECT * FROM person WHERE PERSON_CARD_ID ='$idcard'";
$query2      = DbQuery($sql2,null);
$json2       = json_decode($query2, true);
$dataCount2  = $json2['dataCount'];

if($dataCount2 > 0)
{
  $mess = "มีผู้ใช้งาน ID : ". $idcard ." แล้ว !!";
  $status     = false;
}else{
  if($dataCount > 0){

    $branchId = sprintf("%02d", $branch_id);
    $year = date('ym');
    $code = $year.$branchId;

    $sqlCode = "SELECT max(PERSON_CODE) as PERSON_CODE FROM person where COMPANY_CODE ='$branchcode' and PERSON_CODE like '$code%'";

    $queryc      = DbQuery($sqlCode,null);
    $jsonc       = json_decode($queryc, true);
    $rowc        = $jsonc['data'];
    $dataCountc  = $jsonc['dataCount'];

    if($dataCountc > 0){
      if($rowc[0]['PERSON_CODE'] != null && $rowc[0]['PERSON_CODE'] != ""){
        $lastNum  = $rowc[0]['PERSON_CODE'] + 1;
        $PERSON_CODE = sprintf("%03d", $lastNum);
      }else{
        $PERSON_CODE = $code."001";
      }
    }else{
      $PERSON_CODE = $code."001";
    }

    $PERSON_TEL_MOBILE      = str_replace(" ","",$tel);
    $PERSON_TEL_MOBILE      = str_replace("-","",$tel);

    $PERSON_USER_ID         = "$PERSON_CODE";
    $PERSON_USER_PSW        = md5($PERSON_TEL_MOBILE);

    $user_id_update   = "20001";

    $sqlp   = "INSERT INTO person (
              COMPANY_CODE,PERSON_RUNNO,PERSON_CODE,
              PERSON_CODE_INT,PERSON_TITLE,PERSON_NICKNAME,
              PERSON_NAME,PERSON_LASTNAME,PERSON_NAME_MIDDLE,
              PERSON_TITLE_ENG,PERSON_NAME_ENG,PERSON_LASTNAME_ENG,
              PERSON_SEX,PERSON_BIRTH_DATE,PERSON_CARD_ID,
              PERSON_IMAGE,PERSON_TEL_MOBILE,PERSON_TEL_MOBILE2,
              PERSON_TEL_OFFICE,PERSON_FAX,PERSON_GROUP,
              PERSON_GROUP_SERVICE,PERSON_GROUP_DISCOUNT,PERSON_STATUS,
              PERSON_NOTE,PERSON_USER_ID,	PERSON_USER_PSW,
              PERSON_GRADE,PERSON_GROUP_AGE,
              PERSON_PROVINCE_CODE,
              PERSON_DOOR_PASSWORD,PERSON_DOOR_GROUP,PERSON_HOME1_ADDR1,
              PERSON_HOME1_ADDR2,PERSON_HOME1_SUBDISTRICT,PERSON_HOME1_DISTRICT,
              PERSON_HOME1_PROVINCE,PERSON_HOME1_COUNTRY,PERSON_HOME1_POSTAL,
              PERSON_HOME1_MAP,PERSON_BUSINESS_TYPE,
              PERSON_BILL_TYPE,PERSON_BILL_NAME,PERSON_BILL_ADDR1,
              PERSON_BILL_ADDR2,PERSON_BILL_SUBDISTRICT,PERSON_BILL_DISTRICT,
              PERSON_BILL_PROVINCE,PERSON_BILL_POSTAL,PERSON_BILL_COUNTRY,
              PERSON_BILL_TAXNO,PERSON_SALE_CODE, PERSON_ACC_NO,
              DATA_CREATE_BY,DATA_CREATE_DATE,EMP_CODE_SALE,PERSON_EMAIL,PERSON_ER_TEL)
              VALUES (
              '$branchcode','$regTxt','$PERSON_CODE',
              '$PERSON_CODE','คุณ','$nickname',
              '$name','$lname','',
              '','','',
              '','$bod','$idcard',
              '','$tel','',
              '','','',
              '','','Y',
              '$txt','$PERSON_USER_ID','$PERSON_USER_PSW',
              '','',
              '',
              '','','',
              '','','',
              '','','',
              '','',
              '','','',
              '','','',
              '','','',
              '','','',
              '$user_id_update',NOW(),'','$email','')";

    $queryp      = DbQuery($sqlp,null);
    $jsonp       = json_decode($queryp, true);
    $errorInfop  = $jsonp['errorInfo'];

    if(intval($errorInfop[0]) == 0)
    {
      $mess = "ลงทะเบียนสำเร็จ";
      $message   = "\r\n"."มีผู้สนใจ ".$txt."\r\n";
      $message  .= "ชื่อผู้ลงทะเบียน : ".$fullname."\r\n";
      $message  .= "เบอร์โทรติดต่อ : ".$tel."\r\n";
      $message  .= "E-mail : ".$email;

      $token =  $rows[0]['token_line'];
      LINE_NOTIFY_MESSAGE($message,"",$token);
    }else{
      $mess = "ไม่สามารถลงทะเบียนได้";
      $status  = false;
    }
  }else{
    $mess = "ไม่สามารถลงทะเบียนได้ รอเจ้าหน้าที่ติดต่อกลับ";
    $status  = false;
  }
}

$row[0]['message']  = $mess;
$row[0]['status']   = $status;

header('Content-Type: application/json');
exit(json_encode($row));

?>
