<?php
header('Access-Control-Allow-Origin: *');
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$person_code        = isset($_GET['person_code'])?$_GET['person_code']:"";
$date               = date("Y/m/d");
$time               = date("H:i:s");
$data               = array();

if($person_code != ""){

  $sqlp       = "SELECT person_name,person_lastname,person_nickname,person_expire_date FROM person WHERE person_code = '$person_code' and company_code = 'GYMMK02'";
  $queryp      = DbQuery($sqlp,null);
  $jsonp       = json_decode($queryp, true);
  $rowp        = $jsonp['data'];
  $dataCountp  = $jsonp['dataCount'];


  $data[0]['fname']         = "";
  $data[0]['nickname']      = "";
  $data[0]['expire_date']   = "";
  $data[0]['date_time']     = "";

  if($dataCountp > 0)
  {
    $person_name        = $rowp[0]['person_name'];
    $person_lastname    = $rowp[0]['person_lastname'];
    $person_nickname    = $rowp[0]['person_nickname'];
    $person_expire_date = $rowp[0]['person_expire_date'];

    $data[0]['expire_date']  = DateTxtThai($person_expire_date);
    $data[0]['fname']         = $person_name." ".$person_lastname;
    $data[0]['nickname']      = $person_nickname;



    $sql = "SELECT * FROM tb_checkin WHERE person_code = '$person_code' and date_checkin = '$date'";

    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $row        = $json['data'];
    $dataCount  = $json['dataCount'];

    if($dataCount > 0)
    {
        $checkin_id = $row[0]['checkin_id'];
        $sql = "UPDATE tb_checkin SET time_checkout = '$time' where checkin_id = '$checkin_id'";

        $data[0]['type_time'] = "checkout";
        $data[0]['date_time'] = DateTxtThai($date)." เวลา ".$time." น.";
        $data[0]['message']   = "";
    }
    else
    {
      $sql = "INSERT INTO tb_checkin (date_checkin,time_checkin,person_code)VALUES('$date','$time','$person_code');";

      $data[0]['type_time'] = "checkin";
      $data[0]['date_time'] = DateTxtThai($date)." เวลา ".$time." น.";
      $data[0]['message']   = "";
    }

    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];

    if(intval($row['errorInfo'][0]) == 0){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => true,'message' => 'Success','data' => $data)));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => false,'message' => 'Fail')));
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => false,'message' => 'ไม่พบสมาชิก Member Ref : '.$person_code)));
  }
}else{
  exit("<script>window.location='https://www.timefitnessbkk.com/app'</script>");
}
?>
