<?php
header('Access-Control-Allow-Origin: *');
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$personCode       = isset($_GET['personCode'])?$_GET['personCode']:"";

$con = "";
$dateNow          = date('Y-m-d');

$con = " and rc.expire_date >=  '$dateNow' ";

$sql = "SELECT rc.*,scd.*,e.EMP_CODE,e.EMP_NICKNAME, c.name_class, CONCAT(p.PERSON_TITLE, p.PERSON_NAME, ' ', p.PERSON_LASTNAME) as person_name, p.PERSON_NICKNAME,b.cname
FROM t_reserve_class rc, tb_schedule_class_day scd, t_classes c, data_mas_employee e, person p, t_branch b
WHERE rc.PERSON_CODE = p.PERSON_CODE and rc.PERSON_CODE = '$personCode' and rc.status in ('C','S') and b.branch_code = 	rc.branch_code
and rc.schedule_day_id = scd.id and scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE $con
order by scd.date_class, scd.day, scd.row, rc.date_reserve ";

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$arr = array();
if($dataCount > 0){
  $tmpDate = "";
  $in     = -1;
  $day    = "";
  $month  = "";
  $year   = "";
  $x;
  for($j=0;$j<$dataCount; $j++)
  {
      $date_class = $row[$j]['date_class'];

      if($tmpDate != $date_class)
      {
        $in++;
        $x = 0;
        $tmpDate = $date_class;
        $da = explode("-",$date_class);
        $day    = $da[2] + 0;
        $month  = $da[1] - 1;
        $year   = $da[0] + 0;

        $arr['data'][$in]['day']    = $day;
        $arr['data'][$in]['month']  = $month;
        $arr['data'][$in]['year']   = $year;
      }

      $arr['data'][$in]['detail'][$x]['id']         = $row[$j]['id'];
      $arr['data'][$in]['detail'][$x]['date_class'] = date('D d/m/Y',strtotime($date_class))." ".$row[$j]['time_start']."-".$row[$j]['time_end'];
      $arr['data'][$in]['detail'][$x]['name_class'] = $row[$j]['name_class'];
      $arr['data'][$in]['detail'][$x]['emp_nickname'] = $row[$j]['EMP_NICKNAME'];
      $arr['data'][$in]['detail'][$x]['branch_name'] = $row[$j]['cname'];
      $x++;
  }

}

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode($arr));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}
?>
