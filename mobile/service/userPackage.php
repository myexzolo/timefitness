<?php
header('Access-Control-Allow-Origin: *');
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$personCode = isset($_GET['code'])?$_GET['code']:"";

$dateNow = date("Y/m/d H:i:s");

$sql = "SELECT ps.package_name  as packageName,
DATE_FORMAT(ps.date_start,'%d/%m/%Y') as dateStart,
DATE_FORMAT(ps.date_expire,'%d/%m/%Y')as dateEnd,
CONCAT(ps.use_package, '/', ps.num_use, ' ', ps.package_unit) as totalCount
FROM trans_package_person ps
where ps.person_code = '$personCode' and 	ps.status in ('A','U') and ps.date_expire > '$dateNow'
order by ps.status ASC, ps.date_expire ASC";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else
{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
