<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$lat= isset($_GET['lat'])?$_GET['lat']:"13.8566855";
$lng= isset($_GET['lng'])?$_GET['lng']:"100.439375";



$sql = "SELECT * FROM t_branch WHERE is_active ='Y'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if($dataCount > 0){
  for($j=0;$j<$dataCount; $j++)
  {
    $blat = $row[$j]['lat'];
    $blng = $row[$j]['lng'];

    $units = "imperial";
    $origins = $lat.",".$lng;
    $destinations = $blat.",".$blng;
    $key = "AIzaSyCahuGbluoi_UlR5_iY-GSP2zTcfQD1lZc";
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=$units&origins=$origins&destinations=$destinations&key=$key";
    $jsonMap  = json_decode(getDataSSL($url), true);
    $elements = $jsonMap['rows'][0]['elements'];
    $distance = $elements[0]['distance']['value'];

    $row[$j]['distance'] = $distance;
  }

  usort($row, build_sorter('distance'));
}

function build_sorter($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($a[$key], $b[$key]);
    };
}

header('Content-Type: application/json');
exit(json_encode($row));


?>
