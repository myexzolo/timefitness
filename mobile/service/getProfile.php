<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

$code      = $_GET['code'];
$type      = $_GET['type'];

FIX_PHP_CORSS_ORIGIN();

$sql = "";
if($type == "user"){
  $sql = "SELECT PERSON_CODE as code,PERSON_TITLE as title,PERSON_NICKNAME as nickname,PERSON_NAME as name,PERSON_LASTNAME as lname,
          PERSON_SEX as sex,PERSON_BIRTH_DATE as bod,	PERSON_STATUS as status, PERSON_EMAIL as email,PERSON_TEL_MOBILE as tel,PERSON_TEL_MOBILE2 as tel2,
          PERSON_REGISTER_DATE as reg_date,PERSON_EXPIRE_DATE as exp_date,PERSON_NOTE as note,PERSON_IMAGE as image,
          PERSON_HOME1_ADDR1 as address1,PERSON_HOME1_ADDR2 as address2,PERSON_HOME1_SUBDISTRICT as subdistrict,PERSON_HOME1_DISTRICT as district,
          PERSON_HOME1_PROVINCE as provice,PERSON_HOME1_POSTAL as postal,	'' as staff,'' as trainer,'' as sale
          FROM person WHERE PERSON_CODE ='$code'";

}else if($type == "staff"){
  $sql = "SELECT e.EMP_CODE as code,e.EMP_TITLE as title,e.EMP_NICKNAME as nickname,e.EMP_NAME as name,e.EMP_LASTNAME	as lname,
          e.EMP_SEX as sex,e.EMP_BIRTH_DATE as bod,u.is_active as status, EMP_EMAIL as email, EMP_TEL as tel,
          e.EMP_DATE_INCOME as reg_date,e.EMP_DATE_RETRY as exp_date,'' as note, '' as image,
          e.EMP_ADDRESS1 as address1,e.EMP_ADDRESS2 as address2,e.EMP_ADDR_SUBDISTRICT as subdistrict,e.EMP_ADDR_DISTRICT as district,
          e.EMP_ADDR_PROVINCE as provice,e.EMP_ADDR_POSTAL as postal,e.EMP_IS_STAFF as staff,e.EMP_IS_TRAINER as trainer,e.EMP_IS_SALE as sale
          FROM t_user u, data_master m, data_mas_employee e
          WHERE u.user_login ='$user' AND u.user_password='$pass' and u.is_active = 'Y'
          AND u.department_code = m.DATA_CODE
          and m.DATA_GROUP = 'EMP_DEPARTMENT'
          AND u.user_login = e.EMP_CODE";
}


//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$roleName     = "-";
if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  $row[0]['status'] = true;
  $row[0]['typeUser'] = $type;

  $image = $row[0]['image'];
  if($image != ""){
    $pos = strrpos($image, "data:image");
    if ($pos === false) {
        $image = "data:image/png;base64,".$image;
        $row[0]['image'] = $image;
    }
  }


  $row[0]['package'] = array();
  {
    if($type == "user"){
          $sql = "SELECT * from trans_package_person where person_code = '$code' and status in ('A')";
          $query      = DbQuery($sql,null);
          $json       = json_decode($query, true);
          $errorInfo  = $json['errorInfo'];
          $rowp       = $json['data'];
          $dataCount  = $json['dataCount'];

          for($x=0; $x < $dataCount; $x++)
          {
            $date_expire    = DateThai($rowp[$x]['date_expire']);
            $use_package    = $rowp[$x]['use_package'];
            $num_use        = $rowp[$x]['num_use'];
            $package_unit   = $rowp[$x]['package_unit'];
            $type_package   = $rowp[$x]['type_package'];

            if($type_package == "MB"){
              $text = "Member Exp. ".$date_expire;
              $detail = "Last ".$use_package." ".$package_unit;
            }else if($type_package == "PT")
            {
              $text = "Trainer Exp. ".$date_expire;
              $detail = $use_package."/".$num_use." ครั้ง";
            }

            $row[0]['package'][$x]['text']   = $text;
            $row[0]['package'][$x]['detail'] = $detail;
          }
    }
  }
  header('Content-Type: application/json');
  exit(json_encode($row[0]));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
