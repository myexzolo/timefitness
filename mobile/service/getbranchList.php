<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$lat= isset($_GET['lat'])?$_GET['lat']:"";
$lng= isset($_GET['lng'])?$_GET['lng']:"";





$sql = "SELECT * FROM t_branch WHERE is_active ='Y'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if($dataCount > 0){
  for($j=0;$j<$dataCount; $j++)
  {
    if($lat != "" && $lng != ""){
      $blat = $row[$j]['lat'];
      $blng = $row[$j]['lng'];

      $units = "imperial";
      $origins = $lat.",".$lng;
      $destinations = $blat.",".$blng;
      $key = "AIzaSyCahuGbluoi_UlR5_iY-GSP2zTcfQD1lZc";
      $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=$units&origins=$origins&destinations=$destinations&key=$key";
      $jsonMap  = json_decode(getDataSSL($url), true);
      $elements = $jsonMap['rows'][0]['elements'];
      $distance = $elements[0]['distance']['value'];

      $duration = $elements[0]['duration']['text'];
      $km       = $distance/1000;
      //print_r($jsonMap);

      $row[$j]['distance'] = $distance;
      $row[$j]['place'] = number_format($km, 2)." Km ".$duration;
    }else{
      $row[$j]['distance'] = 	$row[$j]['branch_id'];
      $row[$j]['place'] = "";
    }

  }

  usort($row, build_sorter('distance'));
}

function build_sorter($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($a[$key], $b[$key]);
    };
}

header('Content-Type: application/json');
exit(json_encode($row));


?>
