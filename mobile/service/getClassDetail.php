<?php
header('Access-Control-Allow-Origin: *');
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$id             = isset($_GET['id'])?$_GET['id']:"";
$personCode     = isset($_GET['personCode'])?$_GET['personCode']:"";

// $id         = "312019121023";
// $personCode = "190702021";


$dateNow = date("Y/m/d H:i:s");
$mess_btn   = "Join This Class";
$mess_foot  = "กรุณาจอง Class ก่อนเวลา 30 นาที";

// $sql = "SELECT scd.*,c.name_class,c.image_class,c.vid,c.detail_class,e.EMP_NICKNAME,e.EMP_PICTURE,s.branch_code, u.user_img
// FROM tb_schedule_class_day scd, t_classes c, data_mas_employee e, tb_schedule_class s
// LEFT JOIN t_user u
// ON e.EMP_CODE = u.user_login
// WHERE scd.id_class = c.id_class and scd.EMP_CODE = e.EMP_CODE  and scd.schedule_id = s.schedule_id
// and scd.id = '$id' order by scd.date_class, scd.day, scd.row ";

$sql = "SELECT scd.*,c.name_class,c.image_class,c.vid,c.detail_class,e.EMP_NICKNAME,e.EMP_PICTURE,s.branch_code, u.user_img
FROM tb_schedule_class_day scd
INNER JOIN t_classes c ON scd.id_class = c.id_class
INNER JOIN data_mas_employee e ON scd.EMP_CODE = e.EMP_CODE
INNER JOIN tb_schedule_class s ON scd.schedule_id = s.schedule_id
LEFT JOIN t_user u
ON scd.EMP_CODE = u.user_login
WHERE scd.id = '$id' order by scd.date_class, scd.day, scd.row ";

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];
$class = array();

if($dataCount > 0){
  $date_class = date('D d/m/Y',strtotime($row[0]['date_class']));
  $cname = getBranchName( $row[0]['branch_code']);

  if($row[0]['EMP_PICTURE'] == "" && $row[0]['user_img'] == ""){
    $image = "";
  }else{
    if($row[0]['user_img'] == ""){
      $image = $row[0]['EMP_PICTURE'];
    }else{
      $image = $row[0]['user_img'];
    }
    $pos = strrpos($image, "data:image");
    if ($pos === false) {
        $image = "data:image/png;base64,".$image;
    }
  }

  $flag   = true;
  $flag_c = true;

  $unit = $row[0]['unit'];
  $person_join = $row[0]['person_join'];

  if($unit > 0 && $person_join >= $unit && $personCode != "")
  {
    $flag = false;
    //$flag_c = false;
    $mess_btn   = "fully Booking";
    $mess_foot  = "การจอง Class เต็มจำนวน";
  }

  $dateNow          = date("Y-m-d H:i");
  $reserveDateClass = date('Y-m-d H:i',strtotime($row[0]['date_class']." ".$row[0]['time_start']." -30 minutes"));
  $dateClass        = date('Y-m-d H:i',strtotime($row[0]['date_class']." ".$row[0]['time_start']));
  if($dateNow >= $reserveDateClass && $flag)
  {
    $flag = false;
    $flag_c = false;
    $mess_btn   = "Booking Timeout";
    $mess_foot  = "หมดเวลาจอง Class";
  }

  $now_time1=strtotime($dateNow);
  $now_time2=strtotime($dateClass);
  $time_diff= $now_time2-$now_time1;
  $time_diff_h=floor($time_diff/3600); // จำนวนชั่วโมงที่ต่างกัน
  $time_diff_m=floor(($time_diff%3600)/60); // จำวนวนนาทีที่ต่างกัน
  $time_diff_s=($time_diff%3600)%60; // จำนวนวินาทีที่ต่างกัน

  //echo ">>>>>>>>>>>>>".$dateClass.",".$dateNow.":".$time_diff_h.":".$time_diff_m;

  if($time_diff_h > 48 && $flag){
    $flag = false;
    $flag_c = false;

    $mess_btn   = "Join This Class";
    $mess_foot  = "จอง Class ล่วงหน้าได้ไม่เกิน 2 วัน";
  }

  if($time_diff_h <= 2 && $flag_c){
    $flag_c = false;
    $mess_foot  = "ยกเลิก Class ก่อนเวลา 2 ชั่วโมง";
  }

  $dateBan = "";

  if($personCode != "" && $flag)
  {
    $sql2 = "SELECT * FROM person WHERE PERSON_CODE = '$personCode'";
    $query2      = DbQuery($sql2,null);
    $json2       = json_decode($query2, true);
    $row2        = $json2['data'];

    $companyCode = $row2[0]['COMPANY_CODE'];
    $dateBan     = @$row2[0]['BAN_RESERVE'];

    $branch = "";
    if($companyCode == "GYMMK01")
    {
      $branch = " mp.branch1 = 'Y' ";
    }
    else if ($companyCode == "GYMMK02")
    {
      $branch = " mp.branch2 = 'Y' ";
    }
    else if ($companyCode == "GYMMK03")
    {
      $branch = " mp.branch3 = 'Y' ";
    }
    else if ($companyCode == "GYMMK04")
    {
      $branch = " mp.branch4 = 'Y' ";
    }
    else if ($companyCode == "GYMMK05")
    {
      $branch = " mp.branch5 = 'Y' ";
    }

    $sql2 = "SELECT ps.*
    FROM trans_package_person ps, data_mas_package mp
    WHERE (ps.company_code = '$companyCode' or $branch)
    and ps.package_code = mp.package_code
    and ps.person_code = '$personCode' and 	ps.status in ('A','U') and ps.date_expire > '$dateNow'
    order by ps.status ASC, ps.date_expire ASC";

    $query2      = DbQuery($sql2,null);
    $json2       = json_decode($query2, true);
    $row2        = $json2['data'];
    $dataCount2  = $json2['dataCount'];

    if($dataCount2 <= 0)
    {
      $flag = false;
      $flag_c = false;

      $mess_btn   = "Join This Class";
      $mess_foot  = "เฉพาะ Member Gym เท่านั้น";
    }else{

      if($dateBan != null && $dateBan != "")
      {
         $dateBan   = date("Y-m-d H:i", strtotime($dateBan." 23:00"));
         if($dateBan >= $dateNow)
         {
          $diff = DateDiff($dateNow,$dateBan);

           $flag = false;
           $flag_c = false;
           $mess_btn   = "Join This Class";
           $mess_foot  = "พักสิทธิการจองเวลา ".round($diff)." วัน";
         }
      }
    }
  }

  if($personCode != "" && $flag)
  {
    $sql2 = "SELECT * FROM t_reserve_class WHERE PERSON_CODE = '$personCode' and schedule_day_id = '$id' and status != 'D'";
    $query2      = DbQuery($sql2,null);
    $json2       = json_decode($query2, true);
    $row2        = $json2['data'];
    $dataCount2  = $json2['dataCount'];

    if($dataCount2 > 0)
    {
      $status = $row2[0]['status'];

      if($status == "P"){
        $mess_btn   = "Pending Class";
      }else{
        $mess_btn   = "Booking Class";
      }
      $mess_foot  = "ยกเลิก Class ก่อนเวลา 2 ชั่วโมง";

      $flag = false;
      $flag_c = true;
    }
  }


  $class['data']['image'] = $row[0]['image_class'];
  $class['data']['trainerName'] = $row[0]['EMP_NICKNAME'];
  $class['data']['trainerImage'] = $image;
  $class['data']['className'] = $row[0]['name_class'];
  $class['data']['date'] = $date_class;
  $class['data']['time'] = $row[0]['time_start']." - ".$row[0]['time_end'];
  $class['data']['detail'] = $row[0]['person_join']."/".$row[0]['unit'];
  $class['data']['room'] = "";
  $class['data']['location'] = $cname;
  $class['data']['description'] = $row[0]['detail_class'];
  $class['data']['vid'] = $row[0]['vid'];
  $class['data']['expireDate'] = $row[0]['date_class']." ".str_replace(" ","",$row[0]['time_end'].":00");
  $class['data']['branchCode'] = $row[0]['branch_code'];
  $class['data']['status_booking'] = $flag;
  $class['data']['status_cancel'] = $flag_c;
  $class['data']['mess_btn'] = $mess_btn;
  $class['data']['mess_foot'] = $mess_foot;

}

header('Content-Type: application/json');
exit(json_encode($class));


?>
