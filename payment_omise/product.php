<!DOCTYPE html>
<html>
<head>
  <title>Payment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" href="css/payment.css">
  <style media="screen">
  .checkout-form > button {
    display: inline-block;
    padding: 1em;
    border: 0;
    background-color: #1A53F0;
    color: white;
    outline: none;
    border-radius: 4px;
  }
  </style>
</head>
<body>
  <main class="page payment-page">
    <section class="payment-form dark">
      <div class="container">
        <div class="block-heading">
          <h2>Payment</h2>
          <p>
            <img class="img-responsive" src="http://gymmonkeybkk.com/images/Logo_ym.png" alt="GYM Monkey">
          </p>
        </div>
        <div class="checkout-form">
          <div class="products">
            <h3 class="title">Checkout</h3>
            <div class="item">
              <?php

              $pid = $_GET['pid'];
              $mc  = $_GET['mc'];

              function httpGet($url){
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                $output = curl_exec($ch);
                curl_close($ch);
                return $output;
              }
              $arr = json_decode(httpGet("https://gymmonkey.onesittichok.co.th/mobile/service/payPromotion.php?pid=$pid&mc=$mc"),true);
              // print_r($arr);

              $price = $arr['data']['promotion'][0]['price'];
              $p_id  = $arr['data']['promotion'][0]['id'];
              $code  = $arr['data']['person'][0]['code'];

              $arrr = array(',','.');
              $prices = str_replace($arrr,"",number_format($price,2));

              ?>
              <span class="price"><?=number_format($price,2);?></span>
              <p class="item-name"><?=$arr['data']['promotion'][0]['title']?></p>
              <p class="item-description">
                <img height="100px" src="<?=$arr['data']['promotion'][0]['image']?>">
              </p>
            </div>
            <div class="total">Total<span class="price"><?=number_format($price,2);?></span></div>
          </div>
          <div class="card-details">
            <div class="row">
              <div class="form-group col-sm-12">
                <form  name="checkoutForm" method="POST" action="checkout.php">
                  <input type="hidden" name="p_id" value="<?=$p_id?>">
                  <input type="hidden" name="code" value="<?=$code?>">
                  <script type="text/javascript" src="https://cdn.omise.co/omise.js.gz"
                          data-key="pkey_test_5ht3wucnpumz4icv1eu"
                          data-frame-label="GYM Monkey"
                          data-image="http://gymmonkeybkk.com/images/monkey-ico.png"
                          data-frame-description="@GymMonkeyThailand"
                          data-button-label="ชำระเงิน / Pay"
                          data-amount="<?=$prices?>"
                          data-currency="thb">
                  </script>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
